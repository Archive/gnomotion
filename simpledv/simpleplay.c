/*
 * Adaptation of playdv.c that uses the simpledv interface
 *
 * Written by: Vladimir Vukicevic <vladimir@pobox.com>
 */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include <gtk/gtk.h>

#include "simpledv.h"


int main (int argc, char **argv)
{
     DVFile *dvf;
     GtkWidget *window, *image;
     guint8 *videoptr;

     guint32 curframe = 0;

     int frameint;

     gboolean isPAL;

     if (argc < 2 || argc > 3) {
	  fprintf (stderr, "Usage: %s file.dv [frameint]\n", argv[0]);
	  exit (1);
     }

     if ((dvf = dvf_open (argv[1])) == NULL) {
	  fprintf (stderr, "Failed to open %s\n", argv[1]);
	  exit (1);
     }

     isPAL = dvf_is_PAL (dvf);

     if (argc == 3)
	  frameint = atoi (argv[2]);
     else
	  frameint = 1;

     /* Open up our GTK Window */

     gtk_init (&argc, &argv);
     gdk_rgb_init ();

     gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
     gtk_widget_set_default_visual (gdk_rgb_get_visual ());

     window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
     image = gtk_drawing_area_new ();

     gtk_container_add (GTK_CONTAINER (window), image);
     gtk_drawing_area_size (GTK_DRAWING_AREA (image), 720, isPAL ? 576 : 480);
     gtk_widget_set_usize (GTK_WIDGET (image), 720, isPAL ? 576 : 480);
     gtk_widget_show_all (window);
     gdk_flush ();

     while (!dvf_is_end (dvf)) {
	  videoptr = dvf_get_frame (dvf, curframe);
	  if (!videoptr) break;
	  curframe += frameint;

	  gdk_draw_rgb_image (image->window, image->style->fg_gc[image->state],
                              0, 0, 720, isPAL ? 576 : 480, GDK_RGB_DITHER_NORMAL, videoptr,
                              720 * 3);
	  gdk_flush ();

	  while (gdk_events_pending ())
	       gtk_main_iteration ();
     }

     return 0;
}
