/*
 * simpledv.c
 *   Written by Vladimir Vukicevic <vladimir@helixcode.com>
 *   Copyright 2000 (C) Vladimir Vukicevic
 *
 * A lot of this code has been shamelessly copied from playdv.c, and as such
 * is also:
 *
 *     Copyright (C) Charles 'Buck' Krasic - April 2000
 *     Copyright (C) Erik Walthinsen - April 2000
 *
 *  libdv and simpledv are free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your
 *  option) any later version.
 *   
 *  libdv is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *   
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Make; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "simpledv.h"

#include "dv.h"
#include "bitstream.h"
#include "dct.h"
#include "idct_248.h"
#include "quant.h"
#include "weighting.h"
#include "vlc.h"
#include "parse.h"
#include "place.h"
#include "ycrcb_to_rgb32.h"

#define DIFSIZE 80
#define VSBUFSIZE 5*DIFSIZE
#define FRAMEBUFSIZE 720*576*4
#define DIFSEQSIZE 150

static void convert_coeffs(dv_block_t *bl)
{
  int i;
  for(i=0;
      i<64;
      i++) {
    bl->coeffs248[i] = bl->coeffs[i];
  }
}

static void convert_coeffs_prime(dv_block_t *bl)
{
  int i;
  for(i=0;
      i<64;
      i++) {
    bl->coeffs[i] = bl->coeffs248[i];
  }
}

DVFile *dvf_open (char *filename)
{
     guint8 vsbuf[VSBUFSIZE] __attribute__ ((aligned (64)));
     int the_fd;
     DVFile *dvf;
     static int dvinited = 0;
     struct stat st;

     if (stat (filename, &st) < 0) {
	  perror ("stat");
	  return NULL;
     }

     if ((the_fd = open (filename, O_RDONLY)) < 0) {
	  perror ("open");
	  return NULL;
     }

     dvf = g_malloc (sizeof (DVFile));

     dvf->valid = TRUE;
     dvf->fd = the_fd;

     /* Do some header init */
     dvf->cur_dif = 0;
     dvf->cur_frame = 0;
     dvf->end_of_video = FALSE;

     dvf->file_size = st.st_size;

     /* We read the first dif block just to get info on this stream */
     if (read (the_fd, &vsbuf[0], DIFSIZE) < DIFSIZE) {
	  perror ("read");
	  close (the_fd);
	  g_free (dvf);
	  return 0;
     }

     dvf->isPAL = vsbuf[3] & 0x80;
     dvf->is61834 = vsbuf[3] & 0x01;
     dvf->numDIFseq = dvf->isPAL ? 12 : 10;
     dvf->sampling = (dvf->isPAL && dvf->is61834) ? e_dv_sample_420 : e_dv_sample_411;

     lseek (the_fd, 0, SEEK_SET);
     dvf->cur_dif = 0;

     if (!dvinited) {
	  weight_init ();
	  dct_init ();
	  dv_dct_248_init ();
	  dv_construct_vlc_table ();
	  dv_parse_init ();
	  dv_place_init ();
	  dv_ycrcb_init ();

	  dvinited = 1;
     }
     dvf->videoseg = g_new0 (dv_videosegment_t, 1);
     dvf->videoseg->bs = bitstream_init ();
     dvf->videoseg->isPAL = dvf->isPAL;
     dvf->the_pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, 720, dvf->isPAL ? 576 : 480);
     dvf->cur_framebuf = gdk_pixbuf_get_pixels (dvf->the_pixbuf);

     /* guess from size */
     dvf->num_frames = st.st_size / (DIFSIZE * DIFSEQSIZE * dvf->numDIFseq);
     if ((dvf->num_frames * (DIFSIZE * DIFSEQSIZE * dvf->numDIFseq)) > st.st_size)
         dvf->num_frames--;

#ifdef DEBUG
     fprintf (stderr, "Opened %s.. size %d, frames %d (len %d sec)\n", filename, (glong) st.st_size,
	      dvf->num_frames,
	      dvf->num_frames / (dvf->isPAL ? 24 : 30));
#endif

     return dvf;
}

static int
read_next_frame (DVFile *dvf)
{
     dv_videosegment_t *vseg = dvf->videoseg;
     guint locdif;
     guint ds, v, b, m;

     dv_macroblock_t *mb;
     size_t mb_offset;
     dv_block_t *bl;

     guint offset;

     guint8 vsbuf[VSBUFSIZE] __attribute__ ((aligned (64)));

     g_assert (dvf && dvf->valid);

     locdif = dvf->cur_frame * DIFSEQSIZE * dvf->numDIFseq;

     /* Each DIF segment consists of 150 dif blocks; 135 of these are video blocks */
     for (ds = 0; ds < dvf->numDIFseq; ds++) {
	  /* Skip the first 6 dif blocks in a sequence [why?] */
	  locdif += 6;

	  /* Loop through 27 video segments */
	  for (v = 0; v < 27; v++) {

	       /*  skip audio block -- before every 3rd video segment */
	       if (!(v % 3)) locdif++;

	       /* Parse and decode 5 macroblocks that make up a video segment */
	       offset = DIFSIZE * locdif;
	       lseek (dvf->fd, offset, SEEK_SET);
	       if (read (dvf->fd, vsbuf, VSBUFSIZE) < VSBUFSIZE) {
		    /* return -1 signalling no more frames */
		    memset (dvf->cur_framebuf, 0, FRAMEBUFSIZE);
		    dvf->end_of_video = TRUE;
		    return -1;
	       }

	       bitstream_new_buffer (vseg->bs, vsbuf, VSBUFSIZE);
	       vseg->i = ds;
	       vseg->k = v;
               vseg->isPAL = dvf->isPAL;

	       dv_parse_video_segment (vseg, DV_QUALITY_COLOR | DV_QUALITY_AC_2);

	       /* dequant/unwight/iDCT blocks, place macroblocks */

	       mb = vseg->mb;
	       for (m = 0; m < 5; m++) {
		    bl = mb->b;
		    for (b = 0; b < 6; b++) {
			 if (bl->dct_mode == DV_DCT_248) {
			      quant_248_inverse (bl->coeffs, mb->qno, bl->class_no);
			      weight_248_inverse (bl->coeffs);
			      convert_coeffs (bl);
			      dv_idct_248 (bl->coeffs248);
			      convert_coeffs_prime (bl);
			 } else {
			      quant_88_inverse (bl->coeffs, mb->qno, bl->class_no);
			      weight_88_inverse (bl->coeffs);
			      idct_88 (bl->coeffs);
			 }
			 bl++;
		    }

		    if (dvf->sampling == e_dv_sample_411) {
			 mb_offset = dv_place_411_macroblock (mb, 3);
			 if ((mb->j == 4) && (mb->k > 23)) {
			      dv_ycrcb_420_block (dvf->cur_framebuf + mb_offset, mb->b);
			 } else {
			      dv_ycrcb_411_block (dvf->cur_framebuf + mb_offset, mb->b);
			 }
		    } else {
			 mb_offset = dv_place_420_macroblock (mb, 3);
			 dv_ycrcb_420_block (dvf->cur_framebuf + mb_offset, mb->b);
		    }
		    mb++;
	       }

	       locdif += 5;
	  }
     }
     /* Now we have a pretty frame in dvf->cur_vidbuf */

     dvf->cur_dif = locdif;
     return 0;
}

guint8 *dvf_next_frame (DVFile *dvf)
{
     g_assert (dvf && dvf->valid);

#if 0
     if (read_next_frame (dvf) == 0) {
	  return dvf->cur_framebuf;
     } else {
	  return NULL;
     }
#else
     read_next_frame (dvf);
     dvf->cur_frame++;
     return dvf->cur_framebuf;
#endif
}

guint8 *dvf_cur_frame (DVFile *dvf)
{
#if 0
     if (dvf->end_of_video) return NULL;
#endif

     return dvf->cur_framebuf;
}

int dvf_close (DVFile *dvf)
{
     g_assert (dvf && dvf->valid);
     gdk_pixbuf_unref (dvf->the_pixbuf);
     free (dvf->videoseg->bs);
     free (dvf->videoseg);
     dvf->valid = FALSE;
     free (dvf);
     return 0;
}


gboolean dvf_is_PAL (DVFile *dvf)
{
     g_assert (dvf && dvf->valid);
     return dvf->isPAL;
}

gboolean dvf_is_end (DVFile *dvf)
{
     g_assert (dvf && dvf->valid);
     return dvf->end_of_video;
}

gboolean dvf_goto_frame (DVFile *dvf, guint32 frame)
{
     if (frame >= dvf->num_frames)
	  return FALSE;

     dvf->cur_frame = frame;
     return TRUE;
}

guint8 *dvf_get_frame (DVFile *dvf, guint32 frame)
{
     if (frame != dvf->cur_frame + 1)
	  if (!dvf_goto_frame (dvf, frame))
	       return NULL;

     return dvf_next_frame (dvf);
}

guint32 dvf_num_frames (DVFile *dvf)
{
     g_assert (dvf);
     return dvf->num_frames;
}

GdkPixbuf *dvf_get_pixbuf (DVFile *dvf)
{
     g_assert (dvf);
     return dvf->the_pixbuf;
}
