/*
 * A Simple interface to libdv that exports a nice
 * API for outside use.
 *
 * Written by: Vladimir Vukicevic <vladimir@pobox.com>
 *
 */

#ifndef SIMPLEDV_H_
#define SIMPLEDV_H_

#include <glib.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

struct dv_videosegment_s;

typedef struct {
     gboolean valid;
     int fd;
     guint32 file_size;

     gboolean isPAL;
     gboolean is61834;
     guint numDIFseq;
     gint sampling;
     gboolean end_of_video;

     guint cur_dif;
     guint cur_frame;
     guint num_frames;
     GdkPixbuf *the_pixbuf;
     guint8 *cur_framebuf;

     struct dv_videosegment_s *videoseg __attribute__ ((aligned (64)));
} DVFile;

DVFile *dvf_open (char *filename);
guint8 *dvf_next_frame (DVFile *dvf);
guint8 *dvf_cur_frame (DVFile *dvf);
guint8 *dvf_get_frame (DVFile *dvf, guint32 frame_num);
int dvf_close (DVFile *dvf);

gboolean dvf_is_PAL (DVFile *dvf);
gboolean dvf_is_end (DVFile *dvf);
guint32 dvf_num_frames (DVFile *dvf);

GdkPixbuf *dvf_get_pixbuf (DVFile *dvf);
gboolean dvf_goto_frame (DVFile *dvf, guint32 frame);
#endif
