/*
 * gmui-positioner.c
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gmui-positioner.h"

static void gmui_positioner_class_init (GMUIPositionerClass *klass);
static void gmui_positioner_init (GMUIPositioner *obj);

static GtkWidgetClass *gmui_positioner_parent;

guint
gmui_positioner_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMUIPositioner",
            sizeof (GMUIPositioner),
            sizeof (GMUIPositionerClass),
            (GtkClassInitFunc) gmui_positioner_class_init,
            (GtkObjectInitFunc) gmui_positioner_init,
            NULL,
            NULL,
            NULL
        };

        type = gtk_type_unique (GTK_TYPE_WIDGET, &type_info);
    }

    return type;
}

static void
gmui_positioner_class_init (GMUIPositionerClass *klass)
{
    GtkObjectClass *object_class;
    GtkWidgetClass *widget_class;

    object_class = (GtkObjectClass *) klass;
    widget_class = (GtkWidgetClass *) klass;

    widget_class->draw = gmui_positioner_draw;
    widget_class->size_request = gmui_positioner_size_request;
    widget_class->size_allocate = gmui_positioner_size_allocate;

    gmui_positioner_parent = gtk_type_class (GTK_TYPE_WIDGET);
}

static void
gmui_positioner_init (GMUIPositioner *obj)
{
    obj->display_as_timecode = TRUE;
}

GMUIPositioner *
gmui_positioner_new (GMMediaClip *src_clip, GMCursor *cursor)
{
    GMUIPositioner *tl;

    tl = GMUI_POSITIONER (gtk_type_new (gmui_positioner_get_type ()));
    tl->clip = NULL;
    tl->cursor = cursor;

    /* add a signal on the cursor */

    return tl;
}

static void
gmui_positioner_size_request (GtkWidget *widget,
                            GtkRequisition *requisition)
{
    GMUIPositioner
    gtk_return_if_fail (widget != NULL);

    
}

static void
gmui_positioner_size_allocate (GtkWidget *widget,
                             GtkAllocation *allocation)
{
}

static void
gmui_positioner_draw (GtkWidget *widget,
                    GdkRectangle *area)
{
    
}
