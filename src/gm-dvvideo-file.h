/*
 * gm-dvvideo-file.h: Raw DV video file implementation
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_DVVIDEO_FILE
#define _GM_DVVIDEO_FILE

#include <gtk/gtk.h>

#include "simpledv.h"

#include "gm-media-file.h"

#define GTK_TYPE_GM_DVVIDEO_FILE	(gm_dvvideo_file_get_type ())
#define GM_DVVIDEO_FILE(obj)		(GTK_CHECK_CAST ((obj), \
                                                         GTK_TYPE_GM_DVVIDEO_FILE, GMDVVideoFile))
#define GM_DVVIDEO_FILE_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), \
                                                               GTK_TYPE_GM_DVVIDEO_FILE, \
                                                               GMDVVideoFileClass))
#define IS_GM_DVVIDEO_FILE(obj)		(GTK_CHECK_TYPE ((obj), \
                                                         GTK_TYPE_GM_DVVIDEO_FILE))
#define IS_GM_DVVIDEO_FILE_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), \
                                                               GTK_TYPE_GM_DVVIDEO_FILE))
#define _CLASS(p)			(GM_DVVIDEO_FILE(GTK_OBJECT(p)->klass))

typedef struct _GMDVVideoFile GMDVVideoFile;
typedef struct _GMDVVideoFileClass GMDVVideoFileClass;

struct _GMDVVideoFile {
    GMMediaFile parent;

    DVFile *dvf;
};

struct _GMDVVideoFileClass {
    GMMediaFileClass parent_class;

    /* Signals */
};

guint gm_dvvideo_file_get_type (void);

GMDVVideoFile *gm_dvvideo_file_new (gchar *filename);

#endif /* _GM_DVVIDEO_FILE */
