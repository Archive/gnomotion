/*
 * gm-media-clip.c: MediaClip object for storing a specific stream
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include "misc.h"
#include "gm-media-clip.h"

static void gm_media_clip_class_init (GMMediaClipClass *klass);
static void gm_media_clip_init (GMMediaClip *obj);

static void real_change_notify (GMMediaClip *clip, GMMediaClipChangeType what,
                                gpointer change_data);

static GtkObjectClass *gm_media_clip_parent;

enum SIGNALS {
    CHANGE_NOTIFY,
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

guint
gm_media_clip_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMMediaClip",
            sizeof (GMMediaClip),
            sizeof (GMMediaClipClass),
            (GtkClassInitFunc) gm_media_clip_class_init,
            (GtkObjectInitFunc) gm_media_clip_init,
            (GtkArgSetFunc) NULL,
            (GtkArgGetFunc) NULL,
        };

        type = gtk_type_unique (gtk_object_get_type (), &type_info);
    }

    return type;
}

static void
gm_media_clip_destroy (GtkObject *obj)
{
    /* ? */
}

static void
gm_media_clip_class_init (GMMediaClipClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;

    object_class->destroy = gm_media_clip_destroy;

    gm_media_clip_parent = gtk_type_class (gtk_object_get_type ());

    /* run first, so that things which watch this get an updated obj */
    signals[CHANGE_NOTIFY] =
        gtk_signal_new ("change_notify",
                        GTK_RUN_FIRST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GMMediaClipClass, change_notify),
                        gtk_marshal_NONE__INT_POINTER,
                        GTK_TYPE_NONE, 2,
                        GTK_TYPE_INT,
                        GTK_TYPE_POINTER);

    gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

    klass->change_notify = real_change_notify;
}

GMMediaClip *
gm_media_clip_new (GMMediaStream *stream)
{
    GMMediaClip *new;

    g_assert (stream);

    new = GM_MEDIA_CLIP (gtk_type_new (gm_media_clip_get_type ()));

    new->stream = stream;
    new->in_frame = 0;
    new->out_frame = gm_media_stream_num_frames (stream) - 1;
    new->markers = NULL;

    return new;
}

GMMediaClip *
gm_media_clip_copy (GMMediaClip *clip)
{
    GMMediaClip *new;
    new = gm_media_clip_new (gm_media_stream_copy (clip->stream));
    new->in_frame = clip->in_frame;
    new->out_frame = clip->out_frame;
    new->poster_frame = clip->poster_frame;
    new->flags = clip->flags;
#warning should these be strdupd?
    new->name = g_strdup(clip->name);
    new->comment = g_strdup(clip->comment);

#warning copy markers and labels correctly!
    new->labels = clip->labels;
    new->markers = clip->markers;

    return new;
}

static void
gm_media_clip_init (GMMediaClip *gmc)
{
    /* .. */
}

GMFrameNum gm_media_clip_num_frames (GMMediaClip *clip)
{
    g_return_val_if_fail (clip != NULL, 0);

    return clip->stream->length;
}

GMFrameNum gm_media_clip_inout_num_frames (GMMediaClip *clip)
{
    g_return_val_if_fail (clip != NULL, 0);

    return clip->out_frame - clip->in_frame;
}

void gm_media_clip_get_inout_length (GMMediaClip *clip, GMLength *len)
{
    g_return_if_fail (clip != NULL);

    /* In everything, video is the dominant format -- if a clip has both video and audio,
     * all references to frames, etc., are video frames, rates, etc.
     */
    if (gm_media_stream_video_format (clip->stream)) {
        gm_framenum_to_length (clip->out_frame - clip->in_frame,
                               gm_media_stream_video_format (clip->stream)->stream_rate,
                               len);
    } else {
        gm_framenum_to_length (clip->out_frame - clip->in_frame,
                               gm_media_stream_audio_format (clip->stream)->stream_rate,
                               len);
    }        
}

void gm_media_clip_get_inout_timecode_length (GMMediaClip *clip, GMTimecode *tcode)
{
    g_return_if_fail (clip);

    /* In everything, video is the dominant format -- if a clip has both video and audio,
     * all references to frames, etc., are video frames, rates, etc.
     */
    if (gm_media_stream_video_format (clip->stream)) {
        gm_framenum_to_timecode (clip->out_frame - clip->in_frame,
                                 gm_media_stream_video_format (clip->stream)->stream_rate,
                                 tcode);
    } else {
        gm_framenum_to_timecode (clip->out_frame - clip->in_frame,
                                 gm_media_stream_audio_format (clip->stream)->stream_rate,
                                 tcode);
    }
}

gboolean gm_media_clip_set_in_frame (GMMediaClip *clip, GMFrameNum new_in)
{
    g_return_val_if_fail (clip != NULL, FALSE);

    if (new_in >= gm_media_stream_num_frames (clip->stream))
        return FALSE;

    gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_IN_CHANGED, &new_in);
    if (new_in > clip->out_frame) {
        gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_OUT_CHANGED, &new_in);
    }
    return TRUE;
}

gboolean gm_media_clip_set_out_frame (GMMediaClip *clip, GMFrameNum new_out)
{
    g_return_val_if_fail (clip != NULL, FALSE);

    if (new_out >= gm_media_stream_num_frames (clip->stream))
        return FALSE;

    gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_OUT_CHANGED, &new_out);
    if (new_out < clip->out_frame) {
        gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_IN_CHANGED, &new_out);
    }
    return TRUE;
}

gboolean gm_media_clip_set_poster_frame (GMMediaClip *clip, GMFrameNum new_poster)
{
    g_return_val_if_fail (clip != NULL, FALSE);

    if (new_poster >= gm_media_stream_num_frames (clip->stream))
        return FALSE;

    gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_POSTER_CHANGED,
                     &new_poster);
    return TRUE;
}

GdkPixbuf *gm_media_clip_get_poster_frame_pixbuf (GMMediaClip *clip, gint scale)
{
    const GdkPixbuf *ptr;
    GdkPixbuf *ret = NULL;
    gint w, h;

    /* NOTE: we can use gm_media_stream_render_video_frame for this, except that
     * it doesn't scale */

    g_return_val_if_fail (clip != NULL, NULL);
    g_return_val_if_fail (clip->stream != NULL, NULL);

    /* If no video, no poster */
    /* FIXME -- some pretty gfx of a sound wave should be made, or even a clip of the sound */
    if (gm_media_stream_video_format (clip->stream) == NULL)
        return NULL;

    ptr = gm_media_stream_get_video_pointer (clip->stream);
    if (!ptr) {
        return NULL; /* ? */
    }

    if (!gm_media_stream_seek_frame (clip->stream, clip->poster_frame)) {
        return NULL;
    }

    w = gdk_pixbuf_get_width (ptr) / scale;
    h = gdk_pixbuf_get_height (ptr) / scale;

    ret = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                          gdk_pixbuf_get_has_alpha (ptr),
                          gdk_pixbuf_get_bits_per_sample (ptr),
                          w, h);
    gdk_pixbuf_scale (ptr, ret,
                      0, 0, w, h,
                      0, 0, (double) 1.0 / scale, (double) 1.0 / scale,
                      GDK_INTERP_NEAREST);
    return ret;
}

void gm_media_clip_set_name (GMMediaClip *clip, gchar *new_name)
{
    g_return_if_fail (clip != NULL);
    gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_NAME_CHANGED,
                     new_name);
}

gchar *gm_media_clip_get_name (GMMediaClip *clip)
{
    g_return_val_if_fail (clip != NULL, NULL);
    return clip->name;
}

void gm_media_clip_set_comment (GMMediaClip *clip, gchar *new_cmnt)
{
    g_return_if_fail (clip != NULL);
    gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_COMMENT_CHANGED);
}

gchar *gm_media_clip_get_comment (GMMediaClip *clip)
{
    g_return_val_if_fail (clip != NULL, NULL);
    return clip->comment;
}

gint gm_media_clip_get_flags (GMMediaClip *clip)
{
    g_return_val_if_fail (clip != NULL, 0);
    return clip->flags;
}

void gm_media_clip_set_flag (GMMediaClip *clip, gint flag)
{
    g_return_if_fail (clip != NULL);
    if (clip->flags & flag)
        return;                 /* already set */
    gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_FLAG_SET, GINT_TO_POINTER (flag));
}

void gm_media_clip_unset_flag (GMMediaClip *clip, gint flag)
{
    g_return_if_fail (clip != NULL);
    if ((clip->flags & flag) == 0)
        return;                 /* already unset */
    gtk_signal_emit (GTK_OBJECT (clip), signals[CHANGE_NOTIFY], GM_CLIP_FLAG_UNSET, GINT_TO_POINTER (flag));
}

gchar *gm_media_clip_get_info_string (GMMediaClip *clip)
{
    GMVideoStreamFormat *vfmt;
    GMAudioStreamFormat *afmt;

    gchar *vret = NULL;
    gchar *aret = NULL;
    gchar *ret = NULL;

    g_return_val_if_fail (clip != NULL, NULL);

    vfmt = (GMVideoStreamFormat *) gm_media_stream_video_format (clip->stream);
    afmt = (GMAudioStreamFormat *) gm_media_stream_audio_format (clip->stream);

    if (vfmt) {
        GMTimecode tcd;
        gchar *tcdstr;
        gm_framenum_to_timecode (vfmt->stream_total_frames, vfmt->stream_rate,
                                 &tcd);
        tcdstr = gm_timecode_to_string (&tcd);
        vret = g_strdup_printf ("Video: Length: %s\n"
                                "Format: %s - %s", tcdstr,
                                GMVideoFormat_names[vfmt->source_format],
                                GMColorspace_names[vfmt->source_space]);
        g_free (tcdstr);
    }

    if (afmt) {
        GMTimecode tcd;
        gchar *tcdstr;
        gm_framenum_to_timecode (afmt->stream_total_frames, afmt->stream_rate,
                                 &tcd);
        tcdstr = gm_timecode_to_string (&tcd);
        aret = g_strdup_printf ("Audio: Length: %s\n"
                                "Format: %s - %s", tcdstr,
                                GMAudioFormat_names[afmt->source_format],
                                GMAudiospace_names[afmt->source_space]);
        g_free (tcdstr);
    }

    ret = g_strdup_printf ("%s%s%s",
                           vfmt ? vret : "",
                           vfmt && afmt ? "\n" : "",
                           afmt ? aret : "");
    return ret;
}

static void
real_change_notify (GMMediaClip *clip, GMMediaClipChangeType what,
                    gpointer change_data)
{
    gint flag = GPOINTER_TO_INT(change_data);
    GMFrameNum *fnp = (GMFrameNum *) change_data;

    switch (what) {
    case GM_CLIP_NAME_CHANGED:
        if (clip->name)
            g_free (clip->name);
        clip->name = g_strdup (change_data);
        break;
    case GM_CLIP_COMMENT_CHANGED:
        if (clip->comment)
            g_free (clip->comment);
        clip->comment = g_strdup (change_data);
        break;
    case GM_CLIP_FLAG_SET:
        clip->flags |= flag;
        break;
    case GM_CLIP_FLAG_UNSET:
        clip->flags &= ~flag;
        break;
    case GM_CLIP_POSTER_CHANGED:
        clip->poster_frame = *fnp;
        break;
    case GM_CLIP_IN_CHANGED:
        clip->in_frame = *fnp;
        break;
    case GM_CLIP_OUT_CHANGED:
        clip->out_frame = *fnp;
        break;
    default:
        g_warning ("Unknown change type in gm-media-clip change_notify!");
        break;
    }
}
