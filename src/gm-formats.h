/*
 * gm-formats.h: format definitions used in GM
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_FORMATS_H
#define _GM_FORMATS_H

#include "gm-types.h"

#define GM_FORMAT_BASE(obj)	((GMFormatBase *) obj)
#define GM_FORMAT_TYPE(obj)	(((GMFormatBase *) obj)->type)

typedef enum _GMFormatType {
    GM_FORMAT_AUDIO,
    GM_FORMAT_VIDEO
} GMFormatType;

typedef struct _GMFormatBase {
    GMFormatType type;
} GMFormatBase;

typedef enum _GMVideoFormat {
    GM_VIDEO_FORMAT_IMAGE,         /* raw images */
    GM_VIDEO_FORMAT_DV,            /* DV "blue book" format */
    GM_VIDEO_FORMAT_MJPEG_A,       /* M-JPEG A style (with JPEG markers) */
    GM_VIDEO_FORMAT_MJPEG_B,       /* M-JPEG B style (without JPEG markers) */
    GM_VIDEO_FORMAT_UNKNOWN,       /* some other library will do decoding to frames */
    GM_VIDEO_FORMAT_LAST
} GMVideoFormat;

typedef enum _GMColorspace {
    GM_COLORSPACE_GRAY,
    GM_COLORSPACE_YUV_422,         /* wishful thinking (but also upsampled 411) */
    GM_COLORSPACE_YUV_411,         /* NTSC DV std, also SMPTE PAL DV */
    GM_COLORSPACE_YUV_420,         /* PAL DV std */
    GM_COLORSPACE_RGB,             /* mostly for images */
    GM_COLORSPACE_RGBA,            /* mostly for images */
    GM_COLORSPACE_UNKNOWN,
    GM_COLORSPACE_LAST
} GMColorspace;

typedef enum _GMVideoFrameFormat {
    GM_VIDEO_FRAME_FORMAT_RAW,     /* Raw data (in VideoFormat format) */
    GM_VIDEO_FRAME_FORMAT_RGB24,   /* 24-bit RGB packed-pixel data */
    GM_VIDEO_FRAME_FORMAT_RGB32,   /* 32-bit RGB0 data */
    GM_VIDEO_FRAME_FORMAT_PIXBUF,  /* GdkPixbuf image format */
    GM_VIDEO_FRAME_FORMAT_LAST
} GMVideoFrameFormat;

typedef struct _GMVideoStreamFormat {
    GMFormatBase base;
    
    /* Actual data format */
    GMVideoFormat source_format;
    GMColorspace source_space;

    GMVideoFrameFormat frame_format;
    GMColorspace frame_space;

    guint32 width;
    guint32 height;
    guint32 rowstride;
    guint8 bits_per_pixel;       /* bpp for RGB/RGBA images */

    /* Stream info */
    GMRate stream_rate;
    GMFrameNum stream_total_frames;
} GMVideoStreamFormat;

typedef enum _GMAudioFormat {
    GM_AUDIO_FORMAT_RAW,
    GM_AUDIO_FORMAT_PCM,
    GM_AUDIO_FORMAT_ULAW,
    GM_AUDIO_FORMAT_UNKNOWN,
    GM_AUDIO_FORMAT_LAST
} GMAudioFormat;

typedef enum _GMAudioFrameFormat {
    GM_AUDIO_FRAME_FORMAT_RAW,     /* raw audio samples */
    GM_AUDIO_FRAME_UNKNOWN,
    GM_AUDIO_FRAME_FORMAT_LAST
} GMAudioFrameFormat;


/* Note that 8 is NOT a DV audio format -- it's there to support legacy
 * PCM/uLaw audio data
 */

typedef enum _GMAudiospace {
    GM_AUDIOSPACE_8_MONO,
    GM_AUDIOSPACE_8_STEREO,        /* 16 bits per sample */
    GM_AUDIOSPACE_12_MONO,
    GM_AUDIOSPACE_12_STEREO,       /* 24 bits per sample */
    GM_AUDIOSPACE_16_MONO,
    GM_AUDIOSPACE_16_STEREO,       /* 32 bits per sample */
    GM_AUDIOSPACE_UNKNOWN,
    GM_AUDIOSPACE_LAST
} GMAudiospace;

typedef struct _GMAudioStreamFormat {
    GMFormatBase base;
    
    GMAudioFormat source_format;
    GMAudiospace source_space;

    GMAudioFrameFormat frame_format;
    GMAudiospace frame_space;
    unsigned long frame_size;

    GMRate stream_rate;
    GMFrameNum stream_total_frames; /* total number of frames */
    gboolean locked;            /* is this locked audio? */
    guint32 samples_per_frame;  /* number of samples in each frame -- only valid for */
                                /* locked audio!! */
} GMAudioStreamFormat;

extern const gchar *GMVideoFormat_names[GM_VIDEO_FORMAT_LAST];
extern const gchar *GMColorspace_names[GM_COLORSPACE_LAST];
extern const gchar *GMVideoFrameFormat_names[GM_VIDEO_FRAME_FORMAT_LAST];
extern const gchar *GMAudioFormat_names[GM_AUDIO_FORMAT_LAST];
extern const gchar *GMAudioFrameFormat_names[GM_AUDIO_FRAME_FORMAT_LAST];
extern const gchar *GMAudiospace_names[GM_AUDIOSPACE_LAST];

void gm_video_format_dump (GMVideoStreamFormat *gvf);
void gm_audio_format_dump (GMAudioStreamFormat *gaf);

#endif /* _GM_FORMATS_H */
