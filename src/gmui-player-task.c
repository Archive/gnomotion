/*
 * gmui-player-task.c: Implements a mainloop task for playing a stream
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>

#include <ao/ao.h>

#include "gmui-player-task.h"

gboolean player_task_func (gpointer data);
void player_task_destroy (gpointer data);

static gboolean player_da_configure (GtkWidget *da, GdkEventConfigure *event,
                                     gpointer user_data);
static void player_set_wh (GMUIPlayerTaskData *ptd, gint width, gint height);

struct _GMUIPlayerTaskPrivate {
    GMUIPlayerTaskID my_id;
    GMRate rate;

    gint v_width, v_height;
    double v_x_scale, v_y_scale;

    GdkPixbuf *v_src;
    GdkPixbuf *v_dst;

    void *a_src; /* ?? */
    ao_device_t *aod;
    unsigned long a_src_len;

    gint viewer_configure_id;
};

GMUIPlayerTaskID
gmui_player_task_new (GMUIPlayerTaskData *data)
{
    GMUIPlayerTaskPrivate *priv;
    GMRate stream_rate = 0.0;

    g_return_val_if_fail (data != NULL, 0);

    priv = g_new0 (GMUIPlayerTaskPrivate, 1);
    data->priv = priv;

    if (data->play_what & GMUI_PLAYER_TASK_PLAY_VIDEO) {
        g_return_val_if_fail (gm_media_stream_video_format (data->stream) != NULL, 0);
        priv->v_src = gm_media_stream_get_video_pointer (data->stream);
        stream_rate = gm_media_stream_video_format (data->stream)->stream_rate;

        priv->viewer_configure_id = gtk_signal_connect
            (GTK_OBJECT (data->drawing), "configure_event",
             GTK_SIGNAL_FUNC (player_da_configure), data);
        
        player_set_wh (data, GTK_WIDGET (data->drawing)->allocation.width,
                       GTK_WIDGET (data->drawing)->allocation.height);
    }

    if (data->play_what & GMUI_PLAYER_TASK_PLAY_AUDIO) {
        const GMAudioStreamFormat *gas = gm_media_stream_audio_format (data->stream);
        g_return_val_if_fail (gas != NULL, 0);
        /* UHH. no */
//        if ((data->play_what & GMUI_PLAYER_TASK_PLAY_VIDEO) == 0)
//            stream_rate = gm_media_stream_audio_format (data->stream)->stream_rate;
        ao_initialize ();
        /* FIXME FIXME -- hack(8) */
        switch (gas->frame_space) {
            case GM_AUDIOSPACE_8_MONO:
                priv->aod = ao_open (ao_get_driver_id ("oss"), 8, gas->stream_rate,
                                     1, NULL);
                break;
            case GM_AUDIOSPACE_8_STEREO:
                priv->aod = ao_open (ao_get_driver_id ("oss"), 8, gas->stream_rate,
                                     2, NULL);
                break;
            case GM_AUDIOSPACE_12_MONO:
                priv->aod = ao_open (ao_get_driver_id ("oss"), 12, gas->stream_rate,
                                     1, NULL);
                break;
            case GM_AUDIOSPACE_12_STEREO:
                priv->aod = ao_open (ao_get_driver_id ("oss"), 12, gas->stream_rate,
                                     2, NULL);
                break;
            case GM_AUDIOSPACE_16_MONO:
                priv->aod = ao_open (ao_get_driver_id ("oss"), 16, gas->stream_rate,
                                     1, NULL);
                break;
            case GM_AUDIOSPACE_16_STEREO:
                priv->aod = ao_open (ao_get_driver_id ("oss"), 16, gas->stream_rate,
                                     2, NULL);
                break;
            case GM_AUDIOSPACE_UNKNOWN:
            case GM_AUDIOSPACE_LAST: /* to shut gcc up */
                priv->aod = NULL;
                break;
        }

        if (priv->aod == NULL) {
            g_warning ("audiospace %s not supported by AO! ignoring audio...",
                       GMAudiospace_names[gas->frame_space]);
            data->play_what &= ~GMUI_PLAYER_TASK_PLAY_AUDIO;
        } else {
            priv->a_src = gm_media_stream_get_audio_pointer (data->stream);
            priv->a_src_len = GM_MEDIA_STREAM (data->stream)->audiobuf_samples *
                sizeof (guint16);
        }
    }

    if (data->loop) {
        gm_media_stream_seek_frame (data->stream, data->start_at);
        if (data->cursor) {
            gm_cursor_move (data->cursor, data->start_at);
        }
    } else if (data->cursor) {
        gm_media_stream_seek_frame (data->stream, gm_cursor_frame_num (data->cursor));
    }

    priv->rate = stream_rate;

    priv->my_id = g_timeout_add_full (G_PRIORITY_DEFAULT - 50,
                                      (guint) (1000.0 / stream_rate),
/*                                      10.0, */
                                      player_task_func,
                                      data,
                                      player_task_destroy);
    
    return priv->my_id;
}

gboolean
gmui_player_task_destroy (GMUIPlayerTaskID which)
{
    return g_source_remove (which);
}

void
player_task_destroy (gpointer data)
{
    GMUIPlayerTaskData *ptd = (GMUIPlayerTaskData *) data;
    GMUIPlayerTaskPrivate *priv = ptd->priv;

    /* get the end frame */
    ptd->cursor->fnum--;

    if (ptd->finished_func)
        (* ptd->finished_func) (ptd);

    if (priv->v_dst)
        gdk_pixbuf_unref (priv->v_dst);
    if (priv->v_src)
        gdk_pixbuf_unref (priv->v_src);

    gtk_signal_disconnect (GTK_OBJECT (ptd->drawing),
                           priv->viewer_configure_id);

    g_free (priv);
    g_free (ptd);
}    

static gboolean
player_da_configure (GtkWidget *widget, GdkEventConfigure *event,
                     gpointer user_data)
{
    GMUIPlayerTaskData *ptd = (GMUIPlayerTaskData *) user_data;

    g_return_val_if_fail (ptd->priv != NULL, FALSE);
    
    player_set_wh (ptd, event->width, event->height);
    return FALSE;
}

static void
player_set_wh (GMUIPlayerTaskData *ptd, gint width, gint height)
{
    GMUIPlayerTaskPrivate *priv = ptd->priv;

    gboolean changed = FALSE;

    if (width != ptd->priv->v_width) {
        changed = TRUE;
        ptd->priv->v_width = width;
    }

    if (height != ptd->priv->v_height) {
        changed = TRUE;
        ptd->priv->v_height = height;
    }

    if (changed) {
        if (priv->v_dst)
            gdk_pixbuf_unref (priv->v_dst);
        priv->v_dst = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                                      gdk_pixbuf_get_has_alpha (priv->v_src),
                                      gdk_pixbuf_get_bits_per_sample (priv->v_src),
                                      width, height);
        priv->v_x_scale = (double) width / gdk_pixbuf_get_width (priv->v_src);
        priv->v_y_scale = (double) height / gdk_pixbuf_get_height (priv->v_src);
    }
}
gboolean
player_task_func (gpointer data)
{
    GMUIPlayerTaskData *ptd = (GMUIPlayerTaskData *) data;
    GMUIPlayerTaskPrivate *priv = ptd->priv;

    if (ptd->play_what & GMUI_PLAYER_TASK_PLAY_VIDEO) {
        GdkPixmap *pix;

        gdk_pixbuf_scale (priv->v_src, priv->v_dst, 0, 0,
                          priv->v_width, priv->v_height,
                          0, 0,
                          priv->v_x_scale, priv->v_y_scale,
                          GDK_INTERP_BILINEAR);

        gdk_pixbuf_render_to_drawable
            (priv->v_dst, ptd->drawing->window,
             ptd->drawing->style->fg_gc[ptd->drawing->state],
             0, 0, 0, 0, priv->v_width, priv->v_height,
             GDK_RGB_DITHER_NORMAL,
             0, 0);
    }

    if (ptd->play_what & GMUI_PLAYER_TASK_PLAY_AUDIO) {
        ao_play (priv->aod, priv->a_src, priv->a_src_len);
    }


    if (ptd->cursor)
        gm_cursor_incr (ptd->cursor);

    if (gm_media_stream_next_frame (ptd->stream) == FALSE ||
        (ptd->stop_at && ptd->stream->cur_frame == ptd->stop_at))
    {
        if (ptd->loop) {
            /* go back to start frame */
            gm_media_stream_seek_frame (ptd->stream, ptd->start_at);
            gm_cursor_move (ptd->cursor, ptd->start_at);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    return TRUE;
}
