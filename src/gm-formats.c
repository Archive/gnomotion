/*
 * gm-formats.c: format definitions used in GM
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

/* Just a few const things */

#include <stdio.h>

#include "gm-formats.h"

const gchar *GMVideoFormat_names[GM_VIDEO_FORMAT_LAST] = {
    "Raw Image",
    "DV",
    "MJPEG-A",
    "MJPEG-B",
    "Unknown"
};

const gchar *GMColorspace_names[GM_COLORSPACE_LAST] = {
    "Grayscale",
    "YUV (4:2:2)",
    "YUV (4:1:1)",
    "YUV (4:2:0)",
    "RGB",
    "RGBA",
    "Unknown"
};

const gchar *GMVideoFrameFormat_names[GM_VIDEO_FRAME_FORMAT_LAST] = {
    "Raw data",
    "RGB (24bit)",
    "RGB (32bit)",
    "GdkPixbuf"
};

const gchar *GMAudioFormat_names[GM_AUDIO_FORMAT_LAST] = {
    "Raw Data",
    "PCM",
    "uLaw",
    "Unknown"
};

const gchar *GMAduioFrameFormat_names[GM_AUDIO_FRAME_FORMAT_LAST] = {
    "Raw Data",
    "Unknown"
};

const gchar *GMAudiospace_names[GM_AUDIOSPACE_LAST] = {
    "8-bit (mono)",
    "8-bit (stereo)",
    "12-bit (mono)",
    "12-bit (stereo)",
    "16-bit (mono)",
    "16-bit (stereo)",
    "Unknown"
};



void
gm_video_format_dump (GMVideoStreamFormat *gvf)
{
    fprintf (stderr, "Video Format:\n");
    fprintf (stderr, "   source format: %s\n", GMVideoFormat_names[gvf->source_format]);
    fprintf (stderr, "    source space: %s\n", GMColorspace_names[gvf->source_space]);
    fprintf (stderr, "    frame format: %s\n", GMVideoFrameFormat_names[gvf->frame_format]);
    fprintf (stderr, "     frame space: %s\n", GMColorspace_names[gvf->frame_space]);
    fprintf (stderr, "      frame size: width: %d height: %d\n", gvf->width, gvf->height);
    fprintf (stderr, " frame rowstride: %d\n", gvf->rowstride);
    fprintf (stderr, "       frame bpp: %d\n", gvf->bits_per_pixel);
    fprintf (stderr, "     stream rate: %f\n", gvf->stream_rate);
    fprintf (stderr, "   stream frames: %ld\n", gvf->stream_total_frames);
}


void
gm_audio_format_dump (GMAudioStreamFormat *gaf)
{
    fprintf (stderr, "Audio Format:\n");
    fprintf (stderr, "---eeek write me----\n");
}
