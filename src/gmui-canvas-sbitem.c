#include <stdio.h>

#include <math.h>

#include <glib.h>
#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>
#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_affine.h>
#include <libart_lgpl/art_pixbuf.h>
#include <libart_lgpl/art_rgb_pixbuf_affine.h>

#include "gmui-canvas-sbitem.h"


static void gmui_canvas_sbitem_class_init (GMUICanvasSBItemClass *klass);
static void gmui_canvas_sbitem_init (GMUICanvasSBItem *sbi);
static void gmui_canvas_sbitem_destroy (GtkObject *obj);
static void gmui_canvas_sbitem_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id);
static void gmui_canvas_sbitem_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id);

static void gmui_canvas_sbitem_update (GnomeCanvasItem *item, double *affine,
                                       ArtSVP *clip_path, int flags);
static void gmui_canvas_sbitem_draw (GnomeCanvasItem *item, GdkDrawable *drawable,
                                     int x, int y, int width, int height);
static void gmui_canvas_sbitem_render (GnomeCanvasItem *item, GnomeCanvasBuf *buf);
static double gmui_canvas_sbitem_point (GnomeCanvasItem *item, double x, double y,
                                        int cx, int cy, GnomeCanvasItem **actual_item);
static void gmui_canvas_sbitem_bounds (GnomeCanvasItem *item, double *x1, double *y1,
                                       double *x2, double *y2);

static void
get_bounds (GMUICanvasSBItem *sbi, double *px1, double *py1, double *px2, double *py2);

static GnomeCanvasItemClass *gmui_canvas_sbitem_parent;

#define ITEM_W 100.0
#define ITEM_H 100.0

/* Arg IDs */
enum {
    ARG_0,
    ARG_CLIP,
    ARG_X,
    ARG_Y
};

guint
gmui_canvas_sbitem_get_type (void)
{
    static guint type = 0;
    if (!type) {
        static const GtkTypeInfo typeinfo = {
            "GMUICanvasSBItem",
            sizeof (GMUICanvasSBItem),
            sizeof (GMUICanvasSBItemClass),
            (GtkClassInitFunc) gmui_canvas_sbitem_class_init,
            (GtkObjectInitFunc) gmui_canvas_sbitem_init,
            NULL,
            NULL,
            NULL
        };

        type = gtk_type_unique (gnome_canvas_item_get_type (),
                                &typeinfo);
    }

    return type;
}

static void
gmui_canvas_sbitem_class_init (GMUICanvasSBItemClass *klass)
{
    GtkObjectClass *obj_class = (GtkObjectClass *) klass;
    GnomeCanvasItemClass *ci_class = (GnomeCanvasItemClass *) klass;

    gmui_canvas_sbitem_parent = gtk_type_class (gnome_canvas_item_get_type ());

    gtk_object_add_arg_type ("GMUICanvasSBItem::clip",
                             GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_CLIP);
    gtk_object_add_arg_type ("GMUICanvasSBItem::x",
                             GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X);
    gtk_object_add_arg_type ("GMUICanvasSBItem::y",
                             GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y);

    obj_class->destroy = gmui_canvas_sbitem_destroy;
    obj_class->set_arg = gmui_canvas_sbitem_set_arg;
    obj_class->get_arg = gmui_canvas_sbitem_get_arg;

    ci_class->update = gmui_canvas_sbitem_update;
    ci_class->draw = gmui_canvas_sbitem_draw;
    ci_class->render = gmui_canvas_sbitem_render;
    ci_class->point = gmui_canvas_sbitem_point;
    ci_class->bounds = gmui_canvas_sbitem_bounds;
}

static void
gmui_canvas_sbitem_init (GMUICanvasSBItem *sbi)
{
    sbi->clip = NULL;
}

static void
gmui_canvas_sbitem_destroy (GtkObject *obj)
{
    GMUICanvasSBItem *sbi;

    g_return_if_fail (obj != NULL);
    g_return_if_fail (IS_GMUI_CANVAS_SBITEM (obj));

    sbi = GMUI_CANVAS_SBITEM (obj);

    if (sbi->clip) {
        gtk_object_unref (GTK_OBJECT (sbi->clip));
        sbi->clip = NULL;
    }

    if (GTK_OBJECT_CLASS (gmui_canvas_sbitem_parent)->destroy)
        (* GTK_OBJECT_CLASS (gmui_canvas_sbitem_parent)->destroy) (obj);
}

static void
gmui_canvas_sbitem_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
    GMUICanvasSBItem *sbi;

    GMMediaClip *clip;

    g_return_if_fail (obj != NULL);
    g_return_if_fail (IS_GMUI_CANVAS_SBITEM (obj));

    sbi = GMUI_CANVAS_SBITEM (obj);

    switch (arg_id) {
        case ARG_CLIP:
            clip = GTK_VALUE_POINTER (*arg);

            if (clip != sbi->clip) {
                if (sbi->clip) {
                    gtk_object_unref (GTK_OBJECT (sbi->clip));
                }
                sbi->clip = clip;
            }
            
            gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (obj));
            break;
        case ARG_X:
            sbi->x = GTK_VALUE_DOUBLE (*arg);
            fprintf (stderr, "sbi->x set to %f\n", sbi->x);
            gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (obj));
            break;
        case ARG_Y:
            sbi->y = GTK_VALUE_DOUBLE (*arg);
            fprintf (stderr, "sbi->y set to %f\n", sbi->y);
            gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (obj));
            break;
        default:
            break;
    }
}

static void
gmui_canvas_sbitem_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
    GMUICanvasSBItem *sbi;

    g_return_if_fail (obj != NULL);
    g_return_if_fail (IS_GMUI_CANVAS_SBITEM (obj));

    sbi = GMUI_CANVAS_SBITEM (obj);

    switch (arg_id) {
        case ARG_CLIP:
            GTK_VALUE_POINTER (*arg) = sbi->clip;
        default:
            arg->type = GTK_TYPE_INVALID;
            break;
    }
}

static void gmui_canvas_sbitem_update (GnomeCanvasItem *item, double *affine,
                                       ArtSVP *clip_path, int flags)
{
    GMUICanvasSBItem *sbi = GMUI_CANVAS_SBITEM (item);
    ArtDRect i_bbox, c_bbox;
    double x0, y0, x1, y1;
    
    if (gmui_canvas_sbitem_parent->update)
        (* gmui_canvas_sbitem_parent->update) (item, affine, clip_path, flags);

    /*
    art_drect_affine_transform (&c_bbox, &i_bbox, affine);
    */
    get_bounds (item, &x0, &y0, &x1, &y1);
    gnome_canvas_update_bbox (item, x0, y0, x1, y1);
    
//    fprintf (stderr, "sbix: %f sbiy: %f x1: %f y1: %f x2: %f y2: %f\n", sbi->x, sbi->y, item->x1, item->y1, item->x2, item->y2);

}

static void
get_bounds (GMUICanvasSBItem *sbi, double *px1, double *py1, double *px2, double *py2)
{
	GnomeCanvasItem *item;
	double x1, y1, x2, y2;
	int cx1, cy1, cx2, cy2;

	item = GNOME_CANVAS_ITEM (sbi);

	x1 = sbi->x;
	y1 = sbi->y;
	x2 = x1 + ITEM_W;
	y2 = y1 + ITEM_H;

	gnome_canvas_item_i2w (item, &x1, &y1);
	gnome_canvas_item_i2w (item, &x2, &y2);
	gnome_canvas_w2c (item->canvas, x1, y1, &cx1, &cy1);
	gnome_canvas_w2c (item->canvas, x2, y2, &cx2, &cy2);
	*px1 = cx1;
	*py1 = cy1;
	*px2 = cx2;
	*py2 = cy2;
}

static void
gmui_canvas_sbitem_draw (GnomeCanvasItem *item, GdkDrawable *drawable,
                         int x, int y, int width, int height)
{
    GMUICanvasSBItem *sbi = GMUI_CANVAS_SBITEM (item);
    GdkGC *gc;
    GdkColor color;
    double i2w[6], w2c[6], i2c[6];
    int x1, y1, x2, y2;
    ArtPoint i1, i2;
    ArtPoint c1, c2;

    gnome_canvas_item_i2w_affine (item, i2w);
    gnome_canvas_w2c_affine (item->canvas, w2c);
    art_affine_multiply (i2c, i2w, w2c);

    i1.x = sbi->x;
    i1.y = sbi->y;
    i2.x = sbi->x + ITEM_W;
    i2.y = sbi->y + ITEM_H;
    art_affine_point (&c1, &i1, i2c);
    art_affine_point (&c2, &i2, i2c);
    x1 = c1.x;
    y1 = c1.y;
    x2 = c2.x;
    y2 = c2.y;

#if 0
    g_warning ("Draw");
#endif
    gc = gdk_gc_new (drawable);
    gdk_color_parse ("#ff0000", &color);
    gdk_gc_set_foreground (gc, &color);
    gdk_gc_set_background (gc, &color);
    gdk_draw_rectangle (drawable, gc, TRUE, x1 - x, y1 - y, x2 - x1 + 1, y2 - y1 + 1);

#if 1
    fprintf (stderr, "Drawing [x %d y %d w %d h %d]\n", x, y, width, height);
#endif
    gdk_gc_unref (gc);
}

static void
gmui_canvas_sbitem_render (GnomeCanvasItem *item, GnomeCanvasBuf *buf)
{
    g_warning ("render called on canvas sbitem (only Gdk support)");
}

static double
gmui_canvas_sbitem_point (GnomeCanvasItem *item, double x, double y, int cx, int cy,
                          GnomeCanvasItem **actual_item)
{
    GMUICanvasSBItem *sbi;
    double x1, y1, x2, y2;
    double dx, dy;

    sbi = GMUI_CANVAS_SBITEM(item);

    *actual_item = item;

    gnome_canvas_c2w (item->canvas, sbi->x, sbi->y, &x1, &y1);

    x2 = x1 + (ITEM_W - 1) / item->canvas->pixels_per_unit;
    y2 = y1 + (ITEM_H - 1) / item->canvas->pixels_per_unit;
    
    /* is point inside? */    
    if ((x >= x1) && (y >= y1) && (x <= x2) && (y <= y2))
        return 0.0;

    /* Point is outside */
    if (x < x1)
        dx = x1 - x;
    else if (x > x2)
        dx = x - x2;
    else
        dx = 0.0;

    if (y < y1)
        dy = y1 - y;
    else if (y > y2)
        dy = y - y2;
    else
        dy = 0.0;

    return sqrt (dx * dx + dy * dy);
}

static void
gmui_canvas_sbitem_bounds (GnomeCanvasItem *item, double *x1, double *y1,
                           double *x2, double *y2)
{
    GMUICanvasSBItem *sbi;

    sbi = GMUI_CANVAS_SBITEM(item);

    *x1 = sbi->x;
    *y1 = sbi->y;

    *x2 = *x1 + ITEM_W;
    *y2 = *y1 + ITEM_H;
}


    
