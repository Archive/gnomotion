/*
 * gm-cursor.c: Generic stream position
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gm-cursor.h"
#include "misc.h"

static void gm_cursor_class_init (GMCursorClass *klass);
static void gm_cursor_init (GMCursor *obj);

static void real_cursor_changed (GMCursor *cursor, GMMediaStream *stream,
                                 GMFrameNum fnum);

static GtkObjectClass *gm_cursor_parent;

enum SIGNALS {
    CURSOR_CHANGED,
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

guint
gm_cursor_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMCursor",
            sizeof (GMCursor),
            sizeof (GMCursorClass),
            (GtkClassInitFunc) gm_cursor_class_init,
            (GtkObjectInitFunc) gm_cursor_init,
            NULL,
            NULL,
            NULL
        };

        type = gtk_type_unique (GTK_TYPE_OBJECT, &type_info);
    }

    return type;
}

static void
gm_cursor_class_init (GMCursorClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;

    gm_cursor_parent = gtk_type_class (GTK_TYPE_OBJECT);

    signals[CURSOR_CHANGED] =
        gtk_signal_new ("cursor_changed",
                        GTK_RUN_LAST,
                        object_class->type,
                        GTK_SIGNAL_OFFSET (GMCursorClass, cursor_changed),
                        gtk_marshal_NONE__POINTER_ULONG,
                        GTK_TYPE_NONE, 2,
                        GTK_TYPE_POINTER,
                        GTK_TYPE_ULONG);

    gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

    klass->cursor_changed = real_cursor_changed;
}

static void
gm_cursor_init (GMCursor *obj)
{
    /* ... */
}

GMCursor *
gm_cursor_new (GMMediaStream *stream, GMFrameNum fnum)
{
    GMCursor *new;

    g_return_val_if_fail (stream != NULL, NULL);

    new = gtk_type_new (gm_cursor_get_type ());
    new->stream = stream;
    new->fnum = fnum;
    new->fnum_max = stream->length;
    return new;
}

static void
real_cursor_changed (GMCursor *cursor, GMMediaStream *stream, GMFrameNum fnum)
{
    if (cursor->stream != stream)
        cursor->fnum = stream->length;
    cursor->stream = stream;
    cursor->fnum = fnum;
#ifdef DEBUG_CURSOR
    fprintf (stderr, "cursor set: 0x%8p %ld\n", stream, fnum);
#endif
}

void
gm_cursor_set (GMCursor *cursor, GMMediaStream *stream, GMFrameNum fnum)
{
    g_return_if_fail (stream != NULL);

    if (fnum < 0)
        return;
    if (fnum >= cursor->fnum_max) {
        if (stream != cursor->stream) {
            /* ?? fix the bloody fnum */
            gtk_signal_emit (GTK_OBJECT (cursor), signals[CURSOR_CHANGED],
                             stream, stream->length - 1);
        }
        return;
    }

    gtk_signal_emit (GTK_OBJECT (cursor), signals[CURSOR_CHANGED],
                     stream, fnum);
}

void
gm_cursor_move (GMCursor *cursor, GMFrameNum fnum)
{
    gm_cursor_set (cursor, cursor->stream, fnum);
}

void gm_cursor_incr (GMCursor *cursor)
{
    gm_cursor_set (cursor, cursor->stream, cursor->fnum + 1);
}

void gm_cursor_decr (GMCursor *cursor)
{
    gm_cursor_set (cursor, cursor->stream, cursor->fnum - 1);
}

GMMediaStream *gm_cursor_stream (GMCursor *cursor)
{
    g_return_val_if_fail (cursor != NULL, NULL);

    return cursor->stream;
}

GMFrameNum gm_cursor_frame_num (GMCursor *cursor)
{
    g_return_val_if_fail (cursor != NULL, 0);
#ifdef DEBUG_CURSOR
    fprintf (stderr, "cursor get: 0x%8p [%ld]\n", cursor->stream, cursor->fnum);
#endif
    return cursor->fnum;
}
