/*
 * gmui-bin.h: GM UI Bin class
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GMUI_BIN_H
#define _GMUI_BIN_H

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-media-clip.h"

#define GMUI_BIN_CLIP_WIDTH		72
#define GMUI_BIN_CLIP_HEIGHT 		48


#define GTK_TYPE_GMUI_BIN		(gmui_bin_get_type ())
#define GMUI_BIN(obj)			(GTK_CHECK_CAST ((obj), \
                                                         GTK_TYPE_GMUI_BIN, \
                                                         GMUIBin))
#define GMUI_BIN_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), \
                                                         GTK_TYPE_GMUI_BIN, \
                                                         GMUIBinClass))
#define IS_GMUI_BIN(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_GMUI_BIN))
#define IS_GMUI_BIN_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GMUI_BIN))

typedef enum _GMUIBinItemType {
    GMUI_BIN_ITEM_UNKNOWN,
    GMUI_BIN_ITEM_CLIP,         /* a GMUIMediaPointer */
    GMUI_BIN_ITEM_FOLDER        /* another Bin */
} GMUIBinItemType;

typedef struct _GMUIBinItem {
    GMUIBinItemType type;
    gpointer data;

    gchar *title;
    gchar *other_info;
} GMUIBinItem;

typedef struct _GMUIBin GMUIBin;
typedef struct _GMUIBinClass GMUIBinClass;

struct _GMUIBin {
    GtkCList parent;

    GSList *bin_items;
};

struct _GMUIBinClass {
    GtkCListClass parent_class;

    /* Nothing else */
};

guint gmui_bin_get_type (void);

GMUIBin *gmui_bin_new (void);
GMUIBin *gmui_bin_new_glade (gchar *widget_name, gchar *string1, gchar *string2,
                             gint int1, gint int2);

/* Returns index of row */
gint gmui_bin_add_clip (GMUIBin *bin, GMMediaClip *clip);
gboolean gmui_bin_remove_clip (GMUIBin *bin, GMMediaClip *clip);
gboolean gmui_bin_remove_clip_row (GMUIBin *bin, gint row_num);


#endif /* _GMUI_BIN_H */
