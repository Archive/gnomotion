/*
 * gm-streamcontainer.h: Something that can hand out streams.
 *			       Abstract class.
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_STREAMCONTAINER_H
#define _GM_STREAMCONTAINER_H

#include <gtk/gtk.h>

#include "gm-types.h"

/* Silly C */
typedef struct _GMStreamContainer GMStreamContainer;
typedef struct _GMStreamContainerClass GMStreamContainerClass;

#include "gm-media-stream.h"

#define GTK_TYPE_GM_STREAMCONTAINER		(gm_streamcontainer_get_type ())
#define GM_STREAMCONTAINER(obj)		        (GTK_CHECK_CAST ((obj), \
                                                 GTK_TYPE_GM_STREAMCONTAINER, GMStreamContainer))
#define GM_STREAMCONTAINER_CLASS(klass)	        (GTK_CHECK_CLASS_CAST ((klass), \
                                                 GTK_TYPE_GM_STREAMCONTAINER, \
                                                 GMStreamContainerClass))
#define IS_GM_STREAMCONTAINER(obj)	        (GTK_CHECK_TYPE ((obj), \
                                                 GTK_TYPE_GM_STREAMCONTAINER))
#define IS_GM_STREAMCONTAINER_CLASS(klass)      (GTK_CHECK_CLASS_TYPE ((klass), \
                                                 GTK_TYPE_GM_STREAMCONTAINER))

struct _GMStreamContainer {
    GtkObject parent;
};

struct _GMStreamContainerClass {
    GtkObjectClass parent_class;

    /* Signals */

    /* Virtual functions */

    /* Return the number of video/audio streams in this container
     * and functions to access them
     */
    GMFrameNum (*gm_streamcontainer_stream_frame_length) (GMStreamContainer *gsc,
                                                          guint stream_num);
    GMRate (*gm_streamcontainer_stream_rate) (GMStreamContainer *gsc, guint stream_num);
    guint (*gm_streamcontainer_num_streams) (GMStreamContainer *gsc);
    GMMediaStream * (*gm_streamcontainer_stream) (GMStreamContainer *gsc, guint stream_num);
    
    /* Functions used by streams to get the actual data */
    /* it's not clear what the API for this should look like exactly, but this */
    /* seems to be the right idea. */
    /* Note that the format functions will assume native format, and won't return all fmts */
    GMVideoStreamFormat * (*gm_streamcontainer_stream_video_format) (GMStreamContainer *gsc,
                                                                     guint stream_num);
    GMAudioStreamFormat * (*gm_streamcontainer_stream_audio_format) (GMStreamContainer *gsc,
                                                                     guint stream_num);
    void * (*gm_streamcontainer_stream_get_video_frame_data) (GMStreamContainer *gsc,
                                                              guint stream_num,
                                                              GMFrameNum fnum);
    void * (*gm_streamcontainer_stream_get_audio_frame_data) (GMStreamContainer *gsc,
                                                              guint stream_num,
                                                              GMFrameNum fnum,
                                                              long samples);
};


guint gm_streamcontainer_get_type (void);

GMStreamContainer *gm_streamcontainer_new (void);

/* Virtual function wrappers */
GMFrameNum gm_streamcontainer_stream_frame_length (GMStreamContainer *gsc, guint stream_num);

GMRate gm_streamcontainer_stream_rate (GMStreamContainer *gsc, guint stream_num);
    
guint gm_streamcontainer_num_streams (GMStreamContainer *gsc);

GMMediaStream *gm_streamcontainer_stream (GMStreamContainer *gsc, guint stream_num);

GMVideoStreamFormat *gm_streamcontainer_stream_video_format (GMStreamContainer *gsc,
                                                             guint stream_num);
GMAudioStreamFormat *gm_streamcontainer_stream_audio_format (GMStreamContainer *gsc,
                                                             guint stream_num);
void *gm_streamcontainer_stream_get_video_frame_data (GMStreamContainer *gsc,
                                                      guint stream_num,
                                                      GMFrameNum fnum);
void *gm_streamcontainer_stream_get_audio_frame_data (GMStreamContainer *gsc,
                                                      guint stream_num,
                                                      GMFrameNum fnum,
                                                      long samples);

#endif /* _GM_STREAMCONTAINER_H */
