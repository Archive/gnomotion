/*
 * gm-dvvideo-file.c: Raw DV video file implementation
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-dvvideo-file.h"

static void gm_dvvideo_file_class_init (GMDVVideoFileClass *klass);
static void gm_dvvideo_file_init (GMDVVideoFile *obj);

static GMMediaFileStatus dv_gm_media_file_load_from_file (GMMediaFile *gmf, gchar *filename);
static GMMediaFileStatus dv_gm_media_file_status (GMMediaFile *gmf);
static void dv_gm_media_file_length (GMMediaFile *gmf, GMLength *len);

static GMFrameNum dv_gm_streamcontainer_stream_frame_length (GMStreamContainer *gsc, guint stream_num);
static GMRate dv_gm_streamcontainer_stream_rate (GMStreamContainer *gsc, guint stream_num);
static guint dv_gm_streamcontainer_num_streams (GMStreamContainer *gsc);
static GMMediaStream *dv_gm_streamcontainer_stream (GMStreamContainer *gsc, guint stream_num);
static GMVideoStreamFormat *dv_gm_streamcontainer_stream_video_format (GMStreamContainer *gsc, guint stream_num);
static GMAudioStreamFormat *dv_gm_streamcontainer_stream_audio_format (GMStreamContainer *gsc, guint stream_num);
static void *dv_gm_streamcontainer_stream_get_video_frame_data (GMStreamContainer *gsc, guint stream_num, GMFrameNum fnum);
static void *dv_gm_streamcontainer_stream_get_audio_frame_data (GMStreamContainer *gsc, guint stream_num, GMFrameNum fnum, long samples);


guint gm_dvvideo_file_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMDVVideoFile",
            sizeof (GMDVVideoFile),
            sizeof (GMDVVideoFileClass),
            (GtkClassInitFunc) gm_dvvideo_file_class_init,
            (GtkObjectInitFunc) gm_dvvideo_file_init,
            (GtkArgSetFunc) NULL,
            (GtkArgGetFunc) NULL,
        };

        type = gtk_type_unique (gm_media_file_get_type (), &type_info);
    }

    return type;
}

/* overridden virtual functions */

static GMMediaFileStatus dv_gm_media_file_load_from_file (GMMediaFile *gmf, gchar *filename)
{
    GMDVVideoFile *gmdv = GM_DVVIDEO_FILE(gmf);
    gmdv->dvf = dvf_open (filename, FALSE);
    if (!gmdv->dvf) {
        return GMMF_FAIL;
    }

    return GMMF_OK;
}

static GMMediaFileStatus dv_gm_media_file_status (GMMediaFile *gmf)
{
    return GMMF_OK;
}

static void dv_gm_media_file_length (GMMediaFile *gmf, GMLength *len)
{
    GMDVVideoFile *gmdv = GM_DVVIDEO_FILE(gmf);
    guint32 frames;

    g_assert (gmdv->dvf);

    frames = dvf_num_frames (gmdv->dvf);

    gm_framenum_to_length (frames, dvf_is_PAL(gmdv->dvf) ? GM_RATE_25FPS : GM_RATE_2997FPS,
                           len);
}

static GMFrameNum dv_gm_streamcontainer_stream_frame_length (GMStreamContainer *gsc, guint stream_num)
{
    GMDVVideoFile *gmdv = GM_DVVIDEO_FILE(gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE(gsc);

    g_assert (gmdv->dvf);

    if (stream_num != 0) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return 0;
    }

    return dvf_num_frames (gmdv->dvf);
}

static GMRate dv_gm_streamcontainer_stream_rate (GMStreamContainer *gsc, guint stream_num)
{
    GMDVVideoFile *gmdv = GM_DVVIDEO_FILE(gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE(gsc);

    g_assert (gmdv->dvf);

    if (stream_num != 0) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return 0;
    }

    if (dvf_is_PAL (gmdv->dvf)) {
        return GM_RATE_25FPS;
    } else {
        return GM_RATE_2997FPS;
    }
}

static guint dv_gm_streamcontainer_num_streams (GMStreamContainer *gsc)
{
    g_return_val_if_fail (gsc, 0);
    g_return_val_if_fail (GM_DVVIDEO_FILE(gsc)->dvf, 0);

    return 1;
}

static GMMediaStream *dv_gm_streamcontainer_stream (GMStreamContainer *gsc, guint stream_num)
{
    GMDVVideoFile *gmdv = GM_DVVIDEO_FILE(gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE(gsc);
    GMMediaStream *nu;

    g_assert (gmdv->dvf);

    if (stream_num != 0) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return NULL;
    }

    nu = gm_media_stream_new (gsc, stream_num);
    return nu;
}

static GMVideoStreamFormat *dv_gm_streamcontainer_stream_video_format
(GMStreamContainer *gsc, guint stream_num)
{
    GMDVVideoFile *gmdv = GM_DVVIDEO_FILE(gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE(gsc);
    GMVideoStreamFormat *out_fmt;

    g_assert (gmdv->dvf);

    if (stream_num != 0) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return FALSE;
    }

    out_fmt = g_new0 (GMVideoStreamFormat, 1);
    out_fmt->source_format = GM_VIDEO_FORMAT_DV; /* DV decoding will be done with libdv */

    if (!dvf_is_PAL (gmdv->dvf)) {
        out_fmt->source_space = GM_COLORSPACE_YUV_411;
    } else {
        /* cheat -- check if it's IEC61384 or SMPTE314M-compliant DV for PAL */
        if (gmdv->dvf->is61834)
            out_fmt->source_space = GM_COLORSPACE_YUV_420;
        else
            out_fmt->source_space = GM_COLORSPACE_YUV_411;
    }

    out_fmt->source_space = dvf_is_PAL(gmdv->dvf) ? GM_COLORSPACE_YUV_420 : GM_COLORSPACE_YUV_411;
    out_fmt->frame_format = GM_VIDEO_FRAME_FORMAT_RGB24;
    out_fmt->frame_space = GM_COLORSPACE_RGB;
    out_fmt->width = 720;       /* DV */
    out_fmt->height = dvf_is_PAL(gmdv->dvf) ? 576 : 480;
    out_fmt->rowstride = out_fmt->width * 3;

    out_fmt->stream_rate = dvf_is_PAL(gmdv->dvf) ? GM_RATE_25FPS : GM_RATE_2997FPS;
    out_fmt->stream_total_frames = dvf_num_frames (gmdv->dvf);

    return out_fmt;
}

static GMAudioStreamFormat *
dv_gm_streamcontainer_stream_audio_format (GMStreamContainer *gsc, guint stream_num)
{
    return NULL;
}

static void *
dv_gm_streamcontainer_stream_get_video_frame_data (GMStreamContainer *gsc, guint stream_num, GMFrameNum fnum)
{
    GMDVVideoFile *gmdv = GM_DVVIDEO_FILE(gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE(gsc);
    g_assert (gmdv->dvf);

    if (stream_num != 0) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return FALSE;
    }

    return dvf_get_frame (gmdv->dvf, fnum);
}

static void *dv_gm_streamcontainer_stream_get_audio_frame_data
(GMStreamContainer *gsc, guint stream_num, GMFrameNum fnum, long samples)
{
    return NULL;
}

/*
 * init/class init/etc. funcs
 */

static GMMediaFileClass *gm_dvvideo_file_parent;

enum SIGNALS {
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
gm_dvvideo_file_destroy (GtkObject *obj)
{
    GMDVVideoFile *dvf = GM_DVVIDEO_FILE (obj);
    g_assert (dvf);

    if (dvf->dvf)
        dvf_close (dvf->dvf);

    if (GTK_OBJECT_CLASS (gm_dvvideo_file_parent)->destroy)
        (* GTK_OBJECT_CLASS (gm_dvvideo_file_parent)->destroy) (obj);
}

static void
gm_dvvideo_file_class_init (GMDVVideoFileClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;
    GMMediaFileClass *mfc = (GMMediaFileClass *) klass;
    GMStreamContainerClass *scc = (GMStreamContainerClass *) klass;

    object_class->destroy = gm_dvvideo_file_destroy;

    gm_dvvideo_file_parent = gtk_type_class (gm_media_file_get_type ());

    gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

    mfc->gm_media_file_load_from_file = dv_gm_media_file_load_from_file;
    mfc->gm_media_file_status = dv_gm_media_file_status;
    mfc->gm_media_file_length = dv_gm_media_file_length;

    scc->gm_streamcontainer_stream_frame_length = dv_gm_streamcontainer_stream_frame_length;
    scc->gm_streamcontainer_stream_rate = dv_gm_streamcontainer_stream_rate;
    scc->gm_streamcontainer_num_streams = dv_gm_streamcontainer_num_streams;
    scc->gm_streamcontainer_stream = dv_gm_streamcontainer_stream;
    scc->gm_streamcontainer_stream_video_format = dv_gm_streamcontainer_stream_video_format;
    scc->gm_streamcontainer_stream_audio_format = dv_gm_streamcontainer_stream_audio_format;
    scc->gm_streamcontainer_stream_get_video_frame_data = dv_gm_streamcontainer_stream_get_video_frame_data;
    scc->gm_streamcontainer_stream_get_audio_frame_data = dv_gm_streamcontainer_stream_get_audio_frame_data;
}

static void
gm_dvvideo_file_init (GMDVVideoFile *obj)
{
    obj->dvf = NULL;
}

GMDVVideoFile *gm_dvvideo_file_new (gchar *filename)
{
    GMDVVideoFile *new;

    new = GM_DVVIDEO_FILE (gtk_type_new (gm_dvvideo_file_get_type ()));

    new->dvf = dvf_open (filename, FALSE);
    if (!new->dvf)
        return NULL;
    return new;
}

