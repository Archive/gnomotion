/*
 * gmui-viewer.c: a clip viewer widget
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GMUI_VIEWER_H_
#define GMUI_VIEWER_H_

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-media-stream.h"
#include "gm-media-clip.h"
#include "gm-cursor.h"

#include "gmui-player-task.h"

#define GTK_TYPE_GMUI_VIEWER		(gmui_viewer_get_type ())
#define GMUI_VIEWER(obj)		(GTK_CHECK_CAST ((obj), \
					 GTK_TYPE_GMUI_VIEWER, GMUIViewer))
#define GMUI_VIEWER_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), \
					 GTK_TYPE_GMUI_VIEWER, GMUIViewerClass))
#define IS_GMUI_VIEWER(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_GMUI_VIEWER))
#define IS_GMUI_VIEWER_CLASS(obj)	(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GMUI_VIEWER))


typedef enum _GMUIViewerState GMUIViewerState;

enum _GMUIViewerState {
    GMUI_VIEWER_STOPPED,
    GMUI_VIEWER_PLAYING
};

/* This has pointers to all the useful things that are in this
 * gmuiviewer's widget hierarchy
 */
typedef struct _GMUIViewerController GMUIViewerController;

struct _GMUIViewerController {
    /* Current position */
    GMCursor *cursor;
    guint cursor_changed_conn;

    GMMediaClip *clip;
    guint clip_changed_conn;

    gboolean show_markers;

    /* some cached info about the cursor stream */
    GMFrameNum total_frames;

    gint da_width, da_height;

    GtkAdjustment *hbar_adj;

    /* The useful things */
    GtkWidget *drawingarea;     /* the drawing area at the top */
    GtkWidget *statuslabel;     /* the status bar label */
    GtkWidget *hbar;  	        /* the frame 'ruler' */

    /* The buttons */
    GtkWidget *play, *stop, *playinout;
    GtkWidget *markin, *markout, *mark;
    GtkWidget *framerev, *framefwd, *playout;
    GtkWidget *viewer_size;

    /* hack to break stupid scrollbar/cursor recursion */
    gboolean uglyhack;
};

typedef struct _GMUIViewer GMUIViewer;
typedef struct _GMUIViewerClass GMUIViewerClass;

struct _GMUIViewer {
    GtkFrame parent;

    GMUIViewerState state;
    GMUIPlayerTaskID play_id;

    GMUIViewerController controller;
};

struct _GMUIViewerClass {
    GtkFrameClass parent_class;
};

guint gmui_viewer_get_type (void);

GMUIViewer *gmui_viewer_new (void);

GMCursor *gmui_viewer_get_cursor (GMUIViewer *viewer);
void gmui_viewer_set_clip (GMUIViewer *viewer, GMMediaClip *clip);
void gmui_viewer_set_clip_with_cursor (GMUIViewer *viewer, GMMediaClip *clip,
                                       GMCursor *cursor);

GMMediaClip *gmui_viewer_get_clip (GMUIViewer *viewer);

void gmui_viewer_simple_controls (GMUIViewer *viewer);

/* Convenience function */
GtkWidget *gmui_viewer_new_window_for_clip (GMMediaClip *clip);

#endif
