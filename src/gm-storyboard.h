/*
 * gm-storyboard.h: Keeps track of clips in a 'storyboard' order
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_STORYBOARD_H
#define _GM_STORYBOARD_H

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-types.h"
#include "gm-media-clip.h"

#define GTK_TYPE_GM_STORYBOARD		(gm_storyboard_get_type ())
#define GM_STORYBOARD(obj)		(GTK_CHECK_CAST ((obj), \
                                                         GTK_TYPE_GM_STORYBOARD, \
                                                         GMStoryboard))
#define GM_STORYBOARD_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), \
                                                               GTK_TYPE_GM_STORYBOARD, \
                                                               GMStoryboardClass))
#define IS_GM_STORYBOARD(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_GM_STORYBOARD))
#define IS_GM_STORYBOARD_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GM_STORYBOARD))

#define GM_STORYBOARD_AT_END	-1
#define GM_STORYBOARD_AT_START	-2

typedef struct _GMStoryboard GMStoryboard;
typedef struct _GMStoryboardClass GMStoryboardClass;


typedef struct _GMStoryboardItem GMStoryboardItem;

typedef enum _GMStoryboardItemType {
    GM_STORYBOARD_ITEM_BLANK,
    GM_STORYBOARD_ITEM_TEXT,
    GM_STORYBOARD_ITEM_CLIP
} GMStoryboardItemType;

struct _GMStoryboardItem {
    GMStoryboardItemType type;
    gpointer data;

    GMFrameNum length;

    guint location;             /* these can be discontinous */

    /* Other bits? */
};

struct _GMStoryboard {
    GtkObject parent;

    /* GSList of GMStoryboardItem -- guaranteed to be ascending
     * by item->location
     */
    GList *items;
};

struct _GMStoryboardClass {
    GtkObjectClass parent_class;

    /* Signals */
    void (*items_changed) (GMStoryboard *sb);
};

guint gm_storyboard_get_type (void);

GMStoryboard *gm_storyboard_new (void);

void gm_storyboard_add_clip (GMStoryboard *sb, GMMediaClip *clip, gint position);
void gm_storyboard_add_text (GMStoryboard *sb, gchar *text, gint position);
void gm_storyboard_add_blank (GMStoryboard *sb, gint position);
void gm_storyboard_remove (GMStoryboard *sb, gint which);
void gm_storyboard_move (GMStoryboard *sb, gint from_position, gint to_position,
                         gboolean shrink);

#endif /* _GM_STORYBOARD_H */
