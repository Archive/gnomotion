/*
 * gmui-player-task.h: Implements a mainloop task for playing a stream
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GMUI_PLAYER_TASK_H_
#define GMUI_PLAYER_TASK_H_

#include <glib.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "gm-types.h"
#include "gm-media-stream.h"
#include "gm-cursor.h"

typedef struct _GMUIPlayerTaskData GMUIPlayerTaskData;
typedef struct _GMUIPlayerTaskPrivate GMUIPlayerTaskPrivate;
typedef guint GMUIPlayerTaskID;

typedef void (*GMUIPlayerTaskFinishedFunc) (GMUIPlayerTaskData *data);

#define GMUI_PLAYER_TASK_PLAY_VIDEO	(1 << 0)
#define GMUI_PLAYER_TASK_PLAY_AUDIO	(1 << 1)

struct _GMUIPlayerTaskData {
    gint play_what;             /* video or audio or both */

    GtkWidget *drawing;
    GMMediaStream *stream;

    GMCursor *cursor;

    GMFrameNum start_at;        /* start where if we're looping */
    GMFrameNum stop_at;         /* frame to stop at, or 0 to play until end */
    gboolean loop;              /* whether to loop infinitely */

    GMUIPlayerTaskFinishedFunc finished_func;
    void *user_data;

    /* private */
    GMUIPlayerTaskPrivate *priv;
};

GMUIPlayerTaskID gmui_player_task_new (GMUIPlayerTaskData *data);
gboolean gmui_player_task_destroy (GMUIPlayerTaskID which);

#endif /* GMUI_PLAYER_TASK_H_ */
