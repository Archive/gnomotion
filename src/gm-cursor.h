/*
 * gm-cursor.h: Generic stream position
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * gm-cursor.h: A GMCursor is a pointer to a specific place in a specific stream
 */

#ifndef GM_CURSOR_H_
#define GM_CURSOR_H_

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-types.h"
#include "gm-media-stream.h"

#define GTK_TYPE_GM_CURSOR		(gm_cursor_get_type ())
#define GM_CURSOR(obj)		(GTK_CHECK_CAST ((obj), \
					 GTK_TYPE_GM_CURSOR, GMCursor))
#define GM_CURSOR_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), \
					 GTK_TYPE_GM_CURSOR, GMCursorClass))
#define IS_GM_CURSOR(obj)		(GTK_CHECK_TYPE ((obj, GTK_TYPE_GM_CURSOR)))
#define IS_GM_CURSOR_CLASS(obj)	(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GM_CURSOR))

typedef struct _GMCursor GMCursor;
typedef struct _GMCursorClass GMCursorClass;

struct _GMCursor {
    GtkObject parent;

    GMMediaStream *stream;
    GMFrameNum fnum;
    GMFrameNum fnum_max;
};

struct _GMCursorClass {
    GtkObjectClass parent_class;

    /* Signals */
    void (*cursor_changed) (GMCursor *cursor, GMMediaStream *stream, GMFrameNum fnum);
};

guint gm_cursor_get_type (void);

GMCursor *gm_cursor_new (GMMediaStream *stream, GMFrameNum fnum);
void gm_cursor_set (GMCursor *cursor, GMMediaStream *stream, GMFrameNum fnum);
void gm_cursor_move (GMCursor *cursor, GMFrameNum fnum);
void gm_cursor_incr (GMCursor *cursor);
void gm_cursor_decr (GMCursor *cursor);

GMMediaStream *gm_cursor_stream (GMCursor *cursor);
GMFrameNum gm_cursor_frame_num (GMCursor *cursor);

#endif /* GM_CURSOR_H */
