/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 8 -*- */
/* gm-quicktime-file.c
 *
 * Copyright (C) 2000  Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.

 *
 * Author: Vladimir Vukicevic <vladimir@helixcode.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <quicktime.h>

#include <gnome.h>
#include "gm-quicktime-file.h"

/*
 * Private bits
 */

struct _GMQuicktimeFilePrivate {
    quicktime_t *qt_handle;

    int qt_audio_tracks;
    int qt_video_tracks;

    GMLength qt_length;

    int video_cutoff; /* any track numbers above video_cutoff are audio tracks */
    int num_tracks;

    GMVideoStreamFormat **video_formats;
    GMAudioStreamFormat **audio_formats;

    guint8 **vid_buffer_caches;
    guint8 ***vid_row_buffer_caches;
};

/*
 * GM::GMMediaFile
 */

static GMMediaFileStatus
qt_load_from_file (GMMediaFile *gmf, gchar *filename)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gmf);

    if (qtf->priv->qt_handle) {
        g_message ("Closing previous QT stream...");
        quicktime_close (qtf->priv->qt_handle);
    }

    qtf->priv->qt_handle = quicktime_open (filename, TRUE, FALSE);
    if (!qtf->priv->qt_handle) {
        gmf->error = GMMF_BAD_FILENAME;
        return GMMF_FAIL;
    }

    qtf->priv->video_cutoff = quicktime_video_tracks (qtf->priv->qt_handle);
    qtf->priv->video_cutoff--;  /* i.e., -1 if there are no video tracks */

    qtf->priv->num_tracks = quicktime_video_tracks (qtf->priv->qt_handle);

    if (quicktime_audio_tracks (qtf->priv->qt_handle) == 1) {
        qtf->priv->num_tracks += quicktime_track_channels (qtf->priv->qt_handle, 0);
    } else if (quicktime_audio_tracks (qtf->priv->qt_handle) != 0) {
        g_warning ("QuickTime file with more than one audio track!\n");
    }

    qtf->priv->video_formats = NULL;
    qtf->priv->audio_formats = NULL;

    qtf->priv->vid_buffer_caches = g_new0 (guint8 *,
                                           qtf->priv->video_cutoff + 1);
    qtf->priv->vid_row_buffer_caches = g_new0 (guint8 **,
                                               qtf->priv->video_cutoff + 1);

    return GMMF_OK;
}

static GMMediaFileStatus
qt_status (GMMediaFile *gmf)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gmf);

    if (qtf->priv->qt_handle)
        return GMMF_OK;

#warning FREE the video/audio formats here
    g_message ("video/audio formats not freed, FIXME!");
        
    return GMMF_FAIL;
}

static void
qt_length (GMMediaFile *gmf, GMLength *len)
{
    /* We want to give the length of the longest audio or video stream */
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gmf);
    int naudio, nvideo;
    int i;
    long maxalength, maxvlength;
    int maxatrk, maxvtrk;
    long arate;
    float vrate;

    if (qtf->priv->qt_audio_tracks || qtf->priv->qt_video_tracks) {
        /* sue me. */
        *len = qtf->priv->qt_length;
        return;
    }

    maxalength = 0;
    for (i = 0; i < naudio; i++) {
        long clen;
        clen = quicktime_audio_length (qtf->priv->qt_handle, i);
        if (clen > maxalength) {
            maxalength = clen;
            maxatrk = i;
        }
    }

    maxvlength = 0;
    for (i = 0; i < nvideo; i++) {
        long clen;
        clen = quicktime_video_length (qtf->priv->qt_handle, i);
        if (clen > maxvlength) {
            maxvlength = clen;
            maxvtrk = i;
        }
    }

    arate = quicktime_sample_rate (qtf->priv->qt_handle, maxatrk);
    vrate = quicktime_frame_rate (qtf->priv->qt_handle, maxvtrk);

    if ((maxalength / (float) arate) > (maxvlength / vrate)) {
        /* audio is longer */
        gm_framenum_to_length (maxalength, (GMRate) arate, &qtf->priv->qt_length);
    } else {
        gm_framenum_to_length (maxvlength, (GMRate) vrate, &qtf->priv->qt_length);
    }

    *len = qtf->priv->qt_length;
}

/*
 * GM::StreamContainer impls
 */

static GMFrameNum
qt_stream_frame_length (GMStreamContainer *gsc, guint stream_num)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gsc);

    if (stream_num > qtf->priv->video_cutoff) {
        /* audio track */
        int real_track = stream_num - (qtf->priv->video_cutoff == -1 ? 0 :
                                       qtf->priv->video_cutoff);
        return quicktime_audio_length (qtf->priv->qt_handle,
                                       real_track);
    } else {
        /* video track */
        return quicktime_video_length (qtf->priv->qt_handle,
                                       stream_num);
    }
}

static GMRate
qt_stream_rate (GMStreamContainer *gsc, guint stream_num)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gsc);

    if (stream_num > qtf->priv->video_cutoff) {
        /* audio track */
        int real_track = stream_num - (qtf->priv->video_cutoff == -1 ? 0 :
                                       qtf->priv->video_cutoff);
        return (GMRate) quicktime_sample_rate (qtf->priv->qt_handle,
                                               real_track);
    } else {
        /* video track */
        return (GMRate) quicktime_frame_rate (qtf->priv->qt_handle,
                                              stream_num);
    }
}

static guint
qt_num_streams (GMStreamContainer *gsc)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gsc);
    return qtf->priv->num_tracks;
}

static GMMediaStream *
qt_stream (GMStreamContainer *gsc, guint stream_num)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE (gsc);
    GMMediaStream *nu;

    g_assert (qtf->priv->qt_handle);

    if (stream_num < 0 || stream_num > qtf->priv->num_tracks) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return NULL;
    }

    nu = gm_media_stream_new (gsc, stream_num);
    return nu;
}

static GMVideoStreamFormat *
qt_stream_video_format (GMStreamContainer *gsc, guint stream_num)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE (gsc);
    GMVideoStreamFormat *out_fmt;
    int depth;

    g_assert (qtf->priv->qt_handle);

    if (stream_num < 0 || stream_num > qtf->priv->num_tracks) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return NULL;
    }

    if (stream_num > qtf->priv->video_cutoff) {
        return NULL;
    }

    if (!qtf->priv->video_formats) {
        qtf->priv->video_formats = g_new0 (GMVideoStreamFormat *,
                                           qtf->priv->video_cutoff + 1);
    } else if (qtf->priv->video_formats[stream_num]) {
        out_fmt = g_memdup (qtf->priv->video_formats[stream_num],
                            sizeof (GMVideoStreamFormat));
        return out_fmt;
    }

    if (!quicktime_supported_video (qtf->priv->qt_handle, stream_num)) {
        g_warning ("quicktime lib can't decode video in track %d, format '%s'\n",
                   stream_num, quicktime_video_compressor (qtf->priv->qt_handle, stream_num));
        return NULL;
    }

    out_fmt = g_new0 (GMVideoStreamFormat, 1);
    GM_FORMAT_BASE (out_fmt)->type = GM_FORMAT_VIDEO;

    out_fmt->source_format = GM_VIDEO_FORMAT_UNKNOWN;
    out_fmt->source_space = GM_COLORSPACE_UNKNOWN;

    depth = quicktime_video_depth (qtf->priv->qt_handle, stream_num);
    switch (depth) {
        case 24:
            out_fmt->frame_space = GM_COLORSPACE_RGB;
            out_fmt->frame_format = GM_VIDEO_FRAME_FORMAT_RGB24;
            out_fmt->bits_per_pixel = 24;
            break;
        case 32:
            out_fmt->frame_space = GM_COLORSPACE_RGBA;
            out_fmt->frame_format = GM_VIDEO_FRAME_FORMAT_RGB32;
            out_fmt->bits_per_pixel = 32;
            break;
        default:
            g_warning ("QT library returned unknown video depth: %d!", depth);
            return NULL;
            break;
    }

    out_fmt->width = quicktime_video_width (qtf->priv->qt_handle, stream_num);
    out_fmt->height = quicktime_video_height (qtf->priv->qt_handle, stream_num);
    out_fmt->rowstride = out_fmt->width * (depth / 3);

    out_fmt->stream_rate = quicktime_frame_rate (qtf->priv->qt_handle, stream_num);
    out_fmt->stream_total_frames = quicktime_video_length (qtf->priv->qt_handle, stream_num);

    qtf->priv->video_formats[stream_num] = g_memdup (out_fmt,
                                                     sizeof (GMVideoStreamFormat));

    gm_video_format_dump (out_fmt);
    return out_fmt;
}

static GMAudioStreamFormat *
qt_stream_audio_format (GMStreamContainer *gsc, guint stream_num)
{
#if 1
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE (gsc);
    GMAudioStreamFormat *out_fmt;
    int real_track;
    long tmp;
    const char *tmps;

    g_assert (qtf->priv->qt_handle);

    g_message ("audio_format request, stream %d\n", stream_num);

    if (stream_num < 0 || stream_num > qtf->priv->num_tracks) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return NULL;
    }

//    if (stream_num <= qtf->priv->video_cutoff) {
//        return NULL;
//    }

//    real_track = stream_num - (qtf->priv->video_cutoff == -1 ? 0 :
//                               qtf->priv->video_cutoff);
//    real_track = stream_num;

    if (!qtf->priv->audio_formats) {
        qtf->priv->audio_formats = g_new0 (GMAudioStreamFormat *,
                                           qtf->priv->num_tracks - (qtf->priv->video_cutoff + 1));
    } else if (qtf->priv->audio_formats[real_track]) {
        out_fmt = g_memdup (qtf->priv->audio_formats[real_track],
                            sizeof (GMAudioStreamFormat));
        return out_fmt;
    }

    /* FIXME -- we assume audio track 0 */
    if (!quicktime_supported_audio (qtf->priv->qt_handle, 0)) {
        g_warning ("quicktime lib can't decode audio in track %d, format '%s'\n",
                   stream_num, quicktime_video_compressor (qtf->priv->qt_handle, real_track));
        return NULL;
    }

    out_fmt = g_new0 (GMAudioStreamFormat, 1);
    GM_FORMAT_BASE (out_fmt)->type = GM_FORMAT_AUDIO;

    tmps = quicktime_audio_compressor (qtf->priv->qt_handle, 0);
    if (!strcmp (tmps, "raw "))
        out_fmt->source_format = GM_AUDIO_FORMAT_RAW;
    else
        out_fmt->source_format = GM_AUDIO_FORMAT_UNKNOWN;

    switch ((tmp = quicktime_sample_rate (qtf->priv->qt_handle, 0))) {
        case 32000:
            out_fmt->stream_rate = GM_RATE_32KHZ;
            break;
        case 44100:
            out_fmt->stream_rate = GM_RATE_441KHZ;
            break;
        case 48000:
            out_fmt->stream_rate = GM_RATE_48KHZ;
            break;
        default:
            g_warning ("Unrecognized sample rate: %ld\n", tmp);
            out_fmt->stream_rate = (GMRate) tmp;
            break;
    }

    /* eek, is this stereo or mono?? */
    switch ((tmp = quicktime_audio_bits (qtf->priv->qt_handle, 0))) {
        case 8:
            out_fmt->source_space = GM_AUDIOSPACE_8_MONO;
            break;
        case 12:
            out_fmt->source_space = GM_AUDIOSPACE_12_MONO;
            break;
        case 16:
            out_fmt->source_space = GM_AUDIOSPACE_16_MONO;
            break;
        default:
            g_warning ("Unrecognized sample bits: %ld\n", tmp);
            out_fmt->source_space = GM_AUDIOSPACE_UNKNOWN;
            break;
    }

    out_fmt->frame_format = GM_AUDIO_FORMAT_RAW;
    out_fmt->frame_space = GM_AUDIOSPACE_16_MONO;
    out_fmt->frame_size = (long) out_fmt->stream_rate; /* 1 second by default */

    qtf->priv->audio_formats[0] = g_memdup (out_fmt,
                                            sizeof (GMAudioFormat));

    gm_audio_format_dump (out_fmt);
    return out_fmt;
#endif

    return NULL;

}

static void *
qt_get_video_frame_data (GMStreamContainer *gsc, guint stream_num, GMFrameNum fnum)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gsc);
    GMMediaFile *gmf = GM_MEDIA_FILE (gsc);
    guint8 *retbuf;
    guint8 **rowbuf;


    g_assert (qtf->priv->qt_handle);

    if (stream_num < 0 || stream_num > qtf->priv->num_tracks) {
        gmf->error = GMMF_INVALID_STREAM_NUM;
        return NULL;
    }

    if (stream_num > qtf->priv->video_cutoff) {
        return NULL;
    }

#if 0
    if (!quicktime_supported_video (qtf->priv->qt_handle, stream_num)) {
        g_warning ("quicktime lib can't decode video in track %s, format '%s'\n",
                   stream_num, quicktime_video_compressor (qtf->priv->qt_handle, stream_num));
        return NULL;
    }
#endif

    if (qtf->priv->video_formats == NULL ||
        qtf->priv->video_formats[stream_num] == NULL)
    {
        GMVideoStreamFormat *vsf;
        vsf = qt_stream_video_format (gsc, stream_num);
        if (!vsf)
            return NULL;
        g_free (vsf);
    }

    /* This totally sucks */
    if (qtf->priv->vid_buffer_caches[stream_num] == NULL)
    {
        guint8 **rows;
        guint8 *bigbuf, *bp;
        int i;

        bigbuf = g_new (guint8,
                        qtf->priv->video_formats[stream_num]->height *
                        qtf->priv->video_formats[stream_num]->rowstride);
        rows = g_new (guint8 *,
                      qtf->priv->video_formats[stream_num]->height);
        bp = bigbuf;
        for (i = 0; i < qtf->priv->video_formats[stream_num]->height; i++) {
            rows[i] = bp;
            bp += qtf->priv->video_formats[stream_num]->rowstride;
        }

        qtf->priv->vid_buffer_caches[stream_num] = bigbuf;
        qtf->priv->vid_row_buffer_caches[stream_num] = rows;
    }

    retbuf = qtf->priv->vid_buffer_caches[stream_num];
    rowbuf = qtf->priv->vid_row_buffer_caches[stream_num];

    if (quicktime_set_video_position (qtf->priv->qt_handle,
                                      fnum, stream_num))
    {
        return NULL;
    }

    if (quicktime_decode_video (qtf->priv->qt_handle,
                                rowbuf,
                                stream_num))
    {
        /* something failed. */
        return NULL;
    }

    return retbuf;
}

static void *
qt_get_audio_frame_data (GMStreamContainer *gsc, guint stream_num, GMFrameNum fnum,
                         long samples)
{
    GMQuicktimeFile *qtf = GM_QUICKTIME_FILE (gsc);
    guint16 *samplebuf;
    int res;

    samplebuf = g_new (guint16, samples);

    res = quicktime_decode_audio (qtf->priv->qt_handle, samplebuf, NULL,
                                  samples, 0);

    return samplebuf;
}


GMQuicktimeFile *
gm_quicktime_file_new (gchar *filename)
{
    GMQuicktimeFile *qtf;

    qtf = GM_QUICKTIME_FILE (gtk_type_new (gm_quicktime_file_get_type ()));

    if (qt_load_from_file (GM_MEDIA_FILE (qtf), filename) != GMMF_OK) {
        gtk_object_destroy (GTK_OBJECT (qtf));
        return NULL;
    }

    return qtf;
}



/*
 * GTK Object stuff
 */

static GMMediaFileClass *parent_class = NULL;

static void
class_init (GMQuicktimeFileClass *klass)
{
	GtkObjectClass *object_class;
        GMMediaFileClass *mfc;
        GMStreamContainerClass *scc;

	object_class = GTK_OBJECT_CLASS (klass);
        mfc = GM_MEDIA_FILE_CLASS (klass);
        scc = GM_STREAMCONTAINER_CLASS (klass);

	parent_class = gtk_type_class (gm_media_file_get_type ());

        mfc->gm_media_file_load_from_file = qt_load_from_file;
        mfc->gm_media_file_status = qt_status;
        mfc->gm_media_file_length = qt_length;

        scc->gm_streamcontainer_stream_frame_length = qt_stream_frame_length;
        scc->gm_streamcontainer_stream_rate = qt_stream_rate;
        scc->gm_streamcontainer_num_streams = qt_num_streams;
        scc->gm_streamcontainer_stream = qt_stream;
        scc->gm_streamcontainer_stream_video_format = qt_stream_video_format;
        scc->gm_streamcontainer_stream_audio_format = qt_stream_audio_format;
        scc->gm_streamcontainer_stream_get_video_frame_data = qt_get_video_frame_data;
        scc->gm_streamcontainer_stream_get_audio_frame_data = qt_get_audio_frame_data;
}


static void
init (GMQuicktimeFile *quicktime_file)
{
	GMQuicktimeFilePrivate *priv;

	priv = g_new (GMQuicktimeFilePrivate, 1);
        priv->qt_handle = NULL;

	quicktime_file->priv = priv;
}


GtkType
gm_quicktime_file_get_type (void)
{
  static GtkType type = 0;

  if (type == 0)
    {
      static const GtkTypeInfo info =
      {
        "GMQuicktimeFile",
        sizeof (GMQuicktimeFile),
        sizeof (GMQuicktimeFileClass),
        (GtkClassInitFunc) class_init,
        (GtkObjectInitFunc) init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      type = gtk_type_unique (gm_media_file_get_type (), &info);
    }

  return type;
}

