/*
 * gm-storyboard.c: Keeps track of clips in a 'storyboard' order
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gm-storyboard.h"

static void gm_storyboard_class_init (GMStoryboardClass *klass);
static void gm_storyboard_init (GtkObject *obj);

static GMStoryboardItem *item_new_blank ();
static void real_items_changed (GMStoryboard *sb);
static GList *insert_item (GMStoryboard *sb, GMStoryboardItem *item, gint position);

guint
gm_storyboard_get_type (void)
{
    static guint type;

    if (!type) {
        static GtkTypeInfo typeinfo = {
            "GMStoryboard",
            sizeof (GMStoryboard),
            sizeof (GMStoryboardClass),
            (GtkClassInitFunc) gm_storyboard_class_init,
            (GtkObjectInitFunc) gm_storyboard_init,
            NULL,
            NULL,
            NULL
        };

        type = gtk_type_unique (GTK_TYPE_OBJECT, &typeinfo);
    }

    return type;
}

static GtkObjectClass *gm_storyboard_parent;

enum SIGNALS {
    ITEMS_CHANGED,
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
gm_storyboard_class_init (GMStoryboardClass *klass)
{
    GtkObjectClass *obj_class = (GtkObjectClass *) klass;

    gm_storyboard_parent = gtk_type_class (GTK_TYPE_OBJECT);

    signals[ITEMS_CHANGED] =
        gtk_signal_new ("items_changed",
                        GTK_RUN_LAST,
                        obj_class->type,
                        GTK_SIGNAL_OFFSET (GMStoryboardClass, items_changed),
                        gtk_marshal_NONE__NONE,
                        GTK_TYPE_NONE, 0);

    gtk_object_class_add_signals (obj_class, signals, LAST_SIGNAL);

    klass->items_changed = real_items_changed;
}

static void
gm_storyboard_init (GtkObject *obj)
{
    GMStoryboard *sb = GM_STORYBOARD (obj);

    sb->items = NULL;
}

GMStoryboard *
gm_storyboard_new (void)
{
    GMStoryboard *ret;

    ret = GM_STORYBOARD (gtk_type_new (gm_storyboard_get_type ()));

    return ret;
}

static GMStoryboardItem *item_new_blank ()
{
    GMStoryboardItem *sbi;

    sbi = g_new0 (GMStoryboardItem, 1);
    sbi->type = GM_STORYBOARD_ITEM_BLANK;
    sbi->data = NULL;
    sbi->length = 0;
    /* location to be filled in by callee */
    return sbi;
}

void
gm_storyboard_add_blank (GMStoryboard *sb, gint position)
{
    g_return_if_fail (sb != NULL);
    g_return_if_fail (position < -2);

    insert_item (sb, item_new_blank (), position);

    gtk_signal_emit (GTK_OBJECT (sb), signals[ITEMS_CHANGED]);
}
    
void
gm_storyboard_add_clip (GMStoryboard *sb, GMMediaClip *clip, gint position)
{
    GMStoryboardItem *sbi;

    g_return_if_fail (sb != NULL);
    g_return_if_fail (clip != NULL);
    g_return_if_fail (position < -2);

    sbi = g_new0 (GMStoryboardItem, 1);
    sbi->type = GM_STORYBOARD_ITEM_CLIP;
    sbi->data = clip;
    sbi->length = gm_media_clip_num_frames (clip);

    insert_item (sb, sbi, position);

    gtk_signal_emit (GTK_OBJECT (sb), signals[ITEMS_CHANGED]);
}

void
gm_storyboard_add_text (GMStoryboard *sb, gchar *text, gint position)
{
    g_return_if_fail (sb != NULL);
    g_return_if_fail (text != NULL);
    g_return_if_fail (position < -2);

    g_warning ("gm_storyboard_add_text");
}

static void real_items_changed (GMStoryboard *sb)
{
    /* signal used just for notification */
}

static GList *
insert_item (GMStoryboard *sb, GMStoryboardItem *item, gint position)
{
    GMStoryboardItem *s;
    gint locnum;
    GList *iter;
    GList *ipos;
    
    switch (position) {
        case GM_STORYBOARD_AT_END:
            if (sb->items == NULL) {
                locnum = 0;
            } else {
                s = (g_list_last (sb->items))->data;
                locnum = s->location + 1;
            }
            item->location = locnum;
            sb->items = g_list_append (sb->items, item);
            return sb->items;
            break;
        case GM_STORYBOARD_AT_START:
            if (sb->items != NULL) {
                s = sb->items->data;
                locnum = s->location;
                if (locnum == 0) {
                    /* We need to shift all locations by 1 */
                    iter = sb->items;
                    while (iter) {
                        s = iter->data;
                        s->location++;
                        iter = iter->next;
                    }
                }
            }
                        
            item->location = 0;  /* will always be 0 for AT_START */
            sb->items = g_list_prepend (sb->items, item);
            return sb->items;
            break;
        default:
            /* Insert at a specified position */
            /* Find the position */
            iter = sb->items;
            while (iter) {
                s = iter->data;
                if (s->location >= position)
                    break;
                iter = iter->next;
            }

            if (!iter) {
                GList *last = g_list_last (sb->items);
                s = last->data;
                if (s->location != position) {
                    /* We need to pad with blanks */
                    /* Note that we have to get this right, or we will
                     * recurse infinitely -- also note that we need
                     * to stick ourselves at the right spot
                     */
                    gint how_many = position - s->location;
                    gint loc = s->location + 1;
                    while (how_many--) {
                        if (loc == position) {
                            insert_item (sb, item, loc);
                        } else {
                            insert_item (sb, item_new_blank (), loc);
                        }
                        loc++;
                    }
                } else {
                    /* append at end */
                    item->location = position;
                    sb->items = g_list_append (sb->items, item);
                }

                /* Item was inserted in above loop */
                return sb->items;
                break;
            } else {
                if (s->location == position) {
                    GList *ipos = iter;
                    /* Move all of these things up by one */
                    while (iter) {
                        s = iter->data;
                        s->location++;
                        iter = iter->next;
                    }
                    iter = ipos;
                } else {
                    /* Pad with blanks again */
                    gint how_many = s->location - position - 1;
                    gint loc;
                    if (iter->prev)
                        loc = ((GMStoryboardItem *) iter->prev->data)->location;
                    else
                        loc = 0;

                    while (how_many--) {
                        if (loc == position)
                            iter = 
                        insert_item (sb, item_new_blank (), loc++);

                    }
                }

                ipos = g_list_alloc ();

                ipos->prev = iter->prev;
                ipos->next = iter;
                ipos->prev->next = ipos;
                iter->prev = ipos;
                if (ipos->prev == NULL) {
                    /* First element */
                    sb->items = iter;
                }
            }

            return ipos;
            break;
    }
}

#if 0
/* It's not clear if this is necessary */
static void shift_items (GMStoryboard *sb, gint from_where, gint shift_by)
{
    GMStoryboardItem *sbi;
    GList *iter, *firstpos = NULL;

    iter = sb->items;

    /* Find the item first */
    while (iter) {
        sbi = iter->data;
        if (sbi->location >= from_where) {
            /* Found it */
            firstpos = iter;
            break;
        }
        iter = iter->next;
    }

    if (!firstpos) {
        /* from_where is after end of list, just return */
        return;
    }

    iter = firstpos;
    while (iter) {
        sbi = iter->data;
        sbi->location += shift_by;
        iter = iter->next;
    }
}
#endif
