/*
 * gm-media-clip.h: MediaClip object for storing a specific stream
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_MEDIA_CLIP_H
#define _GM_MEDIA_CLIP_H

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-types.h"
#include "gm-formats.h"
#include "gm-media-stream.h"

#define GTK_TYPE_GM_MEDIA_CLIP               (gm_media_clip_get_type ())
#define GM_MEDIA_CLIP(obj)                   (GTK_CHECK_CAST ((obj), \
                                                              GTK_TYPE_GM_MEDIA_CLIP, \
                                                              GMMediaClip))
#define GM_MEDIA_CLIP_CLASS(klass)           (GTK_CHECK_CLASS_CAST ((klass), \
                                                                    GTK_TYPE_GM_MEDIA_CLIP, \
                                                                    GMMediaClipClass))
#define IS_GM_MEDIA_CLIP(obj)                (GTK_CHECK_TYPE ((obj), GTK_TYPE_GM_MEDIA_CLIP))

#define IS_GM_MEDIA_CLIP_CLASS(klass)        (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GM_MEDIA_CLIP))

typedef enum {
    GM_CLIP_IN_CHANGED,
    GM_CLIP_OUT_CHANGED,
    GM_CLIP_POSTER_CHANGED,
    GM_CLIP_MARKERS_CHANGED,
    GM_CLIP_NAME_CHANGED,
    GM_CLIP_COMMENT_CHANGED,
    GM_CLIP_LABLES_CHANGED,
    GM_CLIP_FLAG_SET,
    GM_CLIP_FLAG_UNSET
} GMMediaClipChangeType;

/* Flags */
#define GM_CLIP_LOCKED		(1 << 0)

typedef struct _GMMediaClip GMMediaClip;
typedef struct _GMMediaClipClass GMMediaClipClass;

struct _GMMediaClip {
    GtkObject parent;

    gchar *name;
    gchar *comment;

    gint flags;

    GSList *labels;             /* some kind of labels */

    GMMediaStream *stream;
    GMFrameNum in_frame;
    GMFrameNum out_frame;
    GMFrameNum poster_frame;    /* frame displayed as preview */

    GMMarkerSList *markers;
};

struct _GMMediaClipClass {
    GtkObjectClass parent_class;

    /* Signals */
    void (*change_notify) (GMMediaClip *clip, GMMediaClipChangeType what,
                           gpointer change_data);
};

guint gm_media_clip_get_type (void);

GMMediaClip *gm_media_clip_new (GMMediaStream *stream);
GMMediaClip *gm_media_clip_copy (GMMediaClip *clip);

GMFrameNum gm_media_clip_num_frames (GMMediaClip *clip);

GMFrameNum gm_media_clip_inout_num_frames (GMMediaClip *clip);
void gm_media_clip_get_inout_length (GMMediaClip *clip, GMLength *len);
void gm_media_clip_get_inout_timecode_length (GMMediaClip *clip, GMTimecode *tcode);

gboolean gm_media_clip_set_in_frame (GMMediaClip *clip, GMFrameNum new_in);
gboolean gm_media_clip_set_out_frame (GMMediaClip *clip, GMFrameNum new_out);
gboolean gm_media_clip_set_poster_frame (GMMediaClip *clip, GMFrameNum new_poster);
void gm_media_clip_set_name (GMMediaClip *clip, gchar *new_name);
gchar *gm_media_clip_get_name (GMMediaClip *clip);
void gm_media_clip_set_comment (GMMediaClip *clip, gchar *new_cmnt);
gchar *gm_media_clip_get_comment (GMMediaClip *clip);

gint gm_media_clip_get_flags (GMMediaClip *clip);
void gm_media_clip_set_flag (GMMediaClip *clip, gint flag);
void gm_media_clip_unset_flag (GMMediaClip *clip, gint flag);

#define GM_CLIP_IS_LOCKED(clip)		(gm_media_clip_get_flags(clip) & GM_CLIP_LOCKED)
#define GM_CLIP_SET_LOCKED(clip)	(gm_media_clip_set_flag(clip, GM_CLIP_LOCKED))
#define GM_CLIP_UNSET_LOCKED(clip)	(gm_media_clip_unset_flag(clip, GM_CLIP_LOCKED))

gchar *gm_media_clip_get_info_string (GMMediaClip *clip);

GdkPixbuf *gm_media_clip_get_poster_frame_pixbuf (GMMediaClip *clip, gint scale);

/* Need marker support stuff */
/* Need label support stuff */

#endif /* _GM_MEDIA_CLIP_H */

