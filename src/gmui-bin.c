/*
 * gmui-bin.c: GM UI Bin class
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include <stdio.h>

#include <glib.h>

#include "gmui-bin.h"
#include "gmui-viewer.h"

static void gmui_bin_class_init (GMUIBinClass *klass);
static void gmui_bin_init (GMUIBin *obj);
static void gmui_bin_destroy (GtkObject *obj);

static gint gmui_bin_button_press (GtkWidget *widget, GdkEventButton *event,
                                   gpointer data);
static gint gmui_bin_drag_begin (GtkWidget *widget, GdkDragContext *context,
                                 gpointer data);

static gboolean gmui_bin_row_action (GMUIBin *bin, int row);

void gmui_bin_drag_data_get (GMUIBin *bin, GdkDragContext *context,
                             GtkSelectionData *selection_data,
                             guint info, guint time, gpointer data);


static GtkCListClass *gmui_bin_parent;

guint
gmui_bin_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMUIBin",
            sizeof (GMUIBin),
            sizeof (GMUIBinClass),
            (GtkClassInitFunc) gmui_bin_class_init,
            (GtkObjectInitFunc) gmui_bin_init,
            (GtkArgSetFunc) NULL,
            (GtkArgGetFunc) NULL,
        };

        type = gtk_type_unique (GTK_TYPE_CLIST, &type_info);
    }

    return type;
}

static void
gmui_bin_class_init (GMUIBinClass *klass)
{
    GtkObjectClass *object_class;
    GtkWidgetClass *widget_class;
    GtkCListClass *clist_class;

    object_class = (GtkObjectClass *) klass;
    widget_class = (GtkWidgetClass *) klass;
    clist_class = (GtkCListClass *) klass;

    gmui_bin_parent = gtk_type_class (GTK_TYPE_CLIST);

    /* We ought to override some things here, I think */
    object_class->destroy = gmui_bin_destroy;
}

static void
gmui_bin_init (GMUIBin *bin)
{
    GtkCList *clist = GTK_CLIST (bin);

    gtk_clist_construct (clist, 3, NULL);

    /* Maybe override some things here? */
    gtk_signal_connect (GTK_OBJECT (bin), "button_press_event",
                        GTK_SIGNAL_FUNC (gmui_bin_button_press),
                        NULL);
    gtk_signal_connect (GTK_OBJECT (bin), "drag_begin",
                        GTK_SIGNAL_FUNC (gmui_bin_drag_begin),
                        NULL);
}

static void
gmui_bin_destroy (GtkObject *obj)
{
    GMUIBin *bin = GMUI_BIN (obj);

    GSList *iter;
    iter = bin->bin_items;
    while (iter) {
        GMUIBinItem *item = (GMUIBinItem *) iter->data;
        /* FIXME Grrr. gotta fix the '(none)' static business */
#if 0
        g_free (item->title);
        g_free (item->other_info);
#endif
        switch (item->type) {
            case GMUI_BIN_ITEM_CLIP:
                gtk_object_unref (GTK_OBJECT (item->data));
                break;
            default:
                break;
        }

        iter = iter->next;
    }
    g_slist_free (bin->bin_items);

    if (GTK_OBJECT_CLASS (gmui_bin_parent)->destroy)
        (* GTK_OBJECT_CLASS (gmui_bin_parent)->destroy) (obj);
}
    
enum {
    GM_DRAG_TYPE_STRING,
    GM_DRAG_TYPE_CLIP
};

static GtkTargetEntry target_table[] = {
    { "application/x-gm-media-clip", 0, GM_DRAG_TYPE_CLIP }
};

static guint num_targets = sizeof(target_table) / sizeof(target_table[0]);

GMUIBin *
gmui_bin_new (void)
{
    GMUIBin *new;
    GtkCList *clist;
    GtkWidget *w;

    new = GMUI_BIN (gtk_type_new (gmui_bin_get_type ()));
    clist = GTK_CLIST (new);
    w = GTK_WIDGET (new);

    gtk_clist_column_titles_hide (clist);
    gtk_drag_source_set (w, GDK_BUTTON1_MASK,
                         target_table, num_targets,
                         GDK_ACTION_COPY);
    gtk_signal_connect (GTK_OBJECT (w), "drag_data_get",
                        GTK_SIGNAL_FUNC (gmui_bin_drag_data_get), NULL);
    return new;
}

GMUIBin *
gmui_bin_new_glade (gchar *widget_name, gchar *string1, gchar *string2,
                    gint int1, gint int2)
{
    GMUIBin *new;
    new = gmui_bin_new ();
    return new;
}

gint gmui_bin_add_clip (GMUIBin *bin, GMMediaClip *clip)
{
    GdkPixbuf *poster_pixbuf;
    GdkPixmap *poster_pixmap, *poster_mask;
    gint row_num;
    GtkCList *clist;
    gchar *default_row[3] = {"","",""};

    GMUIBinItem *new_item;

    g_return_val_if_fail (bin != NULL, -1);
    g_return_val_if_fail (clip != NULL, -1);

    clist = GTK_CLIST (bin);

    new_item = g_new0 (GMUIBinItem, 1);
    new_item->type = GMUI_BIN_ITEM_CLIP;
    new_item->data = clip;
    new_item->title = clip->name ? g_strdup (clip->name) : "(none)";
    new_item->other_info = gm_media_clip_get_info_string (clip);

    /* It's a clip, so we make a poster frame */
    poster_pixbuf = gm_media_clip_get_poster_frame_pixbuf (clip, 10);
    gdk_pixbuf_render_pixmap_and_mask (poster_pixbuf,
                                       &poster_pixmap, &poster_mask,
                                       0);
    gtk_clist_freeze (clist);

    row_num = gtk_clist_append (clist, default_row);

    gtk_clist_set_pixmap (clist, row_num, 0, poster_pixmap, poster_mask);
    gtk_clist_set_text (clist, row_num, 1, new_item->title);
    gtk_clist_set_text (clist, row_num, 2, new_item->other_info);
    gtk_clist_set_row_data (clist, row_num, new_item);
    gtk_clist_columns_autosize (clist);
    gtk_clist_set_row_height (clist, GMUI_BIN_CLIP_HEIGHT + 4);

    gtk_clist_thaw (clist);

    bin->bin_items = g_slist_append (bin->bin_items, new_item);
    gtk_object_ref (GTK_OBJECT (clip));

    return row_num;
}

gboolean gmui_bin_remove_clip (GMUIBin *bin, GMMediaClip *clip)
{
    g_warning ("bin_remove_clip");
    return FALSE;
}

gboolean gmui_bin_remove_clip_row (GMUIBin *bin, gint row_num)
{
    g_warning ("bin_remove_clip_row");
    return FALSE;
}

/*
 * We select the current clip if it's a click-drag (before the drag
 * handler has a chance to grab it); we also handle opening viewers
 * if it's a clip
 */

static gint
gmui_bin_button_press (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
    GMUIBin *bin = GMUI_BIN (widget);
    GtkCList *clist = GTK_CLIST (widget);

    /* Double click, button 1 */
    if (event->button == 1 && event->type == GDK_2BUTTON_PRESS) {
        gboolean valid = FALSE;
        gint row, column;

        valid = gtk_clist_get_selection_info (clist, event->x, event->y, &row, &column);

        return gmui_bin_row_action (bin, row);
    }

    return FALSE;
}

static gint
gmui_bin_drag_begin (GtkWidget *widget, GdkDragContext *context, gpointer data)
{
    GMUIBin *bin = GMUI_BIN (widget);
    GtkCList *clist = GTK_CLIST (widget);
    GtkCListCellInfo *info;

    /* Select the click_cell.row */
    if (clist->drag_button)
        gtk_clist_select_row (clist, clist->click_cell.row, clist->click_cell.column);

    return FALSE;
}

/*
 * Perform the 'default' action on double-clicking this row
 * (or hitting enter, or...)
 */

static gboolean gmui_bin_row_action (GMUIBin *bin, int row)
{
    GMUIBinItem *item;

    item = gtk_clist_get_row_data (GTK_CLIST (bin), row);
    g_return_val_if_fail (item != NULL, FALSE);

    switch (item->type) {
        case GMUI_BIN_ITEM_CLIP:
            /* Open a viewer */
            gtk_widget_show (GTK_WIDGET (gmui_viewer_new_window_for_clip 
                                         GM_MEDIA_CLIP (item->data)));
            return TRUE;
            break;
        default:
            return FALSE;
    }
}

/*
 * Does this need to check some type things to make sure
 * the target can receive a drag of this type? I.e.
 * folders should only be draggable to other bins, but clips
 * can be dragged many places.
 */

void  
gmui_bin_drag_data_get (GMUIBin *bin,
                        GdkDragContext *context,
                        GtkSelectionData *selection_data,
                        guint info,
                        guint time,
                        gpointer data)
{
    GMUIBinItem *item;
    GList *selection = GTK_CLIST (bin)->selection;

    if (info == GM_DRAG_TYPE_CLIP) {
        if (!selection) {
            gtk_selection_data_set (selection_data,
                                    selection_data->target,
                                    GTK_TYPE_POINTER,
                                    NULL, 0);
            return;
        }


        item = gtk_clist_get_row_data (GTK_CLIST (bin),
                                       GPOINTER_TO_INT (selection->data));
        g_return_if_fail (item != NULL);

        if (item->type != GMUI_BIN_ITEM_CLIP)
            return;

        gtk_selection_data_set (selection_data,
                                selection_data->target,
                                GTK_TYPE_POINTER,
                                item->data, /* a clip */
                                sizeof (GMMediaClip));
    }
}
