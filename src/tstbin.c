
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <gnome.h>
#include <glade/glade.h>
#include <glade/glade-build.h>

#include "gmui-bin.h"
#include "gmui-viewer.h"

#include "gm-media-clip.h"
#include "gm-media-stream.h"

#include "gm-util.h"

void
on_new_file1_activate (GtkWidget *widget, gpointer data);

GladeXML *glade_xml_ui = NULL;

int main (int argc, char **argv)
{
    GMMediaStream *ms;
    GMMediaFile *dvf;
    GMMediaClip *clip;
    GMUIBin *bin;
    char **p;

    gnome_init ("tstbin", "1.0", argc, argv);
    glade_gnome_init ();
    gdk_rgb_init ();

    gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
    gtk_widget_set_default_visual (gdk_rgb_get_visual ());

    glade_xml_ui = glade_xml_new ("gnomotion.glade", NULL);
    glade_xml_signal_autoconnect (glade_xml_ui);

    bin = GMUI_BIN (glade_xml_get_widget (glade_xml_ui, "bin_main"));

    p = argv+1;
    while (*p) {
        dvf = gm_open_media_file (*p);
        if (!dvf) {
            p++;
            continue;
        }

        ms = gm_streamcontainer_stream (GM_STREAMCONTAINER(dvf), 0);
        clip = gm_media_clip_new (ms);
        gmui_bin_add_clip (bin, clip);
        p++;
    }

    gtk_main ();
    return 0;
}


void
on_new_file1_activate (GtkWidget *widget, gpointer data)
{
    /* Make a magic thing */

    GtkWidget *topwin;
    GtkWidget *child;

    topwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    child = GTK_WIDGET (gmui_viewer_new ());
    gtk_container_add (GTK_CONTAINER(topwin), child);
    gtk_widget_show_all (topwin);
    gdk_flush ();
}

void
on_open1_activate (GtkWidget *widget, gpointer data)
{
    GtkWidget *new;
    new = GTK_WIDGET (glade_xml_get_widget (glade_xml_ui, "gm_sb_window"));
    gtk_widget_show_all (new);
}

