/*
 * gm-media-file.h: GMMediaFile object - abstract object representing
 *                  any kind of media
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_MEDIA_FILE_H
#define _GM_MEDIA_FILE_H

#include <gtk/gtk.h>

#include "gm-types.h"
#include "gm-streamcontainer.h"

#define GTK_TYPE_GM_MEDIA_FILE		(gm_media_file_get_type ())
#define GM_MEDIA_FILE(obj)		(GTK_CHECK_CAST ((obj), \
                                                         GTK_TYPE_GM_MEDIA_FILE, GMMediaFile))
#define GM_MEDIA_FILE_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), \
                                                               GTK_TYPE_GM_MEDIA_FILE, \
                                                               GMMediaFileClass))
#define IS_GM_MEDIA_FILE(obj)		(GTK_CHECK_TYPE ((obj), \
                                                         GTK_TYPE_GM_MEDIA_FILE))
#define IS_GM_MEDIA_FILE_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), \
                                                               GTK_TYPE_GM_MEDIA_FILE))

#define GMMF_ERROR(obj)			((GM_MEDIA_FILE(obj))->error)


/* Silly C */
typedef struct _GMMediaFile GMMediaFile;
typedef struct _GMMediaFileClass GMMediaFileClass;
typedef enum _GMMediaFileStatus GMMediaFileStatus;
typedef enum _GMMediaFileError GMMediaFileError;

enum _GMMediaFileStatus {
    GMMF_OK = 0,
    GMMF_FAIL
};

enum _GMMediaFileError {
    GMMF_NONE = 0,
    GMMF_BAD_FILENAME,          /* filename passed was invalid/unreadable/etc. */
    GMMF_INVALID_STREAM_NUM,    /* stream number used doesn't exist */
    GMMF_FARMENUM_RANGE         /* frame number is out of range */
};

struct _GMMediaFile {
    GMStreamContainer parent;

    /* Stores error caused by the last operation */
    GMMediaFileError error;
};

struct _GMMediaFileClass {
    GMStreamContainerClass parent_class;

    /* Signals */

    /* Virtual functions */
    GMMediaFileStatus (*gm_media_file_load_from_file) (GMMediaFile *gmf, gchar *filename);
    GMMediaFileStatus (*gm_media_file_status) (GMMediaFile *gmf);

    /* Return the length of this media file, in hms */
    void (*gm_media_file_length) (GMMediaFile *gmf, GMLength *len);
};


guint gm_media_file_get_type (void);

GMMediaFile *gm_media_file_new (void);

/* Virtual function wrappers */
GMMediaFileStatus gm_media_file_load_from_file (GMMediaFile *gmf, gchar *filename);
GMMediaFileStatus gm_media_file_status (GMMediaFile *gmf);
void gm_media_file_length (GMMediaFile *gmf, GMLength *len);

#endif /* _GM_MEDIA_FILE */
