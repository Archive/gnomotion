/*
 * gm-streamcontiner.c: GMStreamContainer
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-streamcontainer.h"

static void gm_streamcontainer_class_init (GMStreamContainerClass *klass);
static void gm_streamcontainer_init (GMStreamContainer *obj);

/*
 * These are all static virtual things that just give a nice warning if called
 */
static GMFrameNum fake_gm_streamcontainer_stream_frame_length (GMStreamContainer *gsc, guint stream_num);
static GMRate fake_gm_streamcontainer_stream_rate (GMStreamContainer *gsc, guint stream_num);
static guint fake_gm_streamcontainer_num_streams (GMStreamContainer *gsc);
static GMMediaStream * fake_gm_streamcontainer_stream (GMStreamContainer *gsc, guint stream_num);
static GMVideoStreamFormat *fake_gm_streamcontainer_stream_video_format (GMStreamContainer *gsc,
                                                                         guint stream_num);
static GMAudioStreamFormat *fake_gm_streamcontainer_stream_audio_format (GMStreamContainer *gsc,
                                                                         guint stream_num);
static void *fake_gm_streamcontainer_stream_get_video_frame_data (GMStreamContainer *gsc,
                                                                  guint stream_num,
                                                                  GMFrameNum fnum);
static void *fake_gm_streamcontainer_stream_get_audio_frame_data (GMStreamContainer *gsc,
                                                                  guint stream_num,
                                                                  GMFrameNum fnum,
                                                                  long samples);

static GtkObjectClass *gm_streamcontainer_parent;

enum SIGNALS {
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

guint
gm_streamcontainer_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMStreamContainer",
            sizeof (GMStreamContainer),
            sizeof (GMStreamContainerClass),
            (GtkClassInitFunc) gm_streamcontainer_class_init,
            (GtkObjectInitFunc) gm_streamcontainer_init,
            (GtkArgSetFunc) NULL,
            (GtkArgGetFunc) NULL,
        };

        type = gtk_type_unique (gtk_object_get_type (), &type_info);
    }

    return type;
}

GMStreamContainer *
gm_streamcontainer_new (void)
{
    GMStreamContainer *new =
        GM_STREAMCONTAINER (gtk_type_new (gm_streamcontainer_get_type ()));

    return new;
}

static void
gm_streamcontainer_destroy (GtkObject *obj)
{
    GMStreamContainer *mf = GM_STREAMCONTAINER (obj);

    g_assert (mf);
    /* ? */

    if (GTK_OBJECT_CLASS (gm_streamcontainer_parent)->destroy)
        (* GTK_OBJECT_CLASS (gm_streamcontainer_parent)->destroy) (obj);
}

static void
gm_streamcontainer_class_init (GMStreamContainerClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;

    object_class->destroy = gm_streamcontainer_destroy;

    gm_streamcontainer_parent = gtk_type_class (gtk_object_get_type ());

    gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

    klass->gm_streamcontainer_stream_frame_length = fake_gm_streamcontainer_stream_frame_length;
    klass->gm_streamcontainer_stream_rate = fake_gm_streamcontainer_stream_rate;
    klass->gm_streamcontainer_num_streams = fake_gm_streamcontainer_num_streams;
    klass->gm_streamcontainer_stream = fake_gm_streamcontainer_stream;
    klass->gm_streamcontainer_stream_video_format = fake_gm_streamcontainer_stream_video_format;
    klass->gm_streamcontainer_stream_audio_format = fake_gm_streamcontainer_stream_audio_format;
    klass->gm_streamcontainer_stream_get_video_frame_data = fake_gm_streamcontainer_stream_get_video_frame_data;
    klass->gm_streamcontainer_stream_get_audio_frame_data = fake_gm_streamcontainer_stream_get_audio_frame_data;
}

static void
gm_streamcontainer_init (GMStreamContainer *obj)
{
    /* */
}

static GMFrameNum fake_gm_streamcontainer_stream_frame_length (GMStreamContainer *gsc, guint stream_num)
{
    g_warning ("gm_streamcontainer_stream_frame_length called: gsc = 0x%p, stream_num = %u",
               gsc, stream_num);
    return 0;
}

static GMRate fake_gm_streamcontainer_stream_rate (GMStreamContainer *gsc, guint stream_num)
{
    g_warning ("gm_streamcontainer_stream_rate called: gsc = 0x%p, stream_num = %u",
               gsc, stream_num);
    return 0.0;
}

static guint fake_gm_streamcontainer_num_streams (GMStreamContainer *gsc)
{
    g_warning ("gm_streamcontainer_num_video_streams called: gsc = 0x%p",
               gsc);
    return 0.0;
}

static GMMediaStream *fake_gm_streamcontainer_stream (GMStreamContainer *gsc, guint stream_num)
{
    g_warning ("gm_streamcontainer_stream called: gsc = 0x%p, stream_num = %u",
               gsc, stream_num);
    return NULL;
}

static GMVideoStreamFormat *fake_gm_streamcontainer_stream_video_format (GMStreamContainer *gsc,
                                                                    guint stream_num)
{
    g_warning ("gm_streamcontainer_stream_video_format called: gsc = 0x%p, stream_num = %u",
               gsc, stream_num);
    return NULL;
}

static GMAudioStreamFormat *fake_gm_streamcontainer_stream_audio_format (GMStreamContainer *gsc,
                                                                    guint stream_num)
{
    g_warning ("gm_streamcontainer_stream_audio_format called: gsc = 0x%p, stream_num = %u",
               gsc, stream_num);
    return NULL;
}

static void *fake_gm_streamcontainer_stream_get_video_frame_data (GMStreamContainer *gsc,
                                                             guint stream_num,
                                                             GMFrameNum fnum)
{
    g_warning ("gm_streamcontainer_stream_get_video_frame_data called: gsc = 0x%p, stream_num = %u, "
               "fnum = %ld", gsc, stream_num, fnum);
    return NULL;
}

static void *fake_gm_streamcontainer_stream_get_audio_frame_data (GMStreamContainer *gsc,
                                                                  guint stream_num,
                                                                  GMFrameNum fnum,
                                                                  long samples)
{
    g_warning ("gm_streamcontainer_stream_get_audio_frame_data called: gsc = 0x%p, stream_num = %u, "
               "fnum = %ld", gsc, stream_num, fnum);
    return NULL;
}


/*
 * Wheee... cpp kicks ass...
 */

#ifdef _CLASS
#undef _CLASS
#endif

#define _CLASS(p)  (GM_STREAMCONTAINER_CLASS(GTK_OBJECT(p)->klass))

#define RET_VIRTWRAPPER(rettype,fname,ob,arglist,args) \
rettype fname arglist \
{ g_assert (_CLASS(ob)->fname); \
return _CLASS(ob)->fname args ; }
#define VOID_VIRTWRAPPER(fname,ob,arglist,args) \
void fname arglist \
{ g_assert (_CLASS(ob)->fname); \
_CLASS(ob)->fname args ; }

/* Virtual function wrappers */
RET_VIRTWRAPPER(GMFrameNum,gm_streamcontainer_stream_frame_length,gsc,(GMStreamContainer *gsc, guint stream_num),(gsc,stream_num))

RET_VIRTWRAPPER(GMRate,gm_streamcontainer_stream_rate,gsc,(GMStreamContainer *gsc, guint stream_num),(gsc,stream_num))

RET_VIRTWRAPPER(guint,gm_streamcontainer_num_streams,gsc,(GMStreamContainer *gsc),(gsc))

RET_VIRTWRAPPER(GMMediaStream *,gm_streamcontainer_stream,gsc,(GMStreamContainer *gsc, guint stream_num),(gsc,stream_num))

RET_VIRTWRAPPER(GMVideoStreamFormat *,gm_streamcontainer_stream_video_format,gsc,(GMStreamContainer *gsc,guint stream_num),(gsc,stream_num))
RET_VIRTWRAPPER(GMAudioStreamFormat *,gm_streamcontainer_stream_audio_format,gsc,(GMStreamContainer *gsc,guint stream_num),(gsc,stream_num))

RET_VIRTWRAPPER(void *,gm_streamcontainer_stream_get_video_frame_data,gsc,(GMStreamContainer *gsc,guint stream_num,GMFrameNum fnum),(gsc,stream_num,fnum))
RET_VIRTWRAPPER(void *,gm_streamcontainer_stream_get_audio_frame_data,gsc,(GMStreamContainer *gsc,guint stream_num,GMFrameNum fnum,long samples),(gsc,stream_num,fnum,samples))

