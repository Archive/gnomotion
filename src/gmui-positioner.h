/*
 * gmui-positioner.h
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GMUI_POSITIONER_H
#define _GMUI_POSITIONER_H

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-cursor.h"

#define GTK_TYPE_GMUI_POSITIONER		(gmui_positioner_get_type ())
#define GMUI_POSITIONER(obj)		(GTK_CHECK_CAST ((obj), \
                                         GTK_TYPE_GMUI_POSITIONER, GMUIPositioner))
#define GMUI_POSITIONER_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), \
                                         GTK_TYPE_GMUI_POSITIONER, GMUIPositionerClass))
#define IS_GMUI_POSITIONER(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_GMUI_POSITIONER))
#define IS_GMUI_POSITIONER_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GMUI_POSITIONER))

typedef struct _GMUIPositioner GMUIPositioner;
typedef struct _GMUIPositionerClass GMUIPositionerClass;

struct _GMUIPositioner {
    GtkWidget parent;

    GMFrameNum start_frame, end_frame;
    gboolean display_as_timecode;

    GMMediaClip *clip;
    GMCursor *cursor;
};

struct _GMUIPositionerClass {
    GtkWidgetClass parent_class;
};

guint gmui_positioner_get_type (void);

GMUIPositioner *gmui_positioner_new (void);

#endif /* _GMUI_POSITIONER_H */
