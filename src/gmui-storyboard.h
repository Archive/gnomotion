/*
 * gmui-storyboard.h: A Canvas-based storyboard
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GMUI_STORYBOARD_H
#define GMUI_STORYBOARD_H

#include <glib.h>
#include <gnome.h>

#include "gm-media-clip.h"
#include "gm-storyboard.h"

#define GTK_TYPE_GMUI_STORYBOARD		(gmui_storyboard_get_type ())
#define GMUI_STORYBOARD(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_GMUI_STORYBOARD, GMUIStoryboard))
#define GMUI_STORYBOARD_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_GMUI_STORYBOARD, GMUIStoryboardClass))
#define IS_GMUI_STORYBOARD(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_GMUI_STORYBOARD))
#define IS_GMUI_STORYBOARD_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GMUI_STORYBOARD))

typedef struct _GMUIStoryboard GMUIStoryboard;
typedef struct _GMUIStoryboardClass GMUIStoryboardClass;

struct _GMUIStoryboard {
    GnomeCanvas parent;

    GMStoryboard *sb;
};

struct _GMUIStoryboardClass {
    GnomeCanvasClass parent_class;
    
};

guint gmui_storyboard_get_type (void);

GMUIStoryboard *gmui_storyboard_new (void);

GMStoryboard *gmui_storyboard_get_board (GMUIStoryboard *uisb);

GMUIStoryboard *gmui_storyboard_new_glade (gchar *widget_name, gchar *string1, gchar *string2,
                                           gint int1, gint int2);

#endif /* GMUI_STORYBOARD_H */
