/*
 * gm-types.c: Types used in GM
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <math.h>

#include "gm-types.h"

/*
 * Convert a frame number into timecode, at specified rate
 */

void
gm_framenum_to_timecode (GMFrameNum fnum, GMRate rate, GMTimecode *gmt)
{
    gfloat sec;
    guint32 int_sec;
    guint32 minutes;
    guint hours;

    g_assert (rate != GM_RATE_NONE);

    sec = fnum / rate;
    int_sec = (guint32) sec;
    minutes = int_sec / 60;
    hours = minutes / 60;

    gmt->hours = hours;
    gmt->minutes = minutes - (hours * 60);
    gmt->seconds = int_sec - (minutes * 60);
    /* This is where drop-frame weirdness should happen. */
    /* We assume we're not drop-frame happy. */
    gmt->frames = fnum - (int_sec * rate);
}

void
gm_framenum_to_length (GMFrameNum fnum, GMRate rate, GMLength *len)
{
    gfloat sec;
    guint32 int_sec;
    guint32 minutes;
    guint hours;

    g_assert (rate != GM_RATE_NONE);

    sec = fnum / rate;
    int_sec = (guint32) sec;
    minutes = int_sec / 60;
    hours = minutes / 60;

    len->hours = (gfloat) hours;
    len->minutes = (gfloat) (minutes - (hours * 60));
    len->seconds = ((gfloat) (int_sec - (minutes * 60))) + ((fnum - (int_sec * rate)) / rate);
}

void
gm_timecode_to_framenum (GMTimecode *gmt, GMRate rate, GMFrameNum *fnum)
{
    g_assert (rate != GM_RATE_NONE);

    *fnum = gmt->frames + (gmt->seconds * rate) + (gmt->minutes * 60 * rate) +
        (gmt->hours * 360 * rate);
}

void
gm_timecode_to_length (GMTimecode *gmt, GMRate rate, GMLength *len)
{
    g_assert (rate != GM_RATE_NONE);

    len->hours = (gfloat) gmt->hours;
    len->minutes = (gfloat) gmt->minutes;
    len->seconds = (gfloat) gmt->seconds;
    len->seconds += gmt->frames / rate;
}

void
gm_length_to_framenum (GMLength *len, GMRate rate, GMFrameNum *fnum)
{
    g_assert (rate != GM_RATE_NONE);

    *fnum = (guint32) floor(((len->hours * 360) + (len->minutes * 60) + (len->seconds)) * rate);
}

void
gm_length_to_timecode (GMLength *len, GMRate rate, GMTimecode *gmt)
{
    g_assert (rate != GM_RATE_NONE);

    gmt->hours = (guint) len->hours;
    gmt->minutes = (guint) len->minutes;
    gmt->seconds = (guint) floor (len->seconds);
    gmt->frames = (guint) ((len->seconds - floor(len->seconds)) * rate);
}

gchar *
gm_length_to_string (GMLength *len)
{
    gchar *ret;

    ret = g_strdup_printf ("%.0f:%02.0f:%02.4f",
                           len->hours,
                           len->minutes,
                           len->seconds);
    return ret;
}

gchar *
gm_timecode_to_string (GMTimecode *gmt)
{
    gchar *ret;

    ret = g_strdup_printf ("%d:%02d:%02d.%02d",
                           gmt->hours,
                           gmt->minutes,
                           gmt->seconds,
                           gmt->frames);
    return ret;
}

