/*
 * gm-typedefs.h: Objects are typedef'd here to avoid nasty circular
 *                include dependencies (blah)
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_TYPEDEFS_H
#define _GM_TYPEDEFS_H

/* gm-media-file.h */
typedef struct _GMMediaFile GMMediaFile;
typedef struct _GMMediaFileClass GMMediaFileClass;
typedef enum _GMMediaFileStatus GMMediaFileStatus;
typedef enum _GMMediaFileError GMMediaFileError;

/* gm-media-stream.h */
typedef struct _GMMediaStream GMMediaStream;
typedef struct _GMMediaStreamClass GMMediaStreamClass;
typedef enum _GMMediaFileStatus GMMediaFileStatus;

#endif
