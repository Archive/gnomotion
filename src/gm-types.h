/*
 * gm-types.h: Types used in GM
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_TYPES_H
#define _GM_TYPES_H

#include <glib.h>

#define GM_RATE_NONE       ((GMRate) 0.0)

#define GM_RATE_25FPS      ((GMRate) 25.0)
#define GM_RATE_2997FPS    ((GMRate) 29.97)
#define GM_RATE_30FPS      ((GMRate) 30.0)

#define GM_RATE_32KHZ      ((GMRate) 32000.0)
#define GM_RATE_441KHZ     ((GMRate) 44100.0)
#define GM_RATE_48KHZ      ((GMRate) 48000.0)

typedef gfloat GMRate;
typedef gulong GMFrameNum;

typedef struct _GMLength {
    gfloat hours;
    gfloat minutes;
    gfloat seconds;
} GMLength;

typedef struct _GMTimecode {
    guint hours;
    guint minutes;
    guint seconds;
    guint frames;
} GMTimecode;

typedef struct _GMMarker {
    GMTimecode position;
    gchar *name;
} GMMarker;

typedef GSList GMMarkerSList;

void gm_framenum_to_timecode (GMFrameNum fnum, GMRate rate, GMTimecode *gmt);
void gm_framenum_to_length (GMFrameNum fnum, GMRate rate, GMLength *len);
void gm_timecode_to_framenum (GMTimecode *gmt, GMRate rate, GMFrameNum *fnum);
void gm_timecode_to_length (GMTimecode *gmt, GMRate rate, GMLength *len);
void gm_length_to_framenum (GMLength *len, GMRate rate, GMFrameNum *fnum);
void gm_length_to_timecode (GMLength *len, GMRate rate, GMTimecode *gmt);

void gm_length_normalize (GMLength *len, GMRate rate);
void gm_timecode_normalize (GMLength *len, GMRate rate);

gchar *gm_length_to_string (GMLength *len);
gchar *gm_timecode_to_string (GMTimecode *gmt);

#endif /* _GM_TYPES_H */
