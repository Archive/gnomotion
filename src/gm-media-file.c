/*
 * gm-media-file.c: GMMediaFile object - abstract object representing
 *                  any kind of media
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-media-file.h"

static void gm_media_file_class_init (GMMediaFileClass *klass);
static void gm_media_file_init (GMMediaFile *obj);


/*
 * These are all static virtual things that just give a nice warning if called
 */
static GMMediaFileStatus fake_gm_media_file_load_from_file (GMMediaFile *gmf, gchar *filename);
static GMMediaFileStatus fake_gm_media_file_status (GMMediaFile *gmf);
static void fake_gm_media_file_length (GMMediaFile *gmf, GMLength *len);

static GMStreamContainerClass *gm_media_file_parent;

enum SIGNALS {
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

guint
gm_media_file_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMMediaFile",
            sizeof (GMMediaFile),
            sizeof (GMMediaFileClass),
            (GtkClassInitFunc) gm_media_file_class_init,
            (GtkObjectInitFunc) gm_media_file_init,
            (GtkArgSetFunc) NULL,
            (GtkArgGetFunc) NULL,
        };

        type = gtk_type_unique (GTK_TYPE_GM_STREAMCONTAINER, &type_info);
    }

    return type;
}

GMMediaFile *
gm_media_file_new (void)
{
    GMMediaFile *new =
        GM_MEDIA_FILE (gtk_type_new (gm_media_file_get_type ()));

    return new;
}

static void
gm_media_file_destroy (GtkObject *obj)
{
    GMMediaFile *mf = GM_MEDIA_FILE (obj);

    g_assert (mf);
    /* ? */

    if (GTK_OBJECT_CLASS (gm_media_file_parent)->destroy)
        (* GTK_OBJECT_CLASS (gm_media_file_parent)->destroy) (obj);
}

static void
gm_media_file_class_init (GMMediaFileClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;

    object_class->destroy = gm_media_file_destroy;

    gm_media_file_parent = gtk_type_class (GTK_TYPE_GM_STREAMCONTAINER);

    gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

    klass->gm_media_file_load_from_file = fake_gm_media_file_load_from_file;
    klass->gm_media_file_status = fake_gm_media_file_status;
    klass->gm_media_file_length = fake_gm_media_file_length;
}

static void
gm_media_file_init (GMMediaFile *obj)
{
    /* */
}

static GMMediaFileStatus fake_gm_media_file_load_from_file (GMMediaFile *gmf, gchar *filename)
{
    g_warning ("gm_media_load_from_file called: gmf = 0x%p filename: '%s'",
               gmf, filename);
    return GMMF_FAIL;
}

static GMMediaFileStatus fake_gm_media_file_status (GMMediaFile *gmf)
{
    g_warning ("gm_media_file_status called: gmf = 0x%p", gmf);
    return GMMF_FAIL;
}

static void fake_gm_media_file_length (GMMediaFile *gmf, GMLength *len)
{
    g_warning ("gm_media_file_length called: gmf = 0x%p", gmf);
    len->hours = 0.0;
    len->minutes = 0.0;
    len->seconds = 0.0;
}

/*
 * Wheee... cpp kicks ass...
 */

#ifdef _CLASS
#undef _CLASS
#endif

#define _CLASS(p)  (GM_MEDIA_FILE_CLASS(GTK_OBJECT(p)->klass))

#define RET_VIRTWRAPPER(rettype,fname,ob,arglist,args) \
rettype fname arglist \
{ g_assert (_CLASS(ob)->fname); \
return _CLASS(ob)->fname args ; }
#define VOID_VIRTWRAPPER(fname,ob,arglist,args) \
void fname arglist \
{ g_assert (_CLASS(ob)->fname); \
_CLASS(ob)->fname args ; }

/* Virtual function wrappers */
RET_VIRTWRAPPER(GMMediaFileStatus,gm_media_file_load_from_file,gmf,(GMMediaFile *gmf, gchar *filename),(gmf,filename))

RET_VIRTWRAPPER(GMMediaFileStatus,gm_media_file_status,gmf,(GMMediaFile *gmf),(gmf))

VOID_VIRTWRAPPER(gm_media_file_length,gmf,(GMMediaFile *gmf, GMLength *len),(gmf,len))


