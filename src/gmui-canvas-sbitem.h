#ifndef _GMUI_CANVAS_SBITEM_H
#define _GMUI_CANVAS_SBITEM_H

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

#include "gm-types.h"
#include "gm-media-clip.h"

#define GNOME_TYPE_GMUI_CANVAS_SBITEM	(gmui_canvas_sbitem_get_type ())
#define GMUI_CANVAS_SBITEM(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_GMUI_CANVAS_SBITEM, \
                                                         GMUICanvasSBItem))
#define GMUI_CANVAS_SBITEM_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_GMUI_CANVAS_SBITEM, \
                                                         GMUICanvasSBItemClass))
#define IS_GMUI_CANVAS_SBITEM(obj)	(GTK_CHECK_TYPE ((obj), GNOME_TYPE_GMUI_CANVAS_SBITEM))
#define IS_GMUI_CANVAS_SBITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_GMUI_CANVAS_SBITEM))

typedef struct _GMUICanvasSBItem GMUICanvasSBItem;
typedef struct _GMUICanvasSBItemClass GMUICanvasSBItemClass;

struct _GMUICanvasSBItem {
    GnomeCanvasItem parent;

    GMMediaClip *clip;
    GdkPixbuf *pix;

    double x, y;
};

struct _GMUICanvasSBItemClass {
    GnomeCanvasItemClass parent_class;
};

guint gmui_canvas_sbitem_get_type (void);

#endif /* _GMUI_CANVAS_SBITEM_H */
