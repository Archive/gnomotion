/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/* gm-util.h
 *
 * Copyright (C) 2000  Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Vladimir Vukicevic <vladimir@helixcode.com>
 */

#include <glib.h>

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "gm-util.h"

#include "gm-quicktime-file.h"

GMMediaFile *
gm_open_media_file (gchar *filename)
{
    GMMediaFile *dvf = NULL;

    g_return_val_if_fail (filename != NULL, NULL);

    if (strcasecmp (strrchr (filename, '.'), ".dv") == 0) {
//        dvf = gm_dvvideo_file_new (filename);
        g_warning ("DV native not supported now -- fix simpledv!");
    } else if (strcasecmp (strrchr (filename, '.'), ".qt") == 0) {
        dvf = (GMMediaFile *) gm_quicktime_file_new (filename);
    } else if (strcasecmp (strrchr (filename, '.'), ".mov") == 0) {
        dvf = (GMMediaFile *) gm_quicktime_file_new (filename);
    } else {
        g_warning ("Unsupported file format/extension: %s\n", filename);
    }

    return dvf;
}

