/*
 * gm-media-stream.c: GMMediaStream object - a stream is part of a
 *                    GMStreamContainer
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include "gm-media-stream.h"
#include "gm-media-file.h"

static void gm_media_stream_class_init (GMMediaStreamClass *klass);
static void gm_media_stream_init (GMMediaStream *obj);
static void gm_media_stream_destroy (GtkObject *obj);

/*
 * Base class virtual function implementations
 */

static const GMVideoStreamFormat *base_gm_media_stream_video_format (GMMediaStream *gms);
static const GMAudioStreamFormat *base_gm_media_stream_audio_format (GMMediaStream *gms);
static const GMFrameNum base_gm_media_stream_num_frames (GMMediaStream *gms);

static void *base_gm_media_stream_get_raw_video_frame (GMMediaStream *gms, GMFrameNum fnum);
static void *base_gm_media_stream_get_raw_audio_frame (GMMediaStream *gms, GMFrameNum fnum);
static GdkPixbuf *base_gm_media_stream_get_video_pointer (GMMediaStream *gms);
static guint16 *base_gm_media_stream_get_audio_pointer (GMMediaStream *gms);
static gboolean base_gm_media_stream_next_frame (GMMediaStream *gms);
static gboolean base_gm_media_stream_seek_frame (GMMediaStream *gms, GMFrameNum fnum);
static GdkPixbuf *base_gm_media_stream_render_video_frame (GMMediaStream *gms, GMFrameNum fnum);

static GtkObjectClass *gm_media_stream_parent;

enum SIGNALS {
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

guint
gm_media_stream_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMMediaStream",
            sizeof (GMMediaStream),
            sizeof (GMMediaStreamClass),
            (GtkClassInitFunc) gm_media_stream_class_init,
            (GtkObjectInitFunc) gm_media_stream_init,
            (GtkArgSetFunc) NULL,
            (GtkArgGetFunc) NULL,
        };

        type = gtk_type_unique (gtk_object_get_type (), &type_info);
    }

    return type;
}

static void
gm_media_stream_class_init (GMMediaStreamClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;

    object_class->destroy = gm_media_stream_destroy;

    gm_media_stream_parent = gtk_type_class (gtk_object_get_type ());

    gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

    klass->gm_media_stream_video_format = base_gm_media_stream_video_format;
    klass->gm_media_stream_audio_format = base_gm_media_stream_audio_format;
    klass->gm_media_stream_num_frames = base_gm_media_stream_num_frames;
    klass->gm_media_stream_get_raw_video_frame = base_gm_media_stream_get_raw_video_frame;
    klass->gm_media_stream_get_raw_audio_frame = base_gm_media_stream_get_raw_audio_frame;
    klass->gm_media_stream_get_video_pointer = base_gm_media_stream_get_video_pointer;
    klass->gm_media_stream_get_audio_pointer = base_gm_media_stream_get_audio_pointer;
    klass->gm_media_stream_next_frame = base_gm_media_stream_next_frame;
    klass->gm_media_stream_seek_frame = base_gm_media_stream_seek_frame;
    klass->gm_media_stream_render_video_frame = base_gm_media_stream_render_video_frame;
}

static void gm_media_stream_init (GMMediaStream *gms)
{
    /* .. */
}

GMMediaStream *
gm_media_stream_new (GMStreamContainer *gsc, guint snum)
{
    GMMediaStream *new;
    GMVideoStreamFormat *vid_fmt;
    GMAudioStreamFormat *aud_fmt;

    g_assert (gsc);
    g_assert (snum >= 0);

    new = GM_MEDIA_STREAM (gtk_type_new (gm_media_stream_get_type ()));

    if ((vid_fmt = gm_streamcontainer_stream_video_format (gsc, snum)) != NULL) {
        new->stream_flags |= GM_MEDIA_STREAM_HAS_VIDEO;
        new->video_format = vid_fmt;
    }

    if ((aud_fmt = gm_streamcontainer_stream_audio_format (gsc, snum)) != NULL) {
        new->stream_flags |= GM_MEDIA_STREAM_HAS_AUDIO;
        new->audio_format = aud_fmt;
    }

    new->videobuf = NULL;
    new->audiobuf = NULL;
    new->my_gsc = gsc;
    new->stream_num = snum;
    new->length = gm_streamcontainer_stream_frame_length (gsc, snum);

    return new;
}

GMMediaStream *
gm_media_stream_copy (GMMediaStream *stream)
{
    GMMediaStream *new;

    g_return_val_if_fail (stream != NULL, NULL);
    new = gm_media_stream_new (stream->my_gsc, stream->stream_num);
    return new;
}

static void
gm_media_stream_destroy (GtkObject *obj)
{
    GMMediaStream *gms = GM_MEDIA_STREAM (obj);

    g_assert (gms);

    g_free (gms->video_format);
    g_free (gms->audio_format);

    gms->video_format = NULL;
    gms->audio_format = NULL;

    if (GTK_OBJECT_CLASS (gm_media_stream_parent)->destroy)
        (* GTK_OBJECT_CLASS (gm_media_stream_parent)->destroy) (obj);
}

static const GMVideoStreamFormat *
base_gm_media_stream_video_format (GMMediaStream *gms)
{
    g_assert (gms);
    return gms->video_format;
}

static const GMAudioStreamFormat *
base_gm_media_stream_audio_format (GMMediaStream *gms)
{
    g_assert (gms);
    return gms->audio_format;
}

static const GMFrameNum
base_gm_media_stream_num_frames (GMMediaStream *gms)
{
    g_assert (gms);
    return gms->length;
}

static void *
base_gm_media_stream_get_raw_video_frame (GMMediaStream *gms, GMFrameNum fnum)
{
    g_warning ("gm_media_stream_get_raw_frame video called: gms = 0x%p, fnum = %u",
               gms, (guint) fnum);
    return NULL;
}

static void *
base_gm_media_stream_get_raw_audio_frame (GMMediaStream *gms, GMFrameNum fnum)
{
    g_warning ("gm_media_stream_get_raw_frame audio called: gms = 0x%p, fnum = %u",
               gms, (guint) fnum);
    return NULL;
}

static GdkPixbuf *
base_gm_media_stream_get_video_pointer (GMMediaStream *gms)
{
    if (gms->stream_flags & GM_MEDIA_STREAM_HAS_VIDEO) {
        if (!gms->videobuf) {
            GdkPixbuf *new_pbuf;
            guchar *data;
            gboolean has_alpha;

            data = g_malloc0 (gms->video_format->height * gms->video_format->rowstride);

            if (gms->video_format->frame_format == GM_VIDEO_FRAME_FORMAT_RGB24)
                has_alpha = FALSE;
            else if (gms->video_format->frame_format == GM_VIDEO_FRAME_FORMAT_RGB32)
                has_alpha = TRUE;
            else {
                g_warning ("Aiee, frame_format isn't RGB24 or RGB32!!");
                return NULL;
            }

            new_pbuf = gdk_pixbuf_new_from_data (data,
                                                 GDK_COLORSPACE_RGB,
                                                 has_alpha,
                                                 8,
                                                 gms->video_format->width,
                                                 gms->video_format->height,
                                                 gms->video_format->rowstride,
                                                 NULL, NULL);

            if (!new_pbuf) {
                g_warning ("Failed in creating gdk_pixbuf!");
                return NULL;
            }
            gms->videobuf = new_pbuf;

            /* Decode first frame */
            base_gm_media_stream_seek_frame (gms, 0);
        }
        gdk_pixbuf_ref (gms->videobuf);
        return gms->videobuf;
    } else {
        /* No video */
        return NULL;
    }
}

static guint16 *
base_gm_media_stream_get_audio_pointer (GMMediaStream *gms)
{
    if (gms->stream_flags & GM_MEDIA_STREAM_HAS_AUDIO) {
        if (!gms->audiobuf) {
            /* How many per frame */
            gms->audiobuf_samples = (long)
                (gms->audio_format->stream_rate / gms->video_format->stream_rate);
            gms->audiobuf = g_new (guint16, gms->audio_format->frame_size);
        }

        return gms->audiobuf;
    } else {
        return NULL;
    }
}

static gboolean
base_gm_media_stream_next_frame (GMMediaStream *gms)
{
    if (gms->cur_frame + 1 >= gms->length)
        return FALSE;

    gms->cur_frame++;

    if (gms->videobuf) {
        guint8 *vbuf;

        vbuf = gm_streamcontainer_stream_get_video_frame_data (gms->my_gsc,
                                                          gms->stream_num,
                                                          gms->cur_frame);
        if (!vbuf) {
            g_warning ("vbuf == NULL!");
            return FALSE;
        }

        memcpy (gdk_pixbuf_get_pixels (gms->videobuf),
                vbuf,
                gms->video_format->height * gms->video_format->rowstride);
    }

    if (gms->audiobuf) {
        /* update audiobuf */
    }

    return TRUE;
}

static gboolean
base_gm_media_stream_seek_frame (GMMediaStream *gms, GMFrameNum fnum)
{
    if (fnum >= gms->length) {
        return FALSE;
    }

    gms->cur_frame = fnum;

    if (gms->videobuf) {
        guint8 *vbuf;

        vbuf = gm_streamcontainer_stream_get_video_frame_data (gms->my_gsc,
                                                               gms->stream_num,
                                                               gms->cur_frame);
        if (!vbuf) {
            g_warning ("vbuf == NULL!");
            return FALSE;
        }

        memcpy (gdk_pixbuf_get_pixels (gms->videobuf),
                vbuf,
                gms->video_format->height * gms->video_format->rowstride);
    }

    if (gms->audiobuf) {
        /* update audiobuf */
        guint16 *abuf;

        /* buffer one frame's worth of audio */
        abuf = gm_streamcontainer_stream_get_audio_frame_data (gms->my_gsc,
                                                               gms->stream_num,
                                                               gms->cur_frame,
                                                               gms->audiobuf_samples);
        memcpy (gms->audiobuf, abuf,
                gms->audiobuf_samples * sizeof (guint16));
        /* FIXME HACK HACK HACK -- the audio buffer isn't fixed. the audio frame
         * size needs to be a format thing */
        g_free (abuf);
    }

    return TRUE;
}

static GdkPixbuf *
base_gm_media_stream_render_video_frame (GMMediaStream *gms, GMFrameNum fnum)
{
    GdkPixbuf *ret;
    GdkPixbuf *ptr;

    g_return_val_if_fail (gms != NULL, NULL);
    g_return_val_if_fail (fnum < gms->length, NULL);
    g_return_val_if_fail (gm_media_stream_video_format (gms) != NULL, NULL);

    ptr = gm_media_stream_get_video_pointer (gms);
    g_return_val_if_fail (ptr != NULL, NULL);

    if (!gm_media_stream_seek_frame (gms, fnum))
        return NULL;            /* ? */

    ret = gdk_pixbuf_copy (ptr);
    return ret;
}

/*
 * Wrappers around virtual functions
 */

#ifdef _CLASS
#undef _CLASS
#endif

#define _CLASS(p)  (GM_MEDIA_STREAM_CLASS(GTK_OBJECT(p)->klass))

#define RET_VIRTWRAPPER(rettype,fname,ob,arglist,args) \
rettype fname arglist \
{ g_assert (_CLASS(ob)->fname); \
return _CLASS(ob)->fname args ; }
#define VOID_VIRTWRAPPER(fname,ob,arglist,args) \
void fname arglist \
{ g_assert (_CLASS(ob)->fname); \
_CLASS(ob)->fname args ; }

RET_VIRTWRAPPER(const GMVideoStreamFormat *,gm_media_stream_video_format,gms,(GMMediaStream *gms),(gms))
RET_VIRTWRAPPER(const GMAudioStreamFormat *,gm_media_stream_audio_format,gms,(GMMediaStream *gms),(gms))
RET_VIRTWRAPPER(const GMFrameNum,gm_media_stream_num_frames,gms,(GMMediaStream *gms),(gms))
RET_VIRTWRAPPER(void *,gm_media_stream_get_raw_video_frame,gms,(GMMediaStream *gms, GMFrameNum fnum),(gms,fnum))
RET_VIRTWRAPPER(void *,gm_media_stream_get_raw_audio_frame,gms,(GMMediaStream *gms, GMFrameNum fnum),(gms,fnum))
RET_VIRTWRAPPER(GdkPixbuf *,gm_media_stream_get_video_pointer,gms,(GMMediaStream *gms),(gms))
RET_VIRTWRAPPER(guint16 *,gm_media_stream_get_audio_pointer,gms,(GMMediaStream *gms),(gms))
RET_VIRTWRAPPER(gboolean,gm_media_stream_next_frame,gms,(GMMediaStream *gms),(gms))
RET_VIRTWRAPPER(gboolean,gm_media_stream_seek_frame,gms,(GMMediaStream *gms, GMFrameNum fnum),(gms,fnum))
RET_VIRTWRAPPER(GdkPixbuf *,gm_media_stream_render_video_frame,gms,(GMMediaStream *gms, GMFrameNum fnum),(gms,fnum))
