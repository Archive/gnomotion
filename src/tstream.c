/*
 * stream test thing
 */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include <gtk/gtk.h>

#include "gm-media-stream.h"
#include "gm-util.h"

#include <gdk-pixbuf/gdk-pixbuf.h>

int main (int argc, char **argv)
{
    GMMediaStream *ms;
    GMMediaFile *dvf;
    GtkWidget *window, *image;

    GdkPixbuf *pbuf;

    if (argc != 2) {
	  fprintf (stderr, "Usage: %s file.dv [frameint]\n", argv[0]);
	  exit (1);
    }

    gtk_init (&argc, &argv);
    gdk_rgb_init ();

    gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
    gtk_widget_set_default_visual (gdk_rgb_get_visual ());

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    image = gtk_drawing_area_new ();

    gtk_container_add (GTK_CONTAINER (window), image);
    gtk_drawing_area_size (GTK_DRAWING_AREA (image), 720, 480);
    gtk_widget_set_usize (GTK_WIDGET (image), 720, 480);
    gtk_widget_show_all (window);
    gdk_flush ();

    dvf = gm_open_media_file (argv[1]);
    if (!dvf) {
        fprintf (stderr, "open_media_file failed");
        exit (1);
    }

    ms = gm_streamcontainer_stream (GM_STREAMCONTAINER(dvf), 0);

    pbuf = gm_media_stream_get_video_pointer (GM_MEDIA_STREAM(ms));

    if (!pbuf) {
        fprintf (stderr, "can't get pixbuf!");
        exit (1);
    }

    do {
        gdk_draw_rgb_32_image (image->window, image->style->fg_gc[image->state],
                               0, 0, 720, 480, GDK_RGB_DITHER_NORMAL,
                               gdk_pixbuf_get_pixels (pbuf),
                               gdk_pixbuf_get_rowstride (pbuf));
        gdk_flush ();
        while (gdk_events_pending ())
	       gtk_main_iteration ();
    } while (gm_media_stream_next_frame (GM_MEDIA_STREAM(ms)));

    return 0;
}


        
