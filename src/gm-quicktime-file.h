/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/* gm-quicktime-file.h
 *
 * Copyright (C) 2000  Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.

 *
 * Author: Vladimir Vukicevic <vladimir@helixcode.com>
 */

#ifndef _GM_QUICKTIME_FILE_H_
#define _GM_QUICKTIME_FILE_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "gm-media-file.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GM_TYPE_QUICKTIME_FILE			(gm_quicktime_file_get_type ())
#define GM_QUICKTIME_FILE(obj)			(GTK_CHECK_CAST ((obj), GM_TYPE_QUICKTIME_FILE, GMQuicktimeFile))
#define GM_QUICKTIME_FILE_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GM_TYPE_QUICKTIME_FILE, GMQuicktimeFileClass))
#define GM_IS_QUICKTIME_FILE(obj)			(GTK_CHECK_TYPE ((obj), GM_TYPE_QUICKTIME_FILE))
#define GM_IS_QUICKTIME_FILE_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GM_TYPE_QUICKTIME_FILE))


typedef struct _GMQuicktimeFile        GMQuicktimeFile;
typedef struct _GMQuicktimeFilePrivate GMQuicktimeFilePrivate;
typedef struct _GMQuicktimeFileClass   GMQuicktimeFileClass;

struct _GMQuicktimeFile {
	GMMediaFile parent;

	GMQuicktimeFilePrivate *priv;
};

struct _GMQuicktimeFileClass {
	GMMediaFileClass parent_class;
};


GtkType    gm_quicktime_file_get_type (void);
GMQuicktimeFile *gm_quicktime_file_new      (gchar *filename);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _GM_QUICKTIME_FILE_H_ */
