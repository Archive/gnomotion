/*
 * gmui-viewer.c: a clip viewer widget
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include <glade/glade.h>

#include "gmui-viewer.h"

#include "gmui-player-task.h"

#include "misc.h"

static void gmui_viewer_class_init (GMUIViewerClass *klass);
static void gmui_viewer_init (GMUIViewer *obj);
static void gmui_viewer_destroy (GtkObject *obj);

static void gmui_viewer_button_play (GtkButton *button, gpointer user_data);
static void gmui_viewer_button_stop (GtkButton *button, gpointer user_data);
static void gmui_viewer_button_playinout (GtkButton *button, gpointer user_data);
static void gmui_viewer_button_playout (GtkButton *button, gpointer user_data);
static void gmui_viewer_button_framerev (GtkButton *button, gpointer user_data);
static void gmui_viewer_button_framefwd (GtkButton *button, gpointer user_data);
static void gmui_viewer_button_in (GtkButton *button, gpointer user_data);
static void gmui_viewer_button_out (GtkButton *button, gpointer user_data);

static void gmui_viewer_hbar_changed (GtkAdjustment *adj, gpointer user_data);

static void gmui_viewer_paint_cursor (GMUIViewer *viewer);

static void make_viewer_widget (GtkWidget *viewer_container);

static void gmui_viewer_drag_data_received (GtkWidget *widget,
                                            GdkDragContext *context,
                                            gint x,
                                            gint y,
                                            GtkSelectionData *data,
                                            guint info,
                                            guint time);

static void gmui_viewer_draw (GtkWidget *self, GdkRectangle *area);
static gboolean gmui_viewer_expose (GtkWidget *widget, GdkEventExpose *event,
                                    gpointer user_data);
static void gmui_viewer_clip_notify (GMMediaClip *clip, GMMediaClipChangeType what,
                                     gpointer change_data, gpointer user_data);
static gboolean gmui_viewer_da_configure (GtkWidget *da, GdkEventConfigure *event,
                                          gpointer user_data);

static void gmui_viewer_player_task_finished (GMUIPlayerTaskData *ptd);

static void gmui_viewer_cursor_changed (GMCursor *cursor,
                                        GMMediaStream *stream,
                                        GMFrameNum fnum,
                                        gpointer user_data);

/*
 * Public functions
 */

guint
gmui_viewer_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMUIViewer",
            sizeof (GMUIViewer),
            sizeof (GMUIViewerClass),
            (GtkClassInitFunc) gmui_viewer_class_init,
            (GtkObjectInitFunc) gmui_viewer_init,
            NULL,
            NULL,
            NULL
        };

        type = gtk_type_unique (GTK_TYPE_FRAME, &type_info);
    }

    return type;
}

enum {
    GM_DRAG_TYPE_STRING,
    GM_DRAG_TYPE_CLIP
};

static GtkTargetEntry target_table[] = {
    { "application/x-gm-media-clip", 0, GM_DRAG_TYPE_CLIP }
};

static guint num_targets = sizeof(target_table) / sizeof(target_table[0]);

GMUIViewer *
gmui_viewer_new (void)
{
    GMUIViewer *ret;

    ret = GMUI_VIEWER (gtk_type_new (gmui_viewer_get_type ()));

    return ret;
}

void
gmui_viewer_set_clip (GMUIViewer *viewer, GMMediaClip *clip)
{
    GMCursor *nc;

    g_return_if_fail (viewer != NULL);

    nc = gm_cursor_new (clip->stream, clip->in_frame);
    gmui_viewer_set_clip_with_cursor (viewer, clip, nc);
}

void
gmui_viewer_set_clip_with_cursor (GMUIViewer *viewer, GMMediaClip *clip, GMCursor *curs)
{
    g_return_if_fail (viewer != NULL);

    if (viewer->controller.clip) {
        gtk_signal_disconnect (GTK_OBJECT (viewer->controller.clip),
                               viewer->controller.clip_changed_conn);
        gtk_object_unref (GTK_OBJECT (viewer->controller.clip));
    }

    gtk_object_ref (GTK_OBJECT (clip));
    viewer->controller.clip = clip;

    if (viewer->controller.cursor) {
        gtk_signal_disconnect (GTK_OBJECT (viewer->controller.cursor),
                               viewer->controller.cursor_changed_conn);
        gtk_object_unref (GTK_OBJECT (viewer->controller.cursor));
    }

    viewer->controller.cursor = curs;
    viewer->controller.total_frames = gm_media_clip_num_frames (clip);
    gm_media_stream_seek_frame (viewer->controller.clip->stream,
                                viewer->controller.clip->in_frame);

    viewer->controller.cursor_changed_conn = gtk_signal_connect
        (GTK_OBJECT (viewer->controller.cursor), "cursor_changed",
         GTK_SIGNAL_FUNC (gmui_viewer_cursor_changed), viewer);

    viewer->controller.clip_changed_conn = gtk_signal_connect
        (GTK_OBJECT (viewer->controller.clip), "change_notify",
         GTK_SIGNAL_FUNC (gmui_viewer_clip_notify), viewer);
                        

    /* fix the adjustment */
    viewer->controller.hbar_adj->lower = 0.0;
    viewer->controller.hbar_adj->upper = viewer->controller.total_frames;
    viewer->controller.hbar_adj->value = viewer->controller.clip->in_frame;
    viewer->controller.hbar_adj->step_increment = 1.0;
    viewer->controller.hbar_adj->page_increment = 1.0;
    viewer->controller.hbar_adj->page_size = 1.0;
    gtk_adjustment_changed (viewer->controller.hbar_adj);

    /* do this so that the display gets updated */
    gm_cursor_move (curs, viewer->controller.clip->in_frame);

    gmui_viewer_paint_cursor (viewer);
}

/* Convenience function */

GtkWidget *gmui_viewer_new_window_for_clip (GMMediaClip *clip)
{
    GtkWidget *topwin;
    GtkWidget *child;

    topwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    child = GTK_WIDGET (gmui_viewer_new ());
    gtk_container_add (GTK_CONTAINER (topwin), child);
    gtk_widget_show_all (child);
    if (clip)
        gmui_viewer_set_clip (GMUI_VIEWER (child), clip);
    return topwin;
}

/*
 * Private functions
 */

enum SIGNALS {
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };
static GtkFrameClass *gmui_viewer_parent;

static void
gmui_viewer_class_init (GMUIViewerClass *klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;
    GtkWidgetClass *widget_class = (GtkWidgetClass *) klass;
/*
    GtkContainerClass *container_class = (GtkContainerClass *) klass;
*/
    gmui_viewer_parent = gtk_type_class (GTK_TYPE_FRAME);

    object_class->destroy = gmui_viewer_destroy;
    widget_class->draw = gmui_viewer_draw;

    gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
}

static void
gmui_viewer_init (GMUIViewer *obj)
{
    make_viewer_widget (GTK_WIDGET (obj));

#define V_GET_WIDGET(name)  GTK_WIDGET(gtk_object_get_data (GTK_OBJECT(obj), (name)))
    obj->controller.drawingarea = V_GET_WIDGET("video_drawing");
    obj->controller.statuslabel = V_GET_WIDGET("status_label");
    obj->controller.hbar = V_GET_WIDGET("viewer_frame_scrollbar");
    obj->controller.play = V_GET_WIDGET("viewer_play_button");
    obj->controller.stop = V_GET_WIDGET("viewer_stop_button");
    obj->controller.playinout = V_GET_WIDGET("viewer_playinout_button");
    obj->controller.framefwd = V_GET_WIDGET("viewer_framefwd_button");
    obj->controller.framerev = V_GET_WIDGET("viewer_framerev_button");
    obj->controller.playout = V_GET_WIDGET("viewer_playout_button");
    obj->controller.viewer_size = V_GET_WIDGET("viewer_size_button");
    obj->controller.markin = V_GET_WIDGET("viewer_mark_in_button");
    obj->controller.markout = V_GET_WIDGET("viewer_mark_out_button");
    obj->controller.mark = V_GET_WIDGET("viewer_mark_button");
#undef V_GET_WIDGET

    obj->controller.hbar_adj = gtk_range_get_adjustment (GTK_RANGE (obj->controller.hbar));

    obj->controller.show_markers = TRUE;

/*     gtk_widget_show_all (GTK_WIDGET (obj)); */

    gtk_drag_dest_set (GTK_WIDGET (obj),
                       GTK_DEST_DEFAULT_ALL,
                       target_table, num_targets,
                       GDK_ACTION_COPY);

    gtk_signal_connect (GTK_OBJECT (obj), "expose-event",
                        GTK_SIGNAL_FUNC (gmui_viewer_expose), NULL);
    gtk_signal_connect (GTK_OBJECT (obj->controller.drawingarea), "configure-event",
                        GTK_SIGNAL_FUNC (gmui_viewer_da_configure), obj);

    gtk_signal_connect (GTK_OBJECT (obj->controller.play), "clicked",
                        gmui_viewer_button_play, obj);
    gtk_signal_connect (GTK_OBJECT (obj->controller.stop), "clicked",
                        gmui_viewer_button_stop, obj);
    gtk_signal_connect (GTK_OBJECT (obj->controller.playinout), "clicked",
                        gmui_viewer_button_playinout, obj);
    gtk_signal_connect (GTK_OBJECT (obj->controller.framerev), "clicked",
                        gmui_viewer_button_framerev, obj);
    gtk_signal_connect (GTK_OBJECT (obj->controller.framefwd), "clicked",
                        gmui_viewer_button_framefwd, obj);
    gtk_signal_connect (GTK_OBJECT (obj->controller.playout), "clicked",
                        gmui_viewer_button_playout, obj);
    gtk_signal_connect (GTK_OBJECT (obj->controller.markin), "clicked",
                        gmui_viewer_button_in, obj);
    gtk_signal_connect (GTK_OBJECT (obj->controller.markout), "clicked",
                        gmui_viewer_button_out, obj);

    gtk_signal_connect (GTK_OBJECT (obj->controller.hbar_adj), "value-changed",
                        gmui_viewer_hbar_changed, obj);

    gtk_signal_connect (GTK_OBJECT (obj), "drag_data_received",
                        GTK_SIGNAL_FUNC (gmui_viewer_drag_data_received), NULL);

    obj->controller.uglyhack = FALSE;
}

static void
gmui_viewer_destroy (GtkObject *obj)
{
    GMUIViewer *viewer = GMUI_VIEWER(obj);

    g_assert (viewer);

    if (viewer->controller.cursor)
        gtk_object_unref (GTK_OBJECT (viewer->controller.cursor));

    if (GTK_OBJECT_CLASS (gmui_viewer_parent)->destroy)
        (* GTK_OBJECT_CLASS (gmui_viewer_parent)->destroy) (obj);
}

static void
gmui_viewer_paint_cursor (GMUIViewer *viewer)
{
    GtkWidget *da;
    GdkPixbuf *pbuf;
    GdkPixbuf *my_pbuf;
    gint width, height;

    GMMediaClip *clip;
    GMMediaStream *ms;
    GMCursor *curs;

    g_return_if_fail (viewer != NULL);

    if (!GTK_WIDGET_REALIZED (viewer))
        return;

    if (viewer->controller.cursor == NULL)
        return;

    clip = viewer->controller.clip;
    curs = viewer->controller.cursor;
    ms = gm_cursor_stream (curs);
    da = viewer->controller.drawingarea;
    width = viewer->controller.da_width;
    height = viewer->controller.da_height;

    gm_media_stream_seek_frame (ms, gm_cursor_frame_num (curs));
    pbuf = gm_media_stream_get_video_pointer (ms);
    my_pbuf = gdk_pixbuf_scale_simple (pbuf, width, height, GDK_INTERP_BILINEAR);
    if (gm_media_stream_video_format (ms)->frame_format == GM_VIDEO_FRAME_FORMAT_RGB32) {
        gdk_draw_rgb_32_image (da->window, da->style->fg_gc[da->state],
                               0, 0, width, height, GDK_RGB_DITHER_NORMAL,
                               gdk_pixbuf_get_pixels (my_pbuf),
                               gdk_pixbuf_get_rowstride (my_pbuf));
    } else if (gm_media_stream_video_format (ms)->frame_format == GM_VIDEO_FRAME_FORMAT_RGB24) {
        gdk_draw_rgb_image (da->window, da->style->fg_gc[da->state],
                            0, 0, width, height, GDK_RGB_DITHER_NORMAL,
                            gdk_pixbuf_get_pixels (my_pbuf),
                            gdk_pixbuf_get_rowstride (my_pbuf));
    } else {
        g_warning ("I don't know how to display this kind of image format!");
    }

    gdk_pixbuf_unref (my_pbuf);
    gdk_pixbuf_unref (pbuf);

    if (viewer->controller.show_markers) {
        /* Now overlay markers over the top of this */
        if (gm_cursor_frame_num (curs) == clip->in_frame) {
            gtk_draw_string (da->style, da->window, da->state,
                             15, 15 + da->style->font->ascent + 1,
                             "IN");
        }

        if (gm_cursor_frame_num (curs) == clip->out_frame) {
            gtk_draw_string (da->style, da->window, da->state,
                             da->allocation.width - 15 -
                             gdk_string_width (da->style->font, "OUT"),
                             15 + da->style->font->ascent + 1,
                             "OUT");
        }
    }

    gdk_flush ();

}

static void
gmui_viewer_player_task_finished (GMUIPlayerTaskData *ptd)
{
    GMUIViewer *viewer = GMUI_VIEWER (ptd->user_data);
    viewer->state = GMUI_VIEWER_STOPPED;
}


/*
 * UI button handlers
 */

static void
gmui_viewer_button_play (GtkButton *button, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);
    GMUIPlayerTaskData *td;
    GMCursor *curs;

    if (viewer->controller.cursor == NULL)
        return;

    curs = viewer->controller.cursor;

    if (gm_cursor_frame_num (curs) == viewer->controller.total_frames)
        gm_cursor_move (curs, viewer->controller.clip->in_frame);

    td = g_new0 (GMUIPlayerTaskData, 1);
    /* FIXME for AUDIO or selectable */
    td->play_what = GMUI_PLAYER_TASK_PLAY_VIDEO | GMUI_PLAYER_TASK_PLAY_AUDIO;
    td->drawing = viewer->controller.drawingarea;
    td->stream = gm_cursor_stream (curs);
    td->cursor = curs;
    td->user_data = viewer;
    td->finished_func = gmui_viewer_player_task_finished;

    viewer->state = GMUI_VIEWER_PLAYING;
    viewer->play_id = gmui_player_task_new (td);
}

static void
gmui_viewer_button_playout (GtkButton *button, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);
    GMUIPlayerTaskData *td;
    GMCursor *curs;

    if (viewer->controller.cursor == NULL)
        return;

    curs = viewer->controller.cursor;

    if (gm_cursor_frame_num (curs) == viewer->controller.total_frames)
        gm_cursor_move (curs, viewer->controller.clip->in_frame);

    td = g_new0 (GMUIPlayerTaskData, 1);
    /* FIXME for AUDIO or selectable */
    td->play_what = GMUI_PLAYER_TASK_PLAY_VIDEO;
    td->drawing = viewer->controller.drawingarea;
    td->stream = gm_cursor_stream (curs);
    td->cursor = curs;
    td->user_data = viewer;
    td->finished_func = gmui_viewer_player_task_finished;
    td->stop_at = viewer->controller.clip->out_frame;

    viewer->state = GMUI_VIEWER_PLAYING;
    viewer->play_id = gmui_player_task_new (td);
}

static void
gmui_viewer_button_playinout (GtkButton *button, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);
    GMUIPlayerTaskData *td;

    GMCursor *curs;

    g_return_if_fail (viewer->controller.cursor != NULL);

    curs = viewer->controller.cursor;
    gm_cursor_move (curs, viewer->controller.clip->in_frame);

    td = g_new0 (GMUIPlayerTaskData, 1);

    /* FIXME for AUDIO or selectable */
    td->play_what = GMUI_PLAYER_TASK_PLAY_VIDEO;
    td->drawing = viewer->controller.drawingarea;
    td->stream = gm_cursor_stream (curs);
    td->cursor = curs;
    td->user_data = viewer;
    td->start_at = viewer->controller.clip->in_frame;
    td->stop_at = viewer->controller.clip->out_frame;
    td->loop = TRUE;

    td->finished_func = gmui_viewer_player_task_finished;

    viewer->state = GMUI_VIEWER_PLAYING;
    viewer->play_id = gmui_player_task_new (td);
}

/* Take us back to the in point, or if we're already at the in point,
 * to the start of the stream
 */

/*
 * FIXME - this can be extended to go back and forth between markers,
 * with options and/or key modifiers to stop only at in/out, etc.
 */

static void
gmui_viewer_button_framerev (GtkButton *button, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);

    if (viewer->controller.cursor == NULL)
        return;
    gm_cursor_decr (viewer->controller.cursor);

    gmui_viewer_paint_cursor (viewer);
}

static void
gmui_viewer_button_framefwd (GtkButton *button, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);

    if (viewer->controller.cursor == NULL)
        return;
    gm_cursor_incr (viewer->controller.cursor);

    gmui_viewer_paint_cursor (viewer);
}

/* Mark the in point from the cursor */

static void
gmui_viewer_button_in (GtkButton *button, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);
    GMCursor *curs;

    if (viewer->controller.cursor == NULL)
        return;

    g_return_if_fail (viewer->controller.clip != NULL);
    curs = viewer->controller.cursor;

    gm_media_clip_set_in_frame (viewer->controller.clip,
                                gm_cursor_frame_num (curs));
}

static void
gmui_viewer_button_out (GtkButton *button, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);
    GMCursor *curs;

    if (viewer->controller.cursor == NULL)
        return;

    g_return_if_fail (viewer->controller.clip != NULL);
    curs = viewer->controller.cursor;

    gm_media_clip_set_out_frame (viewer->controller.clip,
                                 gm_cursor_frame_num (curs));
}

static void
gmui_viewer_button_stop (GtkButton *button, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);

    if (viewer->state != GMUI_VIEWER_PLAYING)
        return;

    gmui_player_task_destroy (viewer->play_id);
    viewer->play_id = 0;
    viewer->state = GMUI_VIEWER_STOPPED;
}

/*
 * Notification handling
 */
static void
gmui_viewer_clip_notify (GMMediaClip *clip, GMMediaClipChangeType what,
                         gpointer change_data, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);

    if (viewer->state != GMUI_VIEWER_PLAYING) {
        gmui_viewer_paint_cursor (viewer);
    }
}

/*
 * Drag and drop stuff
 */

static void
gmui_viewer_drag_data_received (GtkWidget *widget,
                                GdkDragContext *context,
                                gint x,
                                gint y,
                                GtkSelectionData *data,
                                guint info,
                                guint time)
{
    GMUIViewer *viewer = GMUI_VIEWER (widget);

    if ((data->length >= 0) && (data->format = GTK_TYPE_POINTER) &&
        *((void **) (data->data)) != NULL)
    {
        GMMediaClip *clip = GM_MEDIA_CLIP (data->data);
        gmui_viewer_set_clip (viewer, clip);
        gtk_drag_finish (context, TRUE, FALSE, time);
        return;
    }

    gtk_drag_finish (context, FALSE, FALSE, time);
}

static gboolean
gmui_viewer_expose (GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (widget);
    if (viewer->state != GMUI_VIEWER_PLAYING)
        gmui_viewer_paint_cursor (viewer);

    return FALSE;
}

static gboolean
gmui_viewer_da_configure (GtkWidget *da, GdkEventConfigure *event,
                          gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);

    viewer->controller.da_width = event->width;
    viewer->controller.da_height = event->height;

    return FALSE;
}

static void
gmui_viewer_draw (GtkWidget *self, GdkRectangle *area)
{
    GMUIViewer *viewer = GMUI_VIEWER (self);
    if (viewer->state != GMUI_VIEWER_PLAYING)
        gmui_viewer_paint_cursor (viewer);

    if (GTK_WIDGET_CLASS (gmui_viewer_parent)->draw)
        (* GTK_WIDGET_CLASS (gmui_viewer_parent)->draw) (self, area);
}

GMMediaClip *
gmui_viewer_get_clip (GMUIViewer *viewer)
{
    g_return_val_if_fail (viewer != NULL, NULL);

    return viewer->controller.clip;
}

static void
gmui_viewer_hbar_changed (GtkAdjustment *adj, gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);
    if (viewer->state == GMUI_VIEWER_PLAYING) {
#if 0
        /* if it's playing, we need to stop playing */
        gmui_player_task_destroy (viewer->play_id);
#else
        /* The above does not work. I need to write a real cursor
         * scroller widget instead of hacking a scrollbar
         */
        return;
#endif
    }

    /* Update the cursor */
    if (!viewer->controller.uglyhack) {
        gm_cursor_move (viewer->controller.cursor, (GMFrameNum) adj->value);
    } else {
        viewer->controller.uglyhack = FALSE;
    }
}

static void gmui_viewer_cursor_changed (GMCursor *cursor,
                                        GMMediaStream *stream,
                                        GMFrameNum fnum,
                                        gpointer user_data)
{
    GMUIViewer *viewer = GMUI_VIEWER (user_data);
    gchar *new_lab;
    GMTimecode tcd;

    /* Update the scrollbar */
    viewer->controller.uglyhack = TRUE;
    viewer->controller.hbar_adj->value = fnum;
    gtk_adjustment_value_changed (viewer->controller.hbar_adj);

    /* Update the status bar */
    gm_framenum_to_timecode (fnum,
                             gm_media_stream_video_format (stream)->stream_rate,
                             &tcd);

    new_lab = gm_timecode_to_string (&tcd);
    gtk_label_set_text (GTK_LABEL (viewer->controller.statuslabel), new_lab);
    g_free (new_lab);
    gmui_viewer_paint_cursor (viewer);
}

void
gmui_viewer_simple_controls (GMUIViewer *viewer)
{
    viewer->controller.show_markers = FALSE;

    gtk_widget_hide (GTK_WIDGET (viewer->controller.viewer_size));
    gtk_widget_hide (GTK_WIDGET (viewer->controller.markin));
    gtk_widget_hide (GTK_WIDGET (viewer->controller.markout));
    gtk_widget_hide (GTK_WIDGET (viewer->controller.mark));
}


static void
make_viewer_widget (GtkWidget *viewer_container)
{
  GtkWidget *viewer_main_vbox;
  GtkWidget *video_drawing;
  GtkWidget *hbox1;
  GtkWidget *hbox2;
  GtkWidget *viewer_play_button;
  GtkWidget *pixmap1;
  GtkWidget *viewer_stop_button;
  GtkWidget *pixmap3;
  GtkWidget *viewer_playinout_button;
  GtkWidget *pixmap2;
  GtkWidget *viewer_frame_scrollbar;
  GtkWidget *hbox3;
  GtkWidget *viewer_mark_in_button;
  GtkWidget *viewer_mark_out_button;
  GtkWidget *viewer_mark_button;
  GtkWidget *hbox4;
  GtkWidget *hbox5;
  GtkWidget *viewer_playout_button;
  GtkWidget *pixmap4;
  GtkWidget *viewer_framerev_button;
  GtkWidget *pixmap5;
  GtkWidget *viewer_framefwd_button;
  GtkWidget *pixmap6;
  GtkWidget *status_label;
  GtkWidget *viewer_size_button;

  viewer_main_vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref (viewer_main_vbox);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_main_vbox", viewer_main_vbox,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_main_vbox);
  gtk_container_add (GTK_CONTAINER (viewer_container), viewer_main_vbox);

  video_drawing = gtk_drawing_area_new ();
  gtk_widget_ref (video_drawing);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "video_drawing", video_drawing,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (video_drawing);
  gtk_box_pack_start (GTK_BOX (viewer_main_vbox), video_drawing, TRUE, TRUE, 0);
  gtk_widget_set_usize (video_drawing, 180, 144);

  hbox1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref (hbox1);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "hbox1", hbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox1);
  gtk_box_pack_start (GTK_BOX (viewer_main_vbox), hbox1, FALSE, TRUE, 0);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref (hbox2);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "hbox2", hbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (hbox1), hbox2, FALSE, TRUE, 0);

  viewer_play_button = gtk_button_new ();
  gtk_widget_ref (viewer_play_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_play_button", viewer_play_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_play_button);
  gtk_box_pack_start (GTK_BOX (hbox2), viewer_play_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_play_button, 17, 17);

  pixmap1 = create_pixmap (viewer_container, "gnomotion/gm-play.xpm", FALSE);
  gtk_widget_ref (pixmap1);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "pixmap1", pixmap1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (pixmap1);
  gtk_container_add (GTK_CONTAINER (viewer_play_button), pixmap1);

  viewer_stop_button = gtk_button_new ();
  gtk_widget_ref (viewer_stop_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_stop_button", viewer_stop_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_stop_button);
  gtk_box_pack_start (GTK_BOX (hbox2), viewer_stop_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_stop_button, 17, 17);

  pixmap3 = create_pixmap (viewer_container, "gnomotion/gm-stop.xpm", FALSE);
  gtk_widget_ref (pixmap3);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "pixmap3", pixmap3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (pixmap3);
  gtk_container_add (GTK_CONTAINER (viewer_stop_button), pixmap3);

  viewer_playinout_button = gtk_button_new ();
  gtk_widget_ref (viewer_playinout_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_playinout_button", viewer_playinout_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_playinout_button);
  gtk_box_pack_start (GTK_BOX (hbox2), viewer_playinout_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_playinout_button, 17, 17);

  pixmap2 = create_pixmap (viewer_container, "gnomotion/gm-inoutplay.xpm", FALSE);
  gtk_widget_ref (pixmap2);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "pixmap2", pixmap2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (pixmap2);
  gtk_container_add (GTK_CONTAINER (viewer_playinout_button), pixmap2);

  viewer_frame_scrollbar = gtk_hscrollbar_new (GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 0, 0, 0, 0)));
  gtk_widget_ref (viewer_frame_scrollbar);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_frame_scrollbar", viewer_frame_scrollbar,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_frame_scrollbar);
  gtk_box_pack_start (GTK_BOX (hbox1), viewer_frame_scrollbar, TRUE, TRUE, 0);

  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref (hbox3);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "hbox3", hbox3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox3);
  gtk_box_pack_start (GTK_BOX (hbox1), hbox3, FALSE, TRUE, 0);

  viewer_mark_in_button = gtk_button_new_with_label (_("I"));
  gtk_widget_ref (viewer_mark_in_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_mark_in_button", viewer_mark_in_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_mark_in_button);
  gtk_box_pack_start (GTK_BOX (hbox3), viewer_mark_in_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_mark_in_button, 15, -2);

  viewer_mark_out_button = gtk_button_new_with_label (_("O"));
  gtk_widget_ref (viewer_mark_out_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_mark_out_button", viewer_mark_out_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_mark_out_button);
  gtk_box_pack_start (GTK_BOX (hbox3), viewer_mark_out_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_mark_out_button, 15, -2);

  viewer_mark_button = gtk_button_new_with_label (_("M"));
  gtk_widget_ref (viewer_mark_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_mark_button", viewer_mark_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_mark_button);
  gtk_box_pack_start (GTK_BOX (hbox3), viewer_mark_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_mark_button, 15, -2);

  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref (hbox4);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "hbox4", hbox4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox4);
  gtk_box_pack_start (GTK_BOX (viewer_main_vbox), hbox4, FALSE, TRUE, 0);

  hbox5 = gtk_hbox_new (FALSE, 0);
  gtk_widget_ref (hbox5);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "hbox5", hbox5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox5);
  gtk_box_pack_start (GTK_BOX (hbox4), hbox5, FALSE, FALSE, 0);

  viewer_playout_button = gtk_button_new ();
  gtk_widget_ref (viewer_playout_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_playout_button", viewer_playout_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_playout_button);
  gtk_box_pack_start (GTK_BOX (hbox5), viewer_playout_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_playout_button, 17, 17);

  pixmap4 = create_pixmap (viewer_container, "gnomotion/gm-playstop.xpm", FALSE);
  gtk_widget_ref (pixmap4);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "pixmap4", pixmap4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (pixmap4);
  gtk_container_add (GTK_CONTAINER (viewer_playout_button), pixmap4);

  viewer_framerev_button = gtk_button_new ();
  gtk_widget_ref (viewer_framerev_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_framerev_button", viewer_framerev_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_framerev_button);
  gtk_box_pack_start (GTK_BOX (hbox5), viewer_framerev_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_framerev_button, 17, 17);

  pixmap5 = create_pixmap (viewer_container, "gnomotion/gm-framerev.xpm", FALSE);
  gtk_widget_ref (pixmap5);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "pixmap5", pixmap5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (pixmap5);
  gtk_container_add (GTK_CONTAINER (viewer_framerev_button), pixmap5);

  viewer_framefwd_button = gtk_button_new ();
  gtk_widget_ref (viewer_framefwd_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_framefwd_button", viewer_framefwd_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_framefwd_button);
  gtk_box_pack_start (GTK_BOX (hbox5), viewer_framefwd_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (viewer_framefwd_button, 17, 17);

  pixmap6 = create_pixmap (viewer_container, "gnomotion/gm-framefwd.xpm", FALSE);
  gtk_widget_ref (pixmap6);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "pixmap6", pixmap6,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (pixmap6);
  gtk_container_add (GTK_CONTAINER (viewer_framefwd_button), pixmap6);

  status_label = gtk_label_new (_("Status"));
  gtk_widget_ref (status_label);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "status_label", status_label,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (status_label);
  gtk_box_pack_start (GTK_BOX (hbox4), status_label, TRUE, TRUE, 0);
  gtk_widget_set_usize (status_label, -2, 14);
  gtk_label_set_justify (GTK_LABEL (status_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_padding (GTK_MISC (status_label), 5, 0);

  viewer_size_button = gtk_button_new_with_label (_("Size..."));
  gtk_widget_ref (viewer_size_button);
  gtk_object_set_data_full (GTK_OBJECT (viewer_container), "viewer_size_button", viewer_size_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewer_size_button);
  gtk_box_pack_start (GTK_BOX (hbox4), viewer_size_button, FALSE, FALSE, 0);
}
