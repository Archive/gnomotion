/*
 * gm-media-stream.h: GMMediaStream object - a stream is part of a
 *                    GMMediaFile
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GM_MEDIA_STREAM_H
#define _GM_MEDIA_STREAM_H

#include <glib.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "gm-types.h"
#include "gm-formats.h"

/* Silly C */
typedef struct _GMMediaStream GMMediaStream;
typedef struct _GMMediaStreamClass GMMediaStreamClass;

#include "gm-streamcontainer.h"

#define GTK_TYPE_GM_MEDIA_STREAM		(gm_media_stream_get_type ())
#define GM_MEDIA_STREAM(obj)			(GTK_CHECK_CAST ((obj), \
                                                                 GTK_TYPE_GM_MEDIA_STREAM, \
                                                                 GMMediaStream))
#define GM_MEDIA_STREAM_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), \
                                                                       GTK_TYPE_GM_MEDIA_STREAM, \
                                                                       GMMediaStreamClass))
#define IS_GM_MEDIA_STREAM(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_GM_MEDIA_STREAM))
#define IS_GM_MEDIA_STREAM_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GM_MEDIA_STREAM))

#define GM_MEDIA_STREAM_HAS_VIDEO		(1 << 0)
#define GM_MEDIA_STREAM_HAS_AUDIO		(1 << 1)

struct _GMMediaStream {
    GtkObject parent;

    guint8 stream_flags;

    GMStreamContainer *my_gsc;  /* the StreamContainer that we're linked to */
    GMFrameNum length;          /* the total # of frames in this stream */
    GMFrameNum cur_frame;

    guint stream_num;           /* the stream number in GMMF */
 
    GMVideoStreamFormat *video_format;
    GMAudioStreamFormat *audio_format;

    GdkPixbuf *videobuf;        /* pointer to video image buffer (usu. GdkPixbuf) */
    guint16 *audiobuf;          /* pointer to audio image buffer */
    long audiobuf_samples;      /* how many samples are in the audio buf */
};

struct _GMMediaStreamClass {
    GtkObjectClass parent_class;

    /* Signals */

    /* Virtual functions */
    const GMVideoStreamFormat * (*gm_media_stream_video_format) (GMMediaStream *gms);
    const GMAudioStreamFormat * (*gm_media_stream_audio_format) (GMMediaStream *gms);

    const GMFrameNum (*gm_media_stream_num_frames) (GMMediaStream *gms);

    /* Return raw frame data in whatever magic format this is
     * (gotta ask what kind of formats we have)
     */
    void * (*gm_media_stream_get_raw_video_frame) (GMMediaStream *gms, GMFrameNum fnum);
    void * (*gm_media_stream_get_raw_audio_frame) (GMMediaStream *gms, GMFrameNum fnum);

    /* Either a GdkPixbuf * for video, or guint8 for audio
     * For audio, we don't have a way to give the info on the kind of
     * data is in there.. this should be fixed with some sort of generic
     * audio format.. GdkAudiobuf maybe?
     */
    GdkPixbuf * (*gm_media_stream_get_video_pointer) (GMMediaStream *gms);
    guint16 * (*gm_media_stream_get_audio_pointer) (GMMediaStream *gms);
    gboolean (*gm_media_stream_next_frame) (GMMediaStream *gms);
    gboolean (*gm_media_stream_seek_frame) (GMMediaStream *gms, GMFrameNum fnum);

    GdkPixbuf * (*gm_media_stream_render_video_frame) (GMMediaStream *gms, GMFrameNum fnum);
};

guint gm_media_stream_get_type (void);

GMMediaStream *gm_media_stream_new (GMStreamContainer *gsc, guint snum);

GMMediaStream *gm_media_stream_copy (GMMediaStream *stream);

const GMVideoStreamFormat *gm_media_stream_video_format (GMMediaStream *gms);
const GMAudioStreamFormat *gm_media_stream_audio_format (GMMediaStream *gms);
const GMFrameNum gm_media_stream_num_frames (GMMediaStream *gms);
void *gm_media_stream_get_raw_video_frame (GMMediaStream *gms, GMFrameNum fnum);
void *gm_media_stream_get_raw_audio_frame (GMMediaStream *gms, GMFrameNum fnum);
GdkPixbuf *gm_media_stream_get_video_pointer (GMMediaStream *gms);
guint16 *gm_media_stream_get_audio_pointer (GMMediaStream *gms);
gboolean gm_media_stream_next_frame (GMMediaStream *gms);
gboolean gm_media_stream_seek_frame (GMMediaStream *gms, GMFrameNum fnum);
GdkPixbuf *gm_media_stream_render_video_frame (GMMediaStream *gms, GMFrameNum fnum);

#endif /* _GM_MEDIA_STREAM_H */
