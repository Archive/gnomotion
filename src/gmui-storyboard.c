/*
 * gmui-storyboard.c: A Canvas-based storyboard
 *
 * Copyright (C) 2000 Vladimir Vukicevic
 *
 * Authors: Vladimir Vukicevic <vladimir@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gmui-storyboard.h"

#include "gmui-canvas-sbitem.h"

static void gmui_storyboard_class_init (GMUIStoryboardClass *klass);
static void gmui_storyboard_init (GMUIStoryboard *obj);
static void gmui_storyboard_destroy (GtkObject *obj);

guint gmui_storyboard_get_type (void)
{
    static guint type = 0;

    if (!type) {
        GtkTypeInfo type_info = {
            "GMUIStoryboard",
            sizeof (GMUIStoryboard),
            sizeof (GMUIStoryboardClass),
            (GtkClassInitFunc) gmui_storyboard_class_init,
            (GtkObjectInitFunc) gmui_storyboard_init,
            NULL, NULL, NULL
        };

        type = gtk_type_unique (GNOME_TYPE_CANVAS, &type_info);
    }

    return type;
}

GMUIStoryboard *gmui_storyboard_new (void)
{
    GMUIStoryboard *ret;

    ret = GMUI_STORYBOARD (gtk_type_new (gmui_storyboard_get_type ()));
    
    ret->sb = gm_storyboard_new ();
    
    return ret;
}

static void gmui_storyboard_class_init (GMUIStoryboardClass *klass)
{
    ;
}

static void gmui_storyboard_init (GMUIStoryboard *obj)
{
    gint i, j;

    GtkArg argx, argy;
    
    for (j = 0; j < 8; j++) {
        for (i = 0; i < 3; i++) {
#if 1
            gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (obj)),
                                   gmui_canvas_sbitem_get_type (),
                                   "x", 105.0 * i,
                                   "y", 105.0 * j,
                                   NULL);
#else
            gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (obj)),
                                   gnome_canvas_rect_get_type (),
                                   "x1", 105.0 * i,
                                   "y1", 105.0 * j,
                                   "x2", 105.0 * i + 100.0,
                                   "y2", 105.0 * j + 100.0,
                                   "fill_color_rgba", 0xff000000,
                                   NULL);
#endif
        }
    }
}

static void
gmui_storyboard_destroy (GtkObject *obj)
{
    /* .. not overriden */
}

GMStoryboard *
gmui_storyboard_get_board (GMUIStoryboard *uisb)
{
    g_return_val_if_fail (uisb != NULL, NULL);

    return uisb->sb;
}


GMUIStoryboard *
gmui_storyboard_new_glade (gchar *widget_name, gchar *string1, gchar *string2,
                           gint int1, gint int2)
{
    GMUIStoryboard *new;

    new = gmui_storyboard_new ();
    return new;
}
