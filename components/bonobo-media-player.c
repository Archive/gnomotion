/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/* bonobo-media-player.c
 *
 * Copyright (C) 2000  Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.

 *
 * Author: Vladimir Vukicevic <vladimir@helixcode.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/errno.h>
#include <sys/file.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <bonobo.h>
#include <gnome.h>

#include "bonobo-media-player.h"

#include <liboaf/liboaf.h>


#include "gm-types.h"
#include "gm-media-file.h"
#include "gm-media-clip.h"
#include "gm-util.h"

#include "gmui-viewer.h"

/*
 * private struct
 */

struct _BonoboMediaPlayerPrivate {
    GMMediaClip *the_clip;
    GMUIViewer *the_viewer;

    gchar *filename;
};

/*
 * Gtk object bits
 */

static BonoboControlClass *parent_class = NULL;

static void
class_init (BonoboMediaPlayerClass *klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);

	parent_class = gtk_type_class (bonobo_control_get_type ());

}


static void
init (BonoboMediaPlayer *media_player)
{
	BonoboMediaPlayerPrivate *priv;

	priv = g_new0 (BonoboMediaPlayerPrivate, 1);

	media_player->priv = priv;
}

GtkType
bonobo_media_player_get_type (void)
{
  static GtkType type = 0;

  if (type == 0)
    {
      static const GtkTypeInfo info =
      {
        "BonoboMediaPlayer",
        sizeof (BonoboMediaPlayer),
        sizeof (BonoboMediaPlayerClass),
        (GtkClassInitFunc) class_init,
        (GtkObjectInitFunc) init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      type = gtk_type_unique (bonobo_control_get_type (), &info);
    }

  return type;
}


/*
 * Bonobo::PropertyBag
 */

#define BMP_ARG_FILENAME 1

static void
bmp_prop_set (BonoboPropertyBag *bag,
              const BonoboArg *arg,
              guint arg_id,
              gpointer closure)
{
    BonoboMediaPlayer *bmp = (BonoboMediaPlayer *) closure;

    switch (arg_id) {
        case BMP_ARG_FILENAME:
            if (bmp->priv->filename)
                return; /* bah */
            bmp->priv->filename = g_strdup (BONOBO_ARG_GET_STRING (arg));
            break;
        default:
            break;
    }
}

static void
bmp_prop_get (BonoboPropertyBag *bag,
              BonoboArg *arg,
              guint arg_id,
              gpointer closure)
{
    BonoboMediaPlayer *bmp = (BonoboMediaPlayer *) closure;

    switch (arg_id) {
        case BMP_ARG_FILENAME:
            if (bmp->priv->filename)
                BONOBO_ARG_SET_STRING (arg, bmp->priv->filename);
            else
                BONOBO_ARG_SET_STRING (arg, "");
            break;
        default:
            break;
    }
}

/*
 * Bonobo::PersistStream
 */

static void
bmp_stream_load (BonoboPersistStream *ps,
                 const Bonobo_Stream stream,
                 Bonobo_Persist_ContentType type,
                 void *closure,
                 CORBA_Environment *ev)
{
    /* This really sucks right here. But there isn't another way of doing it,
     * for now.
     *
     * The data must be available as a disk file for libquicktime to function,
     * because of the borkenness of the design. It shouldn't be too hard to
     * change this -- luckily libqt abstracts away the data reading. But no
     * time for now, this is a dirty hack.
     */
    BonoboMediaPlayer *bmp = (BonoboMediaPlayer *) closure;

    gchar tmp_template[] = "/tmp/bonobo-media-XXXXXX";
    gint tmp_fd;

    if (!(!strcmp (type, "video/quicktime") || !strcmp (type, "video/x-dv"))) {
        g_warning ("Unsupported video type to media player (%s)!", type);
        return;
    }

    tmp_fd = mkstemp (tmp_template);
    bmp->priv->filename = g_strdup (tmp_template);

    do {
        Bonobo_Stream_iobuf *stream_iobuf;
        int z;

        Bonobo_Stream_read (stream, 4096, &stream_iobuf, ev);
        if (ev->_major != CORBA_NO_EXCEPTION) {
            g_free (bmp->priv->filename);
            bmp->priv->filename = NULL;
            close (tmp_fd);
            unlink (tmp_template);
            return;
        }

        if (stream_iobuf->_length == 0) {
            break;
        }

        /* FIXME -- handle this correctly */
        z = write (tmp_fd, stream_iobuf->_buffer, stream_iobuf->_length);
        if (z < stream_iobuf->_length) {
            g_warning ("write failed: %d %d\n", z, errno);
        }

        CORBA_free (stream_iobuf);
    } while (1);

    close (tmp_fd);

    /* now the entire stream exists as a file. Now we can actually create
     * the clip and stuff
     */
    {
        GMMediaFile *gmf;
        GMMediaStream *gms;
        GMMediaClip *clip;

        if (!strcmp (type, "video/quicktime")) {
            gmf = GM_MEDIA_FILE (gm_quicktime_file_new (tmp_template));
            if (!gmf) {
                g_warning ("Couldn't create quicktime file object");
                unlink (tmp_template);
                return;
            }
        } else if (!strcmp (type, "video/x-dv")) {
            /* DV */
//            gmf = GM_MEDIA_FILE (gm_dvvideo_file_new (tmp_template));
            gmf = NULL;
            if (!gmf) {
                g_warning ("Couldn't create DVvideo file object");
                unlink (tmp_template);
                return;
            }
        }

        gms = gm_streamcontainer_stream (GM_STREAMCONTAINER (gmf), 0);
        clip = gm_media_clip_new (gms);

        bmp->priv->the_clip = clip;
        gmui_viewer_set_clip (bmp->priv->the_viewer,
                              clip);
    }
}

static Bonobo_Persist_ContentTypeList *
bmp_stream_types (BonoboPersistStream *ps,
                  void *closure,
                  CORBA_Environment *ev)
{
    return bonobo_persist_generate_content_types (2,
                                                  "video/quicktime",
                                                  "video/x-dv");
}

/*
 * Media::Controllable
 * not implemented [yet]
 */

/* .. */

/*
 * Bonobo construction bits
 */
BonoboMediaPlayer *
bonobo_media_player_construct (BonoboMediaPlayer *bmp,
                               Bonobo_Control corba_control,
                               GtkWidget *the_widget)
{
    BonoboPersistStream *bps;
    BonoboPropertyBag *pb;

    bonobo_control_construct (BONOBO_CONTROL (bmp), corba_control, the_widget);

    bps = bonobo_persist_stream_new (bmp_stream_load,
                                     NULL, /* save */
                                     NULL, /* max */
                                     bmp_stream_types,
                                     bmp);

    if (!bps) {
        bonobo_object_unref (BONOBO_OBJECT (bmp));
        return NULL;
    }

    bonobo_object_add_interface (BONOBO_OBJECT (bmp), BONOBO_OBJECT (bps));

    pb = bonobo_property_bag_new (bmp_prop_get, bmp_prop_set, bmp);
    bonobo_property_bag_add (pb, "filename", BMP_ARG_FILENAME,
                             BONOBO_ARG_STRING, NULL,
                             "temporary filename",
                             0);

    bonobo_control_set_properties (BONOBO_CONTROL (bmp),
                                   pb);

    return bmp;
}

BonoboMediaPlayer *
bonobo_media_player_create (void)
{
    BonoboMediaPlayer *bmp;
    Bonobo_Control corba_control;
    GMUIViewer *viewer;

    bmp = BONOBO_MEDIA_PLAYER (gtk_type_new (bonobo_media_player_get_type ()));
    corba_control = bonobo_control_corba_object_create (BONOBO_OBJECT (bmp));

    if (corba_control == CORBA_OBJECT_NIL) {
        bonobo_object_unref (BONOBO_OBJECT (bmp));
        return NULL;
    }

    viewer = gmui_viewer_new ();
    gmui_viewer_simple_controls (viewer);
    gtk_widget_show (GTK_WIDGET (viewer));

    bmp->priv->the_viewer = viewer;

    return bonobo_media_player_construct (bmp, corba_control, GTK_WIDGET (viewer));
}



/**
 ** Factory, and main
 **/

static BonoboGenericFactory *bmp_factory = NULL;
static int running_objects = 0;

static void
bmp_destroy_cb (BonoboMediaPlayer *bmp)
{
    if (bmp->priv->filename)
        unlink (bmp->priv->filename);

    if (bmp->priv->the_viewer) {
        gtk_widget_destroy (GTK_WIDGET (bmp->priv->the_viewer));
        gtk_object_destroy (GTK_OBJECT (bmp->priv->the_clip));
    }

    g_free (bmp->priv);

    bonobo_object_unref (BONOBO_OBJECT (bmp));

    running_objects--;
    if (running_objects)
        return;

    gtk_main_quit ();
}


static BonoboObject *
bonobo_media_player_factory (BonoboGenericFactory *factory, void *closure)
{
    BonoboMediaPlayer *bmp;

    bmp = bonobo_media_player_create ();
    bonobo_object_ref (BONOBO_OBJECT (bmp));

    gtk_signal_connect
        (GTK_OBJECT (bmp), "destroy",
         GTK_SIGNAL_FUNC (bmp_destroy_cb), NULL);

    running_objects++;

    return BONOBO_OBJECT (bmp);
}

int
main (int argc, char **argv)
{
	CORBA_Environment ev;
	CORBA_ORB orb = CORBA_OBJECT_NIL;
    
	CORBA_exception_init (&ev);
    
	gnome_init_with_popt_table
		("bonobo-media-player", "0.0", argc, argv, oaf_popt_options, 0, NULL);

	orb = oaf_init (argc, argv);
    
	if (!bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL))
		g_error (_("Could not initialize Bonobo"));
    
	g_message ("Registering media player factory");
    
	bmp_factory = bonobo_generic_factory_new
		("OAFIID:Bonobo_Media_Player_Factory",
		 bonobo_media_player_factory, NULL);
	bonobo_main ();
    
	CORBA_exception_free (&ev);
    
	return 0;
}
