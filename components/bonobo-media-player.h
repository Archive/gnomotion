/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/* bonobo-media-player.h
 *
 * Copyright (C) 2000  Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.

 *
 * Author: Vladimir Vukicevic <vladimir@helixcode.com>
 */

#ifndef _BONOBO_MEDIA_PLAYER_H_
#define _BONOBO_MEDIA_PLAYER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define BONOBO_TYPE_MEDIA_PLAYER			(bonobo_media_player_get_type ())
#define BONOBO_MEDIA_PLAYER(obj)			(GTK_CHECK_CAST ((obj), BONOBO_TYPE_MEDIA_PLAYER, BonoboMediaPlayer))
#define BONOBO_MEDIA_PLAYER_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), BONOBO_TYPE_MEDIA_PLAYER, BonoboMediaPlayerClass))
#define BONOBO_IS_MEDIA_PLAYER(obj)			(GTK_CHECK_TYPE ((obj), BONOBO_TYPE_MEDIA_PLAYER))
#define BONOBO_IS_MEDIA_PLAYER_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), BONOBO_TYPE_MEDIA_PLAYER))


typedef struct _BonoboMediaPlayer        BonoboMediaPlayer;
typedef struct _BonoboMediaPlayerPrivate BonoboMediaPlayerPrivate;
typedef struct _BonoboMediaPlayerClass   BonoboMediaPlayerClass;

struct _BonoboMediaPlayer {
	BonoboControl parent;

	BonoboMediaPlayerPrivate *priv;
};

struct _BonoboMediaPlayerClass {
	BonoboControlClass parent_class;
};


GtkType bonobo_media_player_get_type (void);

BonoboMediaPlayer *bonobo_media_player_construct (BonoboMediaPlayer *bmp,
                                                  Bonobo_Control corba_control,
                                                  GtkWidget *the_widget);
BonoboMediaPlayer *bonobo_media_player_create (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _BONOBO_MEDIA_PLAYER_H_ */
